//
//  LeaderBoardWorkoutDetailViewController.swift
//  OneBeat
//
//  Created by osvinuser on 09/04/19.
//  Copyright © 2019 osvinuser. All rights reserved.
//

import UIKit

class LeaderBoardWorkoutDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet weak var leaderboardTableView: UITableView!
    var getWorkoutListingData:Getworkout?
    var workOutID = String()
    var serverDateTimeStr : String?
    var serverDateInDateFormat : Date?
    var topFiveListingData = [DetailTopFiveClass]()
    var attendeeListing = [AttendeeClass]()
    var points : String?
    var heart_rate = String()
    var count = Int()
    var hours = String()
    var minutes = String()
    var seconds = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if Reachability.isConnectedToNetwork() == true {
           getUser_Workout_Detail()
        } else {
            self.showAlert(Constants.AlertsMessages.noInternetConnecLocalize)
        }
        
        let backBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(backbuttonaction))
        self.navigationItem.leftBarButtonItem  = backBarButtonItem
        let sliderImage = UIImage(named: "ic_back_arrow_black")
        navigationItem.leftBarButtonItem?.image = sliderImage
        navigationItem.leftBarButtonItem?.tintColor = UIColor.black
    }
    
    @objc func backbuttonaction(){
        self.navigationController?.popViewController(animated:true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return topFiveListingData.count + 1
    }
    
    @IBAction func backBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if indexPath.row == 0 {
             let cell = leaderboardTableView.dequeueReusableCell(withIdentifier: "FirstCell", for: indexPath) as! HistoryTableViewCell
            guard self.getWorkoutListingData != nil else {return cell}
            
            if let getImageInfo = self.getWorkoutListingData?.workout_image {
               cell.workOut_Image.sd_setImage(with: URL(string: getImageInfo), placeholderImage: UIImage(named: "ic_sketchy_background") , options: .continueInBackground) { (downloadImage, error, cacheType, urlValue) in
                }
            }
            
            if let getTrainerImageInfo = self.getWorkoutListingData?.get_trainer!["trainer_profile_image"] as? String  {
               
                cell.trainer_Image.sd_setImage(with: URL(string: getTrainerImageInfo), placeholderImage: UIImage(named: "ic_sketchy_background") , options: .continueInBackground) { (downloadImage, error, cacheType, urlValue) in
                }
            }
            
            let Heart_rate = self.getWorkoutListingData?.heart_rate ?? ""
            let totaltimelabel = "\(self.getWorkoutListingData?.attendee_Workout_time ?? "")"
            let HeartRate = Heart_rate.components(separatedBy: " ")
            let Hr = HeartRate[0] as? String
            
            let first_String: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.black, .font: UIFont.boldSystemFont(ofSize: 22)]
            let second_String: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.red, .font: UIFont.systemFont(ofSize: 14)]
            
            let first = NSMutableAttributedString(string: Hr!, attributes: first_String)
            let second = NSMutableAttributedString(string: "  bpm", attributes: second_String)
            first.append(second)
            
            let timeIn_Min = Int(totaltimelabel) as? Int
            convertTo_Min_Hrs_Sec(value: timeIn_Min!)
            if topFiveListingData.count == 0 {
                cell.seeAll_Btn.isHidden = true
            }else {
                cell.seeAll_Btn.isHidden = false
            }
            cell.seeAll_Btn.addTarget(self, action: #selector(SeeAll_User), for: .touchUpInside)
            
            cell.detail_Description_Label.text = self.getWorkoutListingData?.description ?? ""            
            cell.Heart_Rate_Label.attributedText = first
            cell.points_Label.text = getWorkoutListingData?.points ?? "--"
            cell.calories_burnt_Label.text = self.getWorkoutListingData?.calories ?? ""
            cell.total_Time_Label.text = hours
            cell.people_Joined_Label.text = "\(self.getWorkoutListingData?.attendee_count ?? "") people joined"
            let attendcount = self.getWorkoutListingData?.attendee_count ?? ""
            count = Int(attendcount)!
            
            cell.detail_Time_Label.text = "\(self.getWorkoutListingData?.duration ?? "") minutes"
            cell.detail_Location_Label.text = self.getWorkoutListingData?.get_trainer?["trainer_location"] as? String ?? ""
            cell.trainer_Name_Label.text = self.getWorkoutListingData?.get_trainer?["trainer_name"] as? String ?? ""
            cell.user_Name_Label.text = self.getWorkoutListingData?.cat_name
            cell.attendiesCollectionView.reloadData()
            return cell
        }
        else {
             let cell = leaderboardTableView.dequeueReusableCell(withIdentifier: "SecondCell", for: indexPath) as! HistoryTableViewCell
            guard self.getWorkoutListingData != nil else {return cell}
            
            if let getImageInfo = topFiveListingData[indexPath.row - 1].user_image {
                
                cell.second_Cell_user_Image.sd_setImage(with: URL(string: getImageInfo), placeholderImage: UIImage(named: "ic_sketchy_background") , options: .continueInBackground) { (downloadImage, error, cacheType, urlValue) in
                }
            }
            cell.secondCell_User_Name.text = topFiveListingData[indexPath.row - 1].user_name ?? ""
            cell.secondCell_User_Points.text = topFiveListingData[indexPath.row - 1].points ?? ""
            
            return cell
        }    
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 720
        }
        else {
            return 80
        }
    }
    
    
    func convertTo_Min_Hrs_Sec(value: Int) {
    
        let min = (value/60)
        let hrs = value/3600
        if min > 60 {
            let total_Hours = "\(hrs)" + "+" + " " + "hrs"
            hours = "\(total_Hours)"
            print(hours)
        }else {
            hours = "\(min)" + "+" + " " + "min"
            print(hours)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! AttendiesCollectionViewCell
        
        if let getImageInfo = self.attendeeListing[indexPath.row].attende_image  {

            cell.user_Image.sd_setImage(with: URL(string: getImageInfo ), placeholderImage: UIImage(named: "ic_sketchy_background") , options: .continueInBackground) { (downloadImage, error, cacheType, urlValue) in
            }
        }
        return cell
    }
    
    @objc func SeeAll_User() {
        let VC = storyboard?.instantiateViewController(withIdentifier: "SeeAllUseListingViewController") as! SeeAllUseListingViewController
        VC.workout_Id = getWorkoutListingData?.workout_id ?? ""
        VC.checkBtnStatus = "Navigation"
        navigationController?.pushViewController(VC, animated: true)
    }
    
    func serverTime(){
        if self.serverDateTimeStr != nil && self.serverDateTimeStr != ""{
            let serverDateString = self.UTCToLocalStringValue(date: self.serverDateTimeStr ?? "", incomingDateFormat: "yyyy-MM-dd HH:mm:ss", outgoingDateFormat: "yyyy-MM-dd HH:mm:ss")
            
            serverDateInDateFormat = self.getDateFormatterDate(date: serverDateString, incomingDateFormat: "yyyy-MM-dd HH:mm:ss")
            
        }else{}
    }

    func getUser_Workout_Detail(){
        
        self.view.hideToastActivity()
        let parameter: [String:Any] = ["workout_id" : workOutID,
//                                       "user_id": "90"]
                                       "user_id" :getSharedPrefrance(key: "id")]
        //"http://184.73.56.196/Api/Workout/workoutDetails
        executePOST(view: self.view, path: Constants.APIs.LIVEURL+Constants.APIs.getUserworkoutDetail, parameter: parameter){ response in
            print(response)
            let status = response["ResponseCode"].stringValue
            
            if(status == "true"){
                
                self.getWorkoutListingData = Getworkout.init(json:response["WorkoutJoinResponse"].dictionaryObject!)
                
                let topFiveList = response["WorkoutJoinResponse"]

                for data in topFiveList["get_attendee_image"].arrayValue {
                    self.attendeeListing.append(AttendeeClass(json:data.dictionaryObject!)!)
                }
                for data in topFiveList["top_highest_scorer"].arrayValue{
                    self.topFiveListingData.append(DetailTopFiveClass(json:data.dictionaryObject!)!)
                }
                
                let serverDateTime = response["Server_time"].stringValue
                if serverDateTime.count != 0{
                    self.serverDateTimeStr = serverDateTime
                    self.serverTime()
                }
                self.leaderboardTableView.reloadData()
                
            }else{
                self.showToast(message:response["MessageWhatHappen"].stringValue)
            }
        }
    }
}
