//
//  BTDevicesVC.swift
//  OneBeat
//
//  Created by osvinuser on 11/12/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//

import UIKit
import Pulsator

class BTDevicesVC: UIViewController,WFDiscoveryManagerDelegate,UITableViewDataSource,UITableViewDelegate{
    
    @IBOutlet weak var rounddiscoveryview: UIView!
    @IBOutlet weak var discoveryimage: UIImageView!
    @IBOutlet weak var discoveryview: UIView!
    @IBOutlet weak var tableview: UITableView!
    
    let kMaxRadius: CGFloat = 200
    let kMaxDuration: TimeInterval = 10
    
    var dataUpdateTimer: Timer?
    let button = UIButton()
    let pulsator = Pulsator()
    
    var discoveryManager: WFDiscoveryManager?
    var discoveredDevices = [WFDeviceInformation]()
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.layer.layoutIfNeeded()
        pulsator.position =  rounddiscoveryview.layer.position
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.navigationController?.title = "Bluetooth Devices"
        rounddiscoveryview.layer.superlayer?.insertSublayer(pulsator, below:rounddiscoveryview.layer)
        setupInitialValues()
        pulsator.start()
        
        savesharedprefrence(key:"heartrate", value:"false")
        
    }
    
    override func viewWillAppear(_ animated: Bool){
        self.discoveryview.isHidden = true
        //startDiscovery()
    }
    
    override func viewWillDisappear(_ animated: Bool){
        //cancelDiscovery()
    }
    
    @IBAction func bluetoothbuttonaction(_ sender: Any){
        startDiscovery()
        self.discoveryview.isHidden = false
    }
    
    @IBAction func closebuttonaction(_ sender: Any){
        cancelDiscovery()
        self.discoveryview.isHidden = true
    }
    
    private func setupInitialValues(){
        pulsator.numPulse = 5
        pulsator.radius = CGFloat(1.0) * kMaxRadius
        pulsator.backgroundColor = UIColor.red.cgColor
        pulsator.animationDuration = Double(0.5) * kMaxDuration
        pulsator.backgroundColor = UIColor(red: CGFloat(1.0),green: CGFloat(1.0),blue: CGFloat(1.0),alpha: CGFloat(1)).cgColor
    }
    
    func discoveryManager(_ discoveryManager: WFDiscoveryManager!, didDiscoverDevice deviceInformation: WFDeviceInformation!){
        if self.discoveredDevices.contains(deviceInformation) == false{
            self.discoveredDevices.append(deviceInformation)
            self.tableview.beginUpdates()
            self.discoveryview.isHidden = true
            self.tableview.insertRows(at: [IndexPath(row: discoveredDevices.count - 1, section: 0)], with:.automatic)
            self.tableview.endUpdates()
        }
    }
    

    func startDiscovery(){
        print("startDiscovery")
        if !self.discoveredDevices.isEmpty{
            self.discoveredDevices = [WFDeviceInformation]()
            self.tableview.reloadData()
        }
      
        func sensorTypes() -> [Any]?{
            return nil
        }
        
        if self.discoveryManager == nil{
           self.discoveryManager = WFDiscoveryManager()
           self.discoveryManager?.delegate = self
        }
        self.discoveryManager?.discoverSensorTypes(sensorTypes(), on:WF_NETWORKTYPE_ANY)
    }
    
    func cancelDiscovery(){
        print("cancelDiscovery")
        self.discoveryManager?.cancelDiscovery()
    }
    
    func discoveryManager(_ discoveryManager: WFDiscoveryManager!, didLooseDevice deviceInformation: WFDeviceInformation!){

//        let index: Int = discoveredDevices.index(of:deviceInformation)!
//        //index(of: deviceInformation)
//        discoveredDevices.removeAll(where:
//        {
//                element in element == deviceInformation
//        })
//
//        self.tableview.beginUpdates()
//        self.tableview.deleteRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
//        self.tableview.endUpdates()
        if let index: Int = discoveredDevices.index(of:deviceInformation){
            discoveredDevices.removeAll(where:{
                    element in element == deviceInformation
            })
            self.tableview.beginUpdates()
            self.tableview.deleteRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
            self.tableview.endUpdates()
        }        
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.discoveredDevices.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 140
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SensorCell", for: indexPath) as! SensorCell
        cell.selectionStyle = .none
        
        let deviceInformation:WFDeviceInformation = self.discoveredDevices[indexPath.row]
        print(deviceInformation)
        cell.connectbutton.tag = indexPath.row
        cell.namelabel.text = deviceInformation.name
        
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from:deviceInformation.lastUpdate) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd-MMM-yyyy"
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        
        cell.connectionStatusLabel.text = myStringafd
        print(deviceInformation)
        cell.deviceInformation = deviceInformation
        
        return cell
        
    }
}

