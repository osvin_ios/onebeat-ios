//
//  UserWorkoutClass.swift
//  OneBeat
//
//  Created by osvinuser on 26/12/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//


import Foundation
import Gloss

class UserWorkoutClass:JSONDecodable{
    
     var join_id:NSString? = ""
     var user_id:NSString? = ""
     var workout_id:NSString? = ""
     var workout_point:NSString? = ""
     var attendee_workout_time:NSString? = ""
     var avg_heart_rate:NSString? = ""
     var workout_time:NSString? = ""
     var total_Workout_Time:NSString? = ""
     var total_Workouts: NSString? = ""
    
    required init?(json: JSON){
        
        self.join_id = "join_id" <~~ json
        self.user_id = "user_id" <~~ json
        self.workout_id = "workout_id" <~~ json
        self.workout_point = "workout_point" <~~ json
        self.attendee_workout_time = "attendee_workout_time" <~~ json
        self.avg_heart_rate = "max_heart_rate" <~~ json
        self.workout_time = "workout_time" <~~ json
        self.total_Workout_Time = "total_Workout_Time" <~~ json
        self.total_Workouts = "total_Workouts" <~~ json
        
    }
}
