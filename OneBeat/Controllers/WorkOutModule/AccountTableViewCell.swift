//
//  AccountTableViewCell.swift
//  OneBeat
//
//  Created by osvinuser on 14/12/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//

import UIKit

class AccountTableViewCell: UITableViewCell {

    @IBOutlet weak var indicatorImg: UIImageView!
    @IBOutlet weak var pushnotificationswitch: UISwitch!
    @IBOutlet weak var titlelabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
