//
//  Workoutdetails.swift
//  OneBeat
//
//  Created by osvinuser on 20/12/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//

import Foundation
import Gloss

class Workoutdetailslive:JSONDecodable{
    
    var workout_id: String? = ""
    var tittle:String? = ""
    var start_time:String? = ""
    var end_time:String? = ""
    var workout_date:String? = ""
    var duration:String? = ""
    var description:String? = ""
    var meeting_id:String? = ""
    var start_url:String? = ""
    var join_url:String? = ""
    var catName:String? = ""
    var userName:String? = ""
    var userImage:String? = ""
    
    var title:String? = ""
    var user_id:String? = ""
    var workout_image:String? = ""
    var cat_name:String? = ""
    var trainer_name:String? = ""
    var trainer_image:String? = ""
    
    required init?(json: JSON) {
        
        self.workout_id = "workout_id" <~~ json
        self.tittle = "tittle" <~~ json
        self.start_time = "start_time" <~~ json
        self.end_time = "end_time" <~~ json
        self.workout_date = "workout_date" <~~ json 
        self.duration = "duration" <~~ json
        self.description = "description" <~~ json
        self.meeting_id = "meeting_id" <~~ json
        self.start_url = "start_url" <~~ json
        self.join_url = "join_url" <~~ json
        self.catName = "catName" <~~ json
        self.userName = "userName" <~~ json
        self.userImage = "userImage" <~~ json
        
        self.title          = "title" <~~ json
        self.user_id        = "user_id" <~~ json
        self.workout_image   = "workout_image" <~~ json
        self.cat_name       = "cat_name" <~~ json
        self.trainer_name    = "trainer_name" <~~ json
        self.trainer_image   = "trainer_image" <~~ json
        
    }
    
}
