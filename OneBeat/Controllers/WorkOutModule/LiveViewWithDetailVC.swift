//
//  LiveViewWithDetailVC.swift
//  OneBeat
//
//  Created by osvinuser on 29/03/19.
//  Copyright © 2019 osvinuser. All rights reserved.
//

import UIKit

class LiveViewWithDetailVC: UIViewController {
    
    @IBOutlet weak var liveWorkoutVideoView: UIView!
    @IBOutlet weak var workoutNameLabel: UILabel!
    @IBOutlet weak var workoutTimeLabel: UILabel!
    @IBOutlet weak var bpmCountLabel: UILabel!
    @IBOutlet weak var pointsCountLabel: UILabel!
    @IBOutlet weak var PercentageCountLabel: UILabel!
    @IBOutlet weak var leaderboardTableView: UITableView!
    @IBOutlet weak var caloriesCountLabel: NZLabel!
    
    @IBOutlet weak var seeAllBtn: UIButton!
    @IBOutlet weak var videoView_Height_Constant: NSLayoutConstraint!
    //Constraints:
    @IBOutlet weak var leaderboardTableViewHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var total_Time_Spent_Label: UILabel!
    @IBOutlet weak var mainS_creen_CrossBtn: UIButton!
    @IBOutlet weak var full_Screeen_Cross_Btn: UIButton!
    @IBOutlet weak var fullScreen_Btn: UIButton!
    @IBOutlet weak var lowerStats_View: UIView!
    @IBOutlet weak var fullView_Hr_label: UILabel!
    @IBOutlet weak var fullView_Points_label: UILabel!
    @IBOutlet weak var fullView_Calories_label: UILabel!
    @IBOutlet weak var fullView_MaxHR_label: UILabel!
    @IBOutlet weak var fullView_Timer_label: UILabel!
    @IBOutlet weak var timer_View: UIView!
    
    @IBOutlet weak var fullView_LeaderBoard_TableView: UITableView!
    let MobileRTCActiveVideoView1 = MobileRTCActiveVideoView()
    var discoveryManager: WFDiscoveryManager?
    var discoveredDevices = [WFDeviceInformation]()
    var deviceInformation: WFDeviceInformation?
    var sensorConnections = [WFSensorConnection]()
    var sensorConnectionLookup = [String:WFSensorConnection]()
    var getWorkoutListingData:Getworkout?
    var time = String()
    var User_age = Double()
    var points = Int()
    var heart_Rate = Double()
    var leaderBoardUsersListingData = [GetUsersPointsDetailClass]()
    var topFiveListingData = [DetailTopFiveClass]()
    var serverDateTimeStr : String?
    var serverDateInDateFormat : Date?
    var countingTimer: Timer?
    var tickCount = 0
    let tickRate = 1.0
    let totalTicks = 60
    var isHeartRateValueUpdate = false
    var maxPercentage = Double()
    var MaxHRTimer = 0
    var prevCal = Int()
    var preCalRemain = Int()
    var total_Spend_Time_Count =  Int()
    var total_Time = Int()
    var pushData = String()
    var meetingId : Int?
    var workoutId = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.cancelDiscovery()
        self.startDiscovery()
        points = 0
        let getAge = getSharedPrefrance(key: "dob")
        getUser_Workout_Detail()
        calcAge(age: getAge)
        self.leaderboardTableView.register(UINib(nibName: "LiveLeaderboardTableViewCellXib", bundle: nil), forCellReuseIdentifier: "LiveLeaderboardTableViewCellXib")

        time = UserDefaults.standard.value(forKey: "duration") as? String ?? ""
        workoutNameLabel.text = ""
        self.workoutTimeLabel.text = time
//        let duration = time.components(separatedBy: " ")
//        let durationTime = duration[0]
//        time = (durationTime as? String)!
        UserDefaults.standard.removeObject(forKey: "duration")
    }
    
    func calcAge(age: String) {
        let myFormatte = DateFormatter()
        myFormatte.dateFormat = "dd/MM/yyyy"
        let finalDate : Date = myFormatte.date(from: age)!
        let now = Date()
        let calendar = Calendar.current
        let ageComponents = calendar.dateComponents([.year], from: finalDate, to: now)
        User_age = Double("\(ageComponents.year!)") as! Double
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = true
        navigationController?.setNavigationBarHidden(true, animated: false)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTapFrom(recognizer:)))
        MobileRTCActiveVideoView1.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: liveWorkoutVideoView.frame.height)
        MobileRTCActiveVideoView1.backgroundColor = UIColor.white
        //MobileRTCActiveVideoView1.center = liveWorkoutVideoView.center
        liveWorkoutVideoView.addSubview(MobileRTCActiveVideoView1)

        MobileRTCActiveVideoView1.clipsToBounds = true
        MobileRTCActiveVideoView1.layoutIfNeeded()
        liveWorkoutVideoView.layoutIfNeeded()
       
        MobileRTCActiveVideoView1.addGestureRecognizer(tapGestureRecognizer)
        _ = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: { result in
            self.startDiscovery()
        })
    }
   
    override func viewWillDisappear(_ animated: Bool){
        super.viewWillDisappear(true)
        self.tabBarController?.tabBar.isHidden = false
        navigationController?.setNavigationBarHidden(false, animated: false)
        //countingTimer?.invalidate() // Destroy timer.
    }
   
    @objc func handleTapFrom(recognizer : UITapGestureRecognizer)
    {
        if lowerStats_View.isHidden == true {
            UIView.animate(withDuration: 2.0, animations:  {
                self.lowerStats_View.isHidden = false
            })
        }
        else {
            UIView.animate(withDuration: 2.0, animations: {
                self.lowerStats_View.isHidden = true
            })
        }
        if fullView_LeaderBoard_TableView.isHidden == true {
            UIView.animate(withDuration: 2.0, animations:  {
                self.fullView_LeaderBoard_TableView.isHidden = false
            })
        }
        else {
            UIView.animate(withDuration: 2.0, animations:  {
                self.fullView_LeaderBoard_TableView.isHidden = true
            })
        }
    }
    
    func setValue() {
        
        if checkPushData == true {
            //checkPushData = false
            self.workoutTimeLabel.text = time
            self.workoutNameLabel.text = workoutTitle
        } else {
        self.workoutTimeLabel.text = time
        self.workoutNameLabel.text = getWorkoutListingData?.title ?? ""
        }
        points = Int(getWorkoutListingData?.points ?? "") ?? 0
        self.bpmCountLabel.text = getWorkoutListingData?.heart_rate ?? ""
        self.pointsCountLabel.text = getWorkoutListingData?.points ?? ""
        self.fullView_Points_label.text = getWorkoutListingData?.points ?? ""
        self.fullView_Calories_label.text = getWorkoutListingData?.calories ?? ""
        self.caloriesCountLabel.text = getWorkoutListingData?.calories ?? ""
        let total_Time_spent = Int(getWorkoutListingData?.attendee_Workout_time ?? "") ?? 0
        total_Spend_Time_Count = total_Time_spent + tickCount
        prevCal = Int(getWorkoutListingData?.calories ?? "") ?? 0
        preCalRemain = prevCal % 100
        printSecondsToHoursMinutesSeconds()
    
    }
    
    func printSecondsToHoursMinutesSeconds () -> () {
        total_Time = total_Spend_Time_Count + tickCount
        let (h, m, s) = secondsToHoursMinutesSeconds (seconds: total_Time)
        self.total_Time_Spent_Label.text = "\(h): \(m): \(s)"
        self.fullView_Timer_label.text = "\(h): \(m): \(s)"
    }
    
    @IBAction func mainScreen_crossButtonPressed(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: "OneBeat", message: "Do you want an workout summary of email.", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "YES", style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            self.hit_Email_Api()
            MobileRTC.shared()?.getMeetingService()?.leaveMeeting(with: LeaveMeetingCmd_End)
            self.cancelDiscovery()
            self.countingTimer?.invalidate()// Destroy timer.
            self.dismiss(animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "NO", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
//            self.hit_Email_Api()
//            MobileRTC.shared()?.getMeetingService()?.leaveMeeting(with: LeaveMeetingCmd_End)
//            self.cancelDiscovery()
//            self.countingTimer?.invalidate()// Destroy timer.
//            self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
//        MobileRTC.shared()?.getMeetingService()?.leaveMeeting(with: LeaveMeetingCmd_End)
//        self.cancelDiscovery()
//        countingTimer?.invalidate()// Destroy timer.
//        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func fullScreenCrossBtnPressed(_ sender: UIButton) {
        
        fullView_LeaderBoard_TableView.isHidden = true
        fullScreen_Btn.isHidden = false
        videoView_Height_Constant.constant = 224
        mainS_creen_CrossBtn.isHidden = false
        full_Screeen_Cross_Btn.isHidden = true
        
        appDelegate.deviceOrientation = .portrait
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        MobileRTCActiveVideoView1.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: liveWorkoutVideoView.frame.height)
        MobileRTCActiveVideoView1.clipsToBounds = true
        MobileRTCActiveVideoView1.layoutIfNeeded()
        liveWorkoutVideoView.layoutIfNeeded()
        lowerStats_View.isHidden = true
        timer_View.isHidden = true
        MobileRTCActiveVideoView1.isUserInteractionEnabled = false
    }
    
    @IBAction func see_All_Btn(_ sender: UIButton) {
        let workOutId = UserDefaults.standard.value(forKey: "workOutId") as? String ?? ""
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let VC = storyboard.instantiateViewController(withIdentifier: "SeeAllUseListingViewController") as! SeeAllUseListingViewController
        VC.workout_Id = workOutId
       self.present(VC, animated: true, completion: nil)
    }
    
    func startDiscovery(){
        print("startDiscovery")
        if !self.discoveredDevices.isEmpty{
            self.discoveredDevices = [WFDeviceInformation]()
            //self.tableview.reloadData()
            self.updateUI()
        }
        
        func sensorTypes() -> [Any]?{
            return nil
        }
        
        if self.discoveryManager == nil{
            self.discoveryManager = WFDiscoveryManager()
            self.discoveryManager?.delegate = self
        }
        self.discoveryManager?.discoverSensorTypes(sensorTypes(), on:WF_NETWORKTYPE_ANY)
    }
    
    func cancelDiscovery(){
        print("cancelDiscovery")
        self.discoveryManager?.cancelDiscovery()
    }
    
    func updateUI(){
        
        if self.discoveredDevices.count > 0{
            let deviceInformation:WFDeviceInformation = self.discoveredDevices[0]
            self.deviceInformation = deviceInformation
           
        }
        
        self.connect()
        countingTimer?.invalidate()
        self.startTimer()
        
    }
    
    func startTimer(){
        // Create and configure the timer for 1.0 second ticks.
        countingTimer = Timer.scheduledTimer(timeInterval: tickRate, target: self, selector: #selector(onTimerTick), userInfo: "Tick: ", repeats: true)
        // Make the timer efficient.
        countingTimer?.tolerance = 0.15
        // Helps UI stay responsive even with timer.
        RunLoop.current.add(countingTimer!, forMode: RunLoop.Mode.common)
    }
    
    @objc func onTimerTick(timer: Timer) -> Void {
        tickCount += 1
        updateConnectionUI()
        updateCellsheartbeat()
    }
    
    func calculateMaxHR() -> Double{
        let difference = 220 - User_age
        let Max_rate = (heart_Rate/difference) * 100
        return Max_rate
    }
    
    func calculateCal(){
        
            var weight_In_kg: Double
            let weight = Double(getSharedPrefrance(key: "weight"))
            let weightType = getSharedPrefrance(key: "weight_type")
            if weightType == "Lbs" {
                weight_In_kg = 0.4536 * weight!
            }else {
                weight_In_kg = weight ?? 0
            }
            let gender = getSharedPrefrance(key: "gender")
            if gender == "Male" {
                
                let finalValue = ((-55.0969 + (0.6309 * heart_Rate) + (0.1988 * weight_In_kg) + (0.2017 * User_age)) / 4.184) * 0.166667
                
                let valueAfterPointWithOnePlace = finalValue.truncate(places: 1)
                let valueAfterPointStr = "\(valueAfterPointWithOnePlace)"
                let valueAfterPointStrArr = valueAfterPointStr.split(separator: ".")
                
                if let valueAfterPoint = Int(valueAfterPointStrArr[1]){
                    if valueAfterPoint >= 8{
                        print(finalValue.rounded(.up))
                        let absFinalValue = abs(Int(finalValue.rounded(.up)))
                        self.prevCal += absFinalValue
                        self.preCalRemain += absFinalValue
                    }else{
                        print(finalValue.rounded(.down))
                        let absFinalValue = abs(Int(finalValue.rounded(.down)))
                        self.prevCal += absFinalValue
                        self.preCalRemain += absFinalValue
                    }
                    
                    if self.preCalRemain >= 100{
                        let point = self.preCalRemain / 100
                        self.points += point
                        self.pointsCountLabel.text = "\(self.points)"
                        self.fullView_Points_label.text = "\(self.points)"
                        self.showToast(message: "point added due to calories")
                        self.preCalRemain = self.preCalRemain % 100
                    }
                    fullView_Calories_label.text = "\(self.prevCal)"
                    caloriesCountLabel.text = "\(self.prevCal)"
                }
                
            }else {
                
                let finalValue = ((-20.4022 + (0.4472 * heart_Rate) - (0.1263 * weight_In_kg) + (0.074 * User_age)) / 4.184) * 0.166667
                let valueAfterPointWithOnePlace = finalValue.truncate(places: 1)
                let valueAfterPointStr = "\(valueAfterPointWithOnePlace)"
                let valueAfterPointStrArr = valueAfterPointStr.split(separator: ".")
                
                if let valueAfterPoint = Int(valueAfterPointStrArr[1]){
                    if valueAfterPoint >= 8{
                        print(finalValue.rounded(.up))
                        let absFinalValue = abs(Int(finalValue.rounded(.up)))
                        self.prevCal += absFinalValue
                        self.preCalRemain += absFinalValue
                    }else{
                        print(finalValue.rounded(.down))
                        let absFinalValue = abs(Int(finalValue.rounded(.down)))
                        self.prevCal += absFinalValue
                        self.preCalRemain += absFinalValue
                    }
                    
                    if self.preCalRemain >= 100{
                        let p = self.preCalRemain / 100
                        self.points += p
                        self.pointsCountLabel.text = "\(self.points)"
                        self.fullView_Points_label.text = "\(self.points)"
                        //self.showToast(message: "point added due to calories")
                        self.preCalRemain = self.preCalRemain % 100
                    }
                    caloriesCountLabel.text = "\(self.prevCal)"
                    fullView_Calories_label.text = "\(self.prevCal)"
                }
            }
        }
    
    func Calculate_Calories() {
        var weight_In_kg: Double
        let weight = Double(getSharedPrefrance(key: "weight"))
        let weightType = getSharedPrefrance(key: "weight_type")
        if weightType == "Lbs" {
            weight_In_kg = 0.4536 * weight!
        }else {
            weight_In_kg = weight ?? 0
        }
    }
    
    func connect(){
        
        print("WFSensorTVC::connect")
        var connectionParams = [WFConnectionParams]()
        print("connection----parameter", connectionParams)
//        connectionParams = self.deviceInformation?.connecitonParamsForAllSupportSensorTypes() as! [WFConnectionParams]
        if let WFConnectionParams1 = self.deviceInformation?.connecitonParamsForAllSupportSensorTypes() as? [WFConnectionParams]{
            connectionParams = WFConnectionParams1
        }
        var connectionLookup = [String:WFSensorConnection]()
        
        for params: WFConnectionParams in connectionParams {
            let error: Error? = nil
          print("parameterss---",params)
            let sensorConnection: WFSensorConnection? = try? WFHardwareConnector.shared().requestSensorConnection(params, withProximity: WF_PROXIMITY_RANGE_DISABLED)
            if error != nil {
                if let anError = error {
                    print("ERROR: Failed to create sensor connection! \n\(anError)")
                }
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                    self.connect()
                })
            }else{
                 print("sensorytypes----",params.sensorType)
                connectionLookup["\(params.sensorType)"] = sensorConnection
            }
        }
        
        print("conection values---",connectionLookup.values)
        self.sensorConnections = Array(connectionLookup.values)
        print("coneection cont------",self.sensorConnections.count)
        self.sensorConnectionLookup = connectionLookup
        updateConnectionUI()
    }
    
    @IBAction func fullScreen_Btn(_ sender: UIButton){
        fullView_LeaderBoard_TableView.isHidden = false
        MobileRTCActiveVideoView1.isUserInteractionEnabled = true
        timer_View.isHidden = false
        lowerStats_View.isHidden = false
        fullScreen_Btn.isHidden = true
        mainS_creen_CrossBtn.isHidden = true
        full_Screeen_Cross_Btn.isHidden = false
        
        appDelegate.deviceOrientation = .landscapeLeft
        let value = UIInterfaceOrientation.landscapeLeft.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        self.videoView_Height_Constant.constant = UIScreen.main.bounds.height
        MobileRTCActiveVideoView1.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        savesharedprefrence(key:"heartrate", value:"true")
        MobileRTCActiveVideoView1.clipsToBounds = true
        MobileRTCActiveVideoView1.layoutIfNeeded()
        liveWorkoutVideoView.layoutIfNeeded()
        MobileRTCActiveVideoView1.addSubview(fullView_LeaderBoard_TableView)
        MobileRTCActiveVideoView1.addSubview(lowerStats_View)
        MobileRTCActiveVideoView1.addSubview(timer_View)
    }
    
    func updateConnectionUI(){
        
        if self.sensorConnections.isEmpty{
            
        }else{
            let status:WFSensorConnectionStatus_t = SensorConnectionHelper.combindedSensorStatus(fromConnections:self.sensorConnections)
            self.deviceInformation = self.sensorConnections.first?.deviceInformation
            if status == WF_SENSOR_CONNECTION_STATUS_CONNECTED {
                switch status{
                case WF_SENSOR_CONNECTION_STATUS_CONNECTED:
                    //"Connected"
                    savesharedprefrence(key:"heartrate", value:"true")
                    break
                case WF_SENSOR_CONNECTION_STATUS_CONNECTING:
                    //"Connecting"
                    savesharedprefrence(key:"heartrate", value:"false")
                    break
                case WF_SENSOR_CONNECTION_STATUS_DISCONNECTING:
                    //"Disconnecting"
                    savesharedprefrence(key:"heartrate", value:"false")
                    break
                case WF_SENSOR_CONNECTION_STATUS_INTERRUPTED:
                    //"Interrupted"
                    savesharedprefrence(key:"heartrate", value:"false")
                    break;
                case WF_SENSOR_CONNECTION_STATUS_IDLE:
                    //"Waiting"
                    savesharedprefrence(key:"heartrate", value:"false")
                    break
                default:
                    //"Waiting"
                    savesharedprefrence(key:"heartrate", value:"false")
                }
            }else if status != WF_SENSOR_CONNECTION_STATUS_CONNECTED {
                
            }else{
                switch status{
                case WF_SENSOR_CONNECTION_STATUS_CONNECTED:
                    //"Connected"
                    savesharedprefrence(key:"heartrate", value:"true")
                    break
                case WF_SENSOR_CONNECTION_STATUS_CONNECTING:
                    //"Connecting"
                   // savesharedprefrence(key:"heartrate", value:"false")
                    break
                case WF_SENSOR_CONNECTION_STATUS_DISCONNECTING:
                    //"Disconnecting"
                    //savesharedprefrence(key:"heartrate", value:"false")
                    break
                case WF_SENSOR_CONNECTION_STATUS_INTERRUPTED:
                    //"Interrupted"
                   // savesharedprefrence(key:"heartrate", value:"false")
                    break;
                case WF_SENSOR_CONNECTION_STATUS_IDLE:
                    //"Waiting"
                   // savesharedprefrence(key:"heartrate", value:"false")
                    break
                default:
                    //"Waiting"
                    savesharedprefrence(key:"heartrate", value:"false")
                }
            }
        }
    }
    
    @objc func updateCellsheartbeat(){
        
        let heartrateboolean:String =  getSharedPrefrance(key:"heartrate")
        
        if heartrateboolean == "true"{
            
            let updateArray = self.deviceInformation?.supportedSensorTypes
            
            if updateArray != nil{
                //for result:Int in updateArray{
                for result in updateArray!{
                    
                    //let sensorType = WFSensorType_t(UInt(result))
                    let sensorType = wfSensorTypeFromNSNumber((result as! NSNumber))
                    
                    print(sensorType)
                    
                    switch sensorType{
                        
                    case WF_SENSORTYPE_BIKE_POWER:
                        //---- Bike Speed and/or Cadence
                        //---- Including KICKR, inRide and other power meters
                        break
                    case WF_SENSORTYPE_BIKE_SPEED_CADENCE:
                        //---- Bike Speed and/or Cadence
                        break
                    case WF_SENSORTYPE_FOOTPOD:
                        //---- Foodpods / Running Speed & Cadence sensors
                        break
                    case WF_SENSORTYPE_HEARTRATE:
                        //---- Heartrate
                        
                        let i = result
                        
                        if let connection:WFHeartrateConnection = self.sensorConnectionLookup["\(wfSensorTypeFromNSNumber((i as! NSNumber)))"] as? WFHeartrateConnection{
                            
                            if let data:WFHeartrateData = connection.getHeartrateData(){                                
                                self.bpmCountLabel.text = data.formattedHeartrate(true) ?? "N/A"
                                let BPM = self.bpmCountLabel.text?.components(separatedBy: " ")
                               
                                var bpm : Double
                                bpm = Double(BPM![0]) as! Double
                                heart_Rate = bpm
                                self.bpmCountLabel.text = "\(bpm)"
                                self.fullView_Hr_label.text = "\(bpm)"
                                maxPercentage = calculateMaxHR()
                                PercentageCountLabel.text = "\(Int(maxPercentage))"
                                fullView_MaxHR_label.text = "\(Int(maxPercentage))"
                                print("maxHrTimer=",MaxHRTimer)
                                print("tickCount=",tickCount)
                                
                                if maxPercentage >= 84 {
                                    MaxHRTimer += 1
                                    } else{
                                        MaxHRTimer = 0
                                }
                                if MaxHRTimer == 60 {
                                    self.points += 1
                                    pointsCountLabel.text = "\(self.points)"
                                    fullView_Points_label.text = "\(self.points)"
                                    MaxHRTimer = 0
                                    self.showToast(message: "point added due to MaxHR")
                                }
                                if tickCount % 10 == 0 {
                                    self.calculateCal()
                                }
                                if tickCount % 5 == 0 {
                                    self.update_User_Points()
                                }
                                printSecondsToHoursMinutesSeconds()
                            }
                        }
                        
                        break
                    case WF_SENSORTYPE_WEIGHT_SCALE:
                        //---- Weight Scale
                        break
                    case WF_SENSORTYPE_DISPLAY:
                        //---- Display / RFLKT / Echo / Timex
                        break
                    case WF_SENSORTYPE_BIKE_SPEED:
                        //---- ANT+ Speed Only
                        //---- All BTLE Speed and/or Cadence sensors use the Bike Speed and Cadence Profile
                        break
                    case WF_SENSORTYPE_BIKE_CADENCE:
                        //---- ANT+ Cadence Only
                        //---- All BTLE Speed and/or Cadence sensors use the Bike Speed and Cadence Profile
                        break
                    default:
                        break
                        
                    }
                    printSecondsToHoursMinutesSeconds()
                }
               
            }else{
                // printSecondsToHoursMinutesSeconds()
            } 
            
        }else{
            self.bpmCountLabel.text = "0"
            self.PercentageCountLabel.text = "0"
           // printSecondsToHoursMinutesSeconds()
        }
    }
    
    func serverTime(){
        if self.serverDateTimeStr != nil && self.serverDateTimeStr != ""{
            let serverDateString = self.UTCToLocalStringValue(date: self.serverDateTimeStr ?? "", incomingDateFormat: "yyyy-MM-dd HH:mm:ss", outgoingDateFormat: "yyyy-MM-dd HH:mm:ss")
            
            serverDateInDateFormat = self.getDateFormatterDate(date: serverDateString, incomingDateFormat: "yyyy-MM-dd HH:mm:ss")
            
        }else{}
    }
    
    func getUser_Workout_Detail(){
         let workOutId = UserDefaults.standard.value(forKey: "workOutId") as? String ?? ""
        self.view.hideToastActivity()
        let parameter: [String:Any] = ["workout_id" : workOutId,
                                       "user_id" :getSharedPrefrance(key: "id")]
        //"http://184.73.56.196/Api/Workout/workoutDetails"
        executePOST(view: self.view, path: Constants.APIs.LIVEURL+Constants.APIs.getUserworkoutDetail, parameter: parameter){ response in
            print(response)
            let status = response["ResponseCode"].stringValue
            
            if(status == "true"){
                
                self.getWorkoutListingData = Getworkout.init(json:response["WorkoutJoinResponse"].dictionaryObject!)
                
                let topFiveList = response["WorkoutJoinResponse"]
                
                for data in topFiveList["top_highest_scorer"].arrayValue{
                    self.leaderBoardUsersListingData.append(GetUsersPointsDetailClass(json:data.dictionaryObject!)!)
                }
                
                let serverDateTime = response["Server_time"].stringValue
                if serverDateTime.count != 0{
                    self.serverDateTimeStr = serverDateTime
                    self.serverTime()
                }
                self.setValue()
                self.leaderboardTableView.reloadData()
                self.fullView_LeaderBoard_TableView.reloadData()
                
            }else{
                self.showToast(message:response["MessageWhatHappen"].stringValue)
            }
        }
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    func update_User_Points(){
        
         self.view.hideToastActivity()
         let workOutId = UserDefaults.standard.value(forKey: "workOutId") as? String ?? ""
         let parameter: [String:Any] = ["workout_id" : workOutId,
                         "user_id"  : getSharedPrefrance(key: "id"),
                         "attendee_workout_time" : "5",
                         "points" :  points,
                         "heart_rate" : "\(heart_Rate)",
                         "calories": caloriesCountLabel.text!]
        
        executePOST_WithoutToast(view: self.view, path: Constants.APIs.LIVEURL+"/Api/Workout/savePoints", parameter: parameter){ response in
            print(response)
            let status = response["ResponseCode"].stringValue
            if(status == "true"){
                self.leaderBoardUsersListingData.removeAll()
                for data in response["SavePoints_Response"].arrayValue{
                    self.leaderBoardUsersListingData.append(GetUsersPointsDetailClass(json:data.dictionaryObject!)!)
                }
                self.leaderboardTableView.reloadData()
                self.fullView_LeaderBoard_TableView.reloadData()

            } else {
                self.view.makeToast(response["MessageWhatHappen"].stringValue)
            }
        }
    }
    
    func hit_Email_Api() {
        
        self.view.hideToastActivity()
        let workOutId = UserDefaults.standard.value(forKey: "workOutId") as? String ?? ""
        let parameter: [String:Any] = ["workout_id" : workOutId,
                                       "user_id"  : getSharedPrefrance(key: "id")
                                      ]
        
        executePOST_WithoutToast(view: self.view, path: Constants.APIs.LIVEURL+Constants.APIs.email_Api, parameter: parameter){ response in
            print(response)
            let status = response["ResponseCode"].stringValue
            if(status == "true"){
               
            } else {
               self.view.makeToast(response["MessageWhatHappen"].stringValue)
            }
        }
    }
}

extension LiveViewWithDetailVC:WFDiscoveryManagerDelegate{
    func discoveryManager(_ discoveryManager: WFDiscoveryManager!, didDiscoverDevice deviceInformation: WFDeviceInformation!) {
        if self.discoveredDevices.contains(deviceInformation) == false {
            self.discoveredDevices.append(deviceInformation)
            
//            self.tableview.beginUpdates()
//            self.tableview.insertRows(at: [IndexPath(row: discoveredDevices.count - 1, section: 0)], with:.automatic)
//            self.tableview.endUpdates()
            self.updateUI()
        }
    }
    
    func discoveryManager(_ discoveryManager: WFDiscoveryManager!, didLooseDevice deviceInformation: WFDeviceInformation!) {
        if let index: Int = discoveredDevices.index(of:deviceInformation){
            discoveredDevices.removeAll(where:{
                element in element == deviceInformation
            })
//            self.tableview.beginUpdates()
//            self.tableview.deleteRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
//            self.tableview.endUpdates()
            self.updateUI()
        }
    }
}

extension LiveViewWithDetailVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == leaderboardTableView {
           return  leaderBoardUsersListingData.count
        }
        else {
           return leaderBoardUsersListingData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == leaderboardTableView {
            
            let cell : LiveLeaderboardTableViewCellXib = tableView.dequeueReusableCell(withIdentifier: "LiveLeaderboardTableViewCellXib", for: indexPath) as! LiveLeaderboardTableViewCellXib
            cell.selectionStyle = .none

            let image = self.leaderBoardUsersListingData[indexPath.row].profile_image ?? ""
            if image == "" {
                guard leaderBoardUsersListingData.count != 0 else {return cell}
                if let getImageInfo = self.leaderBoardUsersListingData[indexPath.row].user_Image {

                    cell.userImageView?.sd_setImage(with: URL(string: getImageInfo), placeholderImage: UIImage(named: "ic_sketchy_background") , options: .continueInBackground) { (downloadImage, error, cacheType, urlValue) in
                    }
                }
            } else {
                guard leaderBoardUsersListingData.count != 0 else {return cell}
                if let getImageInfo = self.leaderBoardUsersListingData[indexPath.row].profile_image {

                    cell.userImageView?.sd_setImage(with: URL(string: getImageInfo), placeholderImage: UIImage(named: "ic_sketchy_background") , options: .continueInBackground) { (downloadImage, error, cacheType, urlValue) in
                    }
                }
            }

            cell.userNameLabel.text = self.leaderBoardUsersListingData[indexPath.row].name ?? ""
            cell.pointsLabel.text = self.leaderBoardUsersListingData[indexPath.row].points ?? ""
            return cell
        }
        else {
            let cell = fullView_LeaderBoard_TableView.dequeueReusableCell(withIdentifier: "fullViewCell", for: indexPath) as! FullViewTableVIewCell
            cell.selectionStyle = .none
            
            let image = self.leaderBoardUsersListingData[indexPath.row].profile_image ?? " "
            if image == " " {
                guard leaderBoardUsersListingData.count != 0 else {return cell}
                if let getImageInfo = self.leaderBoardUsersListingData[indexPath.row].user_Image {

                    cell.user_ImageView?.sd_setImage(with: URL(string: getImageInfo), placeholderImage: UIImage(named: "ic_sketchy_background") , options: .continueInBackground) { (downloadImage, error, cacheType, urlValue) in
                    }
                }
            }else {
                guard leaderBoardUsersListingData.count != 0 else {return cell}
                if let getImageInfo = self.leaderBoardUsersListingData[indexPath.row].profile_image {

                    cell.user_ImageView?.sd_setImage(with: URL(string: getImageInfo), placeholderImage: UIImage(named: "ic_sketchy_background") , options: .continueInBackground) { (downloadImage, error, cacheType, urlValue) in
                    }
                }
            }

            cell.user_Name.text = self.leaderBoardUsersListingData[indexPath.row].name ?? ""           
            cell.user_Point_Label.text = self.leaderBoardUsersListingData[indexPath.row].points ?? ""
            return cell
        }
    }
}

extension Double{
    func truncate(places : Int)-> Double{
        return Double(floor(pow(10.0, Double(places)) * self)/pow(10.0, Double(places)))
    }
}
