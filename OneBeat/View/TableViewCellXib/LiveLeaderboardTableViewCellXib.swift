//
//  LiveLeaderboardTableViewCellXib.swift
//  OneBeat
//
//  Created by osvinuser on 01/04/19.
//  Copyright © 2019 osvinuser. All rights reserved.
//

import UIKit

class LiveLeaderboardTableViewCellXib: UITableViewCell {

    @IBOutlet weak var serialNumberLabel: UILabel!
    @IBOutlet weak var userImageView: ImageViewDesign!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var logoImageView: ImageViewDesign!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
