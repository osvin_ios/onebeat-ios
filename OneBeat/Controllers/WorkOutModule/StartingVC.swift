//
//  StartingVC.swift
//  OneBeat
//
//  Created by osvinuser on 13/12/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//

import UIKit

class StartingVC: UIViewController{
    
    @IBOutlet weak var signupbutton: GRButton!
    @IBOutlet weak var signinbutton: GRButton!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        settingui()
    }

    func settingui(){
        signinbutton.cornerRadius = 6.0
        signinbutton.borderColor = UIColor.clear
        signinbutton.clipsToBounds = true
        signupbutton.cornerRadius = 6.0
        signupbutton.borderColor = UIColor.clear
        signupbutton.clipsToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(true)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool){
        super.viewWillDisappear(true)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @IBAction func signupbuttonaction(_ sender: Any){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func signinbuttonaction(_ sender: Any){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SigninVC") as! SigninVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
