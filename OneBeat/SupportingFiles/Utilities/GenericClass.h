//
//  GenericClass.h
//  BitFinex
//
//  Created by Tarun Sachdeva on 6/29/17.
//  Copyright © 2017 Pixel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#include <ifaddrs.h>
#include <arpa/inet.h>


@interface GenericClass : NSObject

+ (id)genericMethod;
-(CGFloat)heightForText:(NSString*)text font:(UIFont*)font withinWidth:(CGFloat)width;
-(void)renderUIObject: (CGFloat)value borderWidth:(CGFloat)width borderColor:(UIColor*)borderColor object:(UIView*)obj;
-(UIAlertController*)showAlertWithMessage :(NSString *)message title:(NSString*)title;
-(BOOL) NSStringIsValidEmail:(NSString *)checkString;
-(NSString*)checkIsNull:(NSString*)str;
-(CGSize)sizeOfText:(NSString*)text font:(UIFont*)font bundingSize:(CGSize)size;
-(void)addBottomLine:(UIControl*)obj;
-(void)addBottomLinediffrentColor:(UIControl*)obj;
-(void)addLeftPaddingToUITextField:(UITextField*)txtField;
- (NSString *)getIPAddress;
-(BOOL)getIdentificationStatus : (NSString*)userID token:(NSString*)identificationToken;
-(void)shadowUIObject : (UIView *)object;
- (NSString *)capitalizeFirstLetterOnlyOfString:(NSString *)string;
-(void)addBottomLineForMode:(UIControl *)obj;
//-(void)setStatusBarBackgroundColor:(UIColor *)color value:(BOOL)ans;
@property (retain,nonatomic) NSString * login;

- (NSString *)sentenceCapitalizedString:(NSString*)str;

- (void)showAnimate:(UIView*)view;

- (void)removeAnimate:(UIView*)view;

-(NSAttributedString*)createattributestring:(UIColor*)color simpleString:(NSString*)simpleString otherString:(NSString*)otherString otherStringColor:(UIColor*)otherStringColor;



@end
