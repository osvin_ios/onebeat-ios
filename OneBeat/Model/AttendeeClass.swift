//
//  AttendeeClass.swift
//  OneBeat
//
//  Created by osvinuser on 11/04/19.
//  Copyright © 2019 osvinuser. All rights reserved.
//

import Foundation
import Gloss

class AttendeeClass:JSONDecodable{
    
    var attendee_workout_time : String? = ""
    var points : String? = ""
    var calories: String? = ""
    var user_image : String? = ""
    var heart_rate : String? = ""
    var user_id: String? = ""
    var user_name: String? = ""
    var workout_id: String? = ""
    var attende_image: String? = ""
    
    required init?(json: JSON) {
        
        self.workout_id = "workout_id" <~~ json
        self.user_id = "user_id" <~~ json
        self.points = "points" <~~ json
        self.heart_rate = "max_heart_rate" <~~ json
        self.attendee_workout_time = "attendee_workout_time" <~~ json
        self.calories = "calories" <~~ json
        self.attende_image = "attendee_image" <~~ json
        
    }
}
