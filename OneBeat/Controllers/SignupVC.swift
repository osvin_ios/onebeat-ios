//
//  SignupVC.swift
//  OneBeat
//
//  Created by osvinuser on 11/12/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//

import UIKit
import FloatingTextField

class SignupVC: UIViewController{
    
    @IBOutlet weak var nextbutton: GRButton!
    @IBOutlet weak var firstnameTF: FloatingTextField!
    @IBOutlet weak var lastnameTF: FloatingTextField!
    @IBOutlet weak var passwordTF: FloatingTextField!
    @IBOutlet weak var emailAddressTF: FloatingTextField!
    
    var tempdic = [SimpleModle]()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        settingui()
    }
    
    func settingui(){
        nextbutton.cornerRadius = 2.0
        nextbutton.borderColor = UIColor.clear
        self.passwordTF.isSecureTextEntry = true
        addNavBarImage()
    }
    
    func settingmethods(){
    
    }
    
    @IBAction func backbuttonaction(_ sender: Any){
        self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func nextbuttonaction(_ sender: Any) {
        if firstnameTF.text == ""{
            showToast(message:"Please Enter First Name")
        }else if lastnameTF.text == ""{
            showToast(message:"Please Enter Last Name")
        }else if(!isValidEmail(testStr: emailAddressTF.text!)) {
            showToast(message:"Please Enter Valid Email Address")
        }else if passwordTF.text == ""{
            showToast(message:"Please Enter Vaild Password")
        }else{
            let simplemodle = SimpleModle.init(firstname:self.firstnameTF.text, password:self.passwordTF.text, lastname:self.lastnameTF.text, emailaddress:self.emailAddressTF.text)
            tempdic.append(simplemodle)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignupSecondVC") as! SignupSecondVC
            vc.signupDic = tempdic
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func isValidEmail(testStr:String) -> Bool{
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }

}
