//
//  SeeAllUseListingViewController.swift
//  OneBeat
//
//  Created by osvinuser on 10/04/19.
//  Copyright © 2019 osvinuser. All rights reserved.
//

import UIKit

class SeeAllUseListingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var UserListing_TableView: UITableView!
    var leaderBoardUsersListingData = [DetailTopFiveClass]()
    var workout_Id = String()
    var page = Int()
    @IBOutlet weak var backBtn: UIButton!
    var checkBtnStatus = String()
    var stopApi = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        stopApi = "no"
        self.tabBarController?.tabBar.isHidden = true
        navigationController?.setNavigationBarHidden(true, animated: true)
        page = 1
        See_All_Users_Api()
        backBtn.addTarget(self, action: #selector(dissmissView), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    @objc func dissmissView() {
        if checkBtnStatus == "Navigation" {
            navigationController?.popViewController(animated: true)
        }else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leaderBoardUsersListingData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UserListing_TableView.dequeueReusableCell(withIdentifier: "SecondCell", for: indexPath) as! HistoryTableViewCell
        cell.selectionStyle = .none
        guard leaderBoardUsersListingData.count != 0 else {return cell}
        
        if let getImageInfo = self.leaderBoardUsersListingData[indexPath.row].user_image {

            cell.second_Cell_user_Image?.sd_setImage(with: URL(string: getImageInfo), placeholderImage: UIImage(named: "ic_sketchy_background") , options: .continueInBackground) { (downloadImage, error, cacheType, urlValue) in
            }
        }
        cell.secondCell_User_Name.text = self.leaderBoardUsersListingData[indexPath.row].user_name ?? ""
        cell.secondCell_User_Points.text = self.leaderBoardUsersListingData[indexPath.row].points ?? ""
        //cell.workout_Name_Label.text =  self.leaderBoardUsersListingData[indexPath.row].workOut_Name ?? ""
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if stopApi == "no" {
            page += 1
            See_All_Users_Api()
        }
        else {
            print("no")
        }
    }
    
    func See_All_Users_Api(){
        
        let parameter: [String:Any] = ["workout_id"  : workout_Id,
                                       "page" : "\(page)"]
        
        executePOST(view: self.view, path: Constants.APIs.LIVEURL+"/Api/Workout/userWorkoutListings", parameter: parameter){ response in
//            print(response)
            let status = response["ResponseCode"].stringValue
            if(status == "true"){
                //self.leaderBoardUsersListingData.removeAll()
                for data in response["userWorkoutListingsResponse"].arrayValue{
                    self.leaderBoardUsersListingData.append(DetailTopFiveClass(json:data.dictionaryObject!)!)
                }
                self.UserListing_TableView.reloadData()
            }else{
                self.stopApi = "yes"
                self.view.makeToast(response["MessageWhatHappen"].stringValue)
            }
        }
    }
}
