//
//  ChangePasswordVC.swift
//  OneBeat
//
//  Created by osvinuser on 14/12/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//

import UIKit
import FloatingTextField
import Alamofire
import SwiftyJSON

class ChangePasswordVC: UIViewController {

    @IBOutlet weak var currentpasswordTF: FloatingTextField!
    @IBOutlet weak var newpasswordTF: FloatingTextField!
    @IBOutlet weak var confirmnewpasswordTF: FloatingTextField!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        self.navigationItem.title = "Change Password"
        
        let backBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(backbuttonaction))
        self.navigationItem.leftBarButtonItem  = backBarButtonItem
        let sliderImage = UIImage(named: "ic_back_black")
        
        navigationItem.leftBarButtonItem?.image = sliderImage
        navigationItem.leftBarButtonItem?.tintColor = UIColor.black
        // Do any additional setup after loading the view.
    }
    
    @objc func backbuttonaction(){
        self.navigationController?.popViewController(animated:false)
    }

    @IBAction func updatebuttonaction(_ sender: Any){
        if currentpasswordTF.text != nil{
             showToast(message:"Please Enter Current Password")
        }else if newpasswordTF.text != nil{
             showToast(message:"Please Enter New Password")
        }else if newpasswordTF.text != confirmnewpasswordTF.text{
             showToast(message:"Your password and confirmation password do not match")
        }else{
            updatepasswordapi()
        }
    }
}

extension ChangePasswordVC{
    
    func updatepasswordapi(){
        
        let currentpassword:String = (self.currentpasswordTF?.text)!
        let newpassword:String = (self.newpasswordTF?.text)!
        let confirmnewpassword:String = (self.confirmnewpasswordTF?.text)!
        
        let parameter:[String:Any] = [
            "oldpassword": currentpassword,
            "newpassword": confirmnewpassword,
            ]
        
        print(parameter)
        
        executePOST(view: self.view, path: Constants.APIs.LIVEURL+"/Api/User/changepassword", parameter: parameter){ response in
            print(response)
            let status = response["ResponseCode"].stringValue
            
            if(status == "true"){
                self.showToast(message:response["MessageWhatHappen"].stringValue)
                savesharedprefrence(key:"id", value:response["changepasswordresponse"]["id"].stringValue)
                savesharedprefrence(key:"first_name", value:response["changepasswordresponse"]["first_name"].stringValue)
                savesharedprefrence(key:"email", value:response["changepasswordresponse"]["email"].stringValue)
                savesharedprefrence(key:"last_name", value:response["changepasswordresponse"]["last_name"].stringValue)
                savesharedprefrence(key:"gender", value:response["changepasswordresponse"]["gender"].stringValue)
                savesharedprefrence(key:"dob", value:response["changepasswordresponse"]["dob"].stringValue)
                savesharedprefrence(key:"weight", value:response["changepasswordresponse"]["weight"].stringValue)
                savesharedprefrence(key:"height", value:response["changepasswordresponse"]["height"].stringValue)
                savesharedprefrence(key:"profile_image", value:response["changepasswordresponse"]["profile_image"].stringValue)
            }else{
                self.showToast(message:response["MessageWhatHappen"].stringValue)
            }
        }
    }
    
}

