//
//  AccountSettingsVC.swift
//  OneBeat
//
//  Created by osvinuser on 14/12/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//

import UIKit

class AccountSettingsVC: UIViewController {

    var accountArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Account Settings"
        
        accountArray = ["Push Notification","Change Password","Terms and Conditions","Privacy Policy", "Subscription"]
        let backBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(backbuttonaction))
        self.navigationItem.leftBarButtonItem  = backBarButtonItem
        let sliderImage = UIImage(named: "ic_back_arrow_black")
        
        navigationItem.leftBarButtonItem?.image = sliderImage
        navigationItem.leftBarButtonItem?.tintColor = UIColor.black
    }
    
    @objc func backbuttonaction(){
        self.navigationController?.popViewController(animated:true)
    }
}

 extension AccountSettingsVC:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return accountArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:AccountTableViewCell = tableView.dequeueReusableCell(withIdentifier:"AccountTableViewCell") as! AccountTableViewCell
        
        if indexPath.row == 0{
            cell.indicatorImg.isHidden = true
            cell.pushnotificationswitch.isHidden = false
            cell.pushnotificationswitch.addTarget(self, action: #selector(AccountSettingsVC.switchStateDidChange(_:)), for: .valueChanged)
        }else{
            cell.indicatorImg.isHidden = false
            cell.pushnotificationswitch.isHidden = true
        }
        cell.titlelabel.text = accountArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            
        } else if indexPath.row == 1 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else if indexPath.row == 4 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MasterViewController") as! AutoRenewable_TableView_Controller
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{}
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 69.0
    }
    
    @objc func switchStateDidChange(_ sender:UISwitch!){
        if (sender.isOn == true){
            print("UISwitch state is now ON")
            pushnotificationapi(status:"1")
        }else{
            print("UISwitch state is now Off")
            pushnotificationapi(status:"2")
        }
    }
}

extension AccountSettingsVC {
    
    //push notification api
    func pushnotificationapi(status:String){
        
        let parameter = [
            "user_id":getSharedPrefrance(key:"id"),
            "status":status
        ]
        
        executePOST(view: self.view, path: Constants.APIs.LIVEURL+"/Api/Workout/postNotification", parameter: parameter){ response in
            print(response)
            let status = response["ResponseCode"].stringValue
            if(status == "true"){
                
                self.view.makeToast(response["MessageWhatHappen"].stringValue)
                savesharedprefrence(key:"postNotificationResponse", value:response["postNotificationResponse"]["notification_status"].string!)
                let postNotificationResponse = PostNotificationClass.init(json:response["postNotificationResponse"].dictionaryObject!)
                savesharedprefrence(key:"postNotificationResponse", value:postNotificationResponse!.notification_status! as String)
            }else{
                self.view.makeToast(response["MessageWhatHappen"].stringValue)
            }
        }
    }    
}
