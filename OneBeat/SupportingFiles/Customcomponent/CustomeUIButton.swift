//
//  CustomeUIButton.swift
//  GetBlushh
//
//  Created by Arvind Mehta on 29/12/17.
//  Copyright © 2017 Arvind Mehta. All rights reserved.
//

import Foundation
import UIKit
class CustomUIButton: UIButton{
    
}
extension UIButton {
    
    private struct ButtonTag {
        static var tagKey = "key"
    }
    
    var myTag : (Int , Int)? {
        
        set {
            if let value = newValue {
                objc_setAssociatedObject(self, &ButtonTag.tagKey, value as! AnyObject, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
        
        get {
            return objc_getAssociatedObject(self, &ButtonTag.tagKey) as? (Int , Int)
        }
    }
}
