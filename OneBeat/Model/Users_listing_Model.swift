//
//  Users_listing_Model.swift
//  OneBeat
//
//  Created by osvinuser on 08/07/19.
//  Copyright © 2019 osvinuser. All rights reserved.
//
//

import Foundation
import Gloss

class Users_listing_Model:JSONDecodable {
    
    var user_id: String? = ""
    var profile_image: String? = ""
    var user_Name : String? = ""
    var height : String? = ""
    var location : String? = ""
    var weight : String? = ""
    var likes : String? = ""
    var followers : String? = ""
    var points : String? = ""
    var time : String? = ""
    var workout : String? = ""
    var maxhr : String? = ""
    var add_Friend_LikeStatus : String? = ""
    var follow_Status : String? = ""
    var total_Friends : String? = ""
    
    required init?(json: JSON) {
        
     self.user_id = "id" <~~ json
     self.user_Name = "username" <~~ json
     self.profile_image = "profile_image" <~~ json
     self.height = "profile_image" <~~ json
     self.location = "location" <~~ json
     self.weight = "weight" <~~ json
     self.workout = "total_workout" <~~ json
     self.followers = "followers" <~~ json
     self.points = "points" <~~ json
     self.time = "attendee_workout_time" <~~ json
     self.maxhr = "heart_rate" <~~ json
     self.likes = "likes" <~~ json
     self.add_Friend_LikeStatus = "friend_status" <~~ json
     self.follow_Status = "follow_status" <~~ json
     self.total_Friends = "no_of_friends" <~~ json
        
    }
}


