//
//  OneBeatProduct
//  OneBeat
//
//  Created by osvinuser on 14/06/19.
//  Copyright © 2019 osvinuser. All rights reserved.

import Foundation

public struct OneBeatProduct {
  
  public static let SwiftShopping = "com.developer.dev.OneBeat.OneBeatSubscription"
  
  private static let productIdentifiers: Set<ProductIdentifier> = [OneBeatProduct.SwiftShopping]

  public static let store = IAPHelper(productIds: OneBeatProduct.productIdentifiers)
}

func resourceNameForProductIdentifier(_ productIdentifier: String) -> String? {
  return productIdentifier.components(separatedBy: ".").last
}
