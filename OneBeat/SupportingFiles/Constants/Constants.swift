//
//  Constants.swift
//  GetBlushh
//
//  Created by Arvind Mehta on 13/12/17.
//  Copyright © 2017 Arvind Mehta. All rights reserved.
//

import Foundation

struct Constants{
    
    struct APIs {
        //http://184.73.56.196/assets/profile_image/workout_cat.jpg
        //static let baseURL            = ""
        //static let baseURLImages      = ""
        static let LIVEURL:String = "http://184.73.56.196" //"http://184.73.56.196/"
        static let GETIMAGE = "/Api/User/getImage"
        
        static let REGISTER:String = "/Api/User/SignUp"
        
        static let Api_Home_liveSessions = "/Api/Home/liveSessions"
        static let Api_Home_workoutCategory = "/Api/Home/workoutCategory"
        
        static let Api_Workout_getWorkout = "/Api/Workout/getWorkout"
        
        static let Api_Workout_workoutJoin = "/Api/Workout/workoutJoin"
        static let Api_Workout_workoutDetails = "/Api/Workout/workoutDetails"
        
        static let Api_LeaderBoard_leaderBoardUsers = "/Api/LeaderBoard/leaderBoardUsers"
        
        static let User_Like_Api = "/Api/LeaderBoard/Userlike"
        static let like_listing_Api = "/Api/LeaderBoard/Likelisting"
        static let user_Listing_Api = "/Api/LeaderBoard/leaderBoardUsers"
        static let workout_Listing = "/Api/Workout/listingWorkout"
        static let email_Api = "/Api/workout/workoutEmail"
        static let userLike_Api = "Api/LeaderBoard/UserImage"
        static let LeaderBoardUseraimageapi = "Api/LeaderBoard/UserImage"
        static let followersAPi = "Api/LeaderBoard/UserImage"
        static let adddFriendApi = "Api/LeaderBoard/Useradd"
        static let follow_Api = "Api/LeaderBoard/Userfollow"
         static let getUserworkoutDetail = "Api/Workout/workoutDetails"
    }
    
    struct AlertsMessages {
        
        static let noInternetConnecLocalize = "No internet connection"
        static let somethingWentWrongPleaseTryAgainLater = "Something went wrong. Please try again later."
        static let dataIsNotInProperFormat = "Data is not in proper format"
        
        static let loginToContinueLocalize = "Please login to continue"
        static let allFieldsAreEmpty = "All fields are required"
        static let pleaseEnterNameLocalize = "Please enter name"
        static let pleaseEnterEmailLocalize = "Please enter an email"
        static let pleaseEnterPhoneLocalize = "Please enter phone number"
        static let enterPasswordLocalize = "Please enter password"
        static let enterEmailLocalize = "Please enter a valid email"
        static let EnterValidUsername = "Please enter a valid name"
        static let sixCharactersPasswordLocalize = "Password should be at least 6 characters long"
        static let wrong_phone_entered = "Please enter a valid phone number"
        static let klogout = "Are you sure you want to logout?"
    }
    
    struct appTitle {
        static let alertTitle = "OneBeat"
    }
    
    static let LOGIN_CONTROLLER:String = "Login"
    static let ID:String = ""
    static let FIRST_NAME:String = ""
    static let EMAIL:String = ""
    static let LAST_NAME:String = ""
    static let GENDER:String = ""
    static let DOB:String = ""
    static let WEIGHT:String = ""
    static let HEIGHT:String = ""
    static let PROFILE_IMAGE:String = ""
    
}

struct AppConstants {
    
    static let appDelegete = UIApplication.shared.delegate as! AppDelegate
    
}

