//
//  EditProfileVC.swift
//  OneBeat
//
//  Created by osvinuser on 14/12/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//

import UIKit
import FloatingTextField
import Alamofire
import SwiftyJSON

class EditProfileVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var locationTF: UITextField!
    @IBOutlet weak var lastnameTF: UITextField!
    @IBOutlet weak var profileimage: UIImageView!
    @IBOutlet weak var typeofheight:UITextField!
    @IBOutlet weak var heightTF: UITextField!
    @IBOutlet weak var weightTF: UITextField!
    @IBOutlet weak var typeofweightTF: UITextField!
    @IBOutlet weak var dobTF: UITextField!
    @IBOutlet weak var genderTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var timzoneTF: FloatingTextField!
    @IBOutlet weak var user_Name_TxtField: UITextField!
    
    var userImage: UIImage?
    var imagePicker = UIImagePickerController()
    let pickergenderview = GMPicker()
    var pickergender = [String]()
    let pickerweightview = GMPicker()
    var pickerweight = [String]()
    let pickerinchesview = GMPicker()
    var pickerinches = [String]()
    let pickerpoundsview = GMPicker()
    var pickerpounds = [String]()
    let pickerheightview = GMPicker()
    var pickerheight = [String]()
    var datePicker = GMDatePicker()
    var dateFormatter = DateFormatter()
    var simplearray = [SimpleModle]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Edit Profile"
        let saveBarButtonItem = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(saveUser))
        self.navigationItem.rightBarButtonItem  = saveBarButtonItem
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        imagePicker.delegate = self
        
        let backBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(backbuttonaction))
        self.navigationItem.leftBarButtonItem  = backBarButtonItem
        let sliderImage = UIImage(named: "ic_back_arrow_black")
       
        navigationItem.leftBarButtonItem?.image = sliderImage
        navigationItem.leftBarButtonItem?.tintColor = UIColor.black
        let profile_imageStr:String = getSharedPrefrance(key:"profile_image")
        
        let profilenewString = profile_imageStr.replacingOccurrences(of: "localhost", with: "192.168.1.90", options: .literal, range: nil)
        
        self.profileimage.sd_setImage(with: URL(string:profilenewString), placeholderImage: UIImage(named: "placeholder.png"))
        
        self.nameTF.text = getSharedPrefrance(key:"first_name")
        self.lastnameTF.text = getSharedPrefrance(key:"last_name")
        self.emailTF.text = getSharedPrefrance(key:"email")
        self.genderTF.text = getSharedPrefrance(key:"gender")
        self.dobTF.text = getSharedPrefrance(key:"dob")
        self.weightTF.text = getSharedPrefrance(key:"weight")
        self.heightTF.text = getSharedPrefrance(key:"height")
        self.typeofweightTF.text = getSharedPrefrance(key:"weight_unit")
        self.typeofheight.text = getSharedPrefrance(key:"height_unit")
        self.timzoneTF.text = getCurrentTimeZone()
        self.tabBarController?.tabBar.isHidden = true
        self.locationTF.text = getSharedPrefrance(key:"location")
        self.user_Name_TxtField.text = getSharedPrefrance(key: "userName")
        self.nameTF.delegate = self
        self.emailTF.delegate = self
        self.genderTF.delegate = self
        self.dobTF.delegate = self
        self.weightTF.delegate = self
        self.heightTF.delegate = self
        self.typeofheight.delegate = self
        self.typeofweightTF.delegate = self
        self.lastnameTF.delegate = self
        self.user_Name_TxtField.delegate = self
        
        dobTF.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(chooseDOB)))
        dateFormatter.dateFormat = "dd/MM/YYYY"
        genderTF.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(chooseGender)))
        // weightTF.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(chooseweight)))
        typeofweightTF.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(choosepounds)))
        // heightTF.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(chooseheight)))
        typeofheight.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(chooseinches)))
        
        setupDatePicker()
        setupgenderPickerView()
        //setupweightPickerView()
        setuppoundsPickerView()
        // setupheightPickerView()
        setupinchesPickerView()
    }
    
    @IBAction func logoutbuttonaction(_ sender: Any){
        logoutapi()
    }
    
    @IBAction func accountsettingsbuttonaction(_ sender: Any){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AccountSettingsVC") as! AccountSettingsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func uploadimagebuttonaction(_ sender: Any){
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func choosepounds(){
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        pickerpoundsview.choosefortypeofweight()
        pickerpoundsview.show(inVC:self)
    }
    
    @objc func chooseinches(){
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        pickerinchesview.chooseheightformate()
        pickerinchesview.show(inVC:self)
    }
    
    @objc func chooseheight(){
        pickerheightview.setupweight()
        pickerheightview.show(inVC:self)
    }
    
    @objc func chooseweight(){
        if self.weightTF.text == "Pounds"{
            pickerweightview.setuppounds()
        }else{
            pickerweightview.setupkgs()
        }
        pickerweightview.show(inVC:self)
    }
    
    @objc func chooseGender(){
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        pickergenderview.setupGender()
        pickergenderview.show(inVC: self)
    }
    
    @objc func chooseDOB(){
        datePicker.show(inVC: self)
        self.navigationItem.rightBarButtonItem?.isEnabled = true
    }
    
    @objc func backbuttonaction(){
        self.navigationController?.popViewController(animated:true)
    }
    
    @objc func saveUser(){
        if (self.nameTF.text?.isEmpty)!{
            showToast(message:"Please Enter First Name")
        }else if (self.emailTF.text?.isEmpty)! || (!isValidEmail(testStr: emailTF.text!)){
            showToast(message:"Please Enter Vaild Email")
        }else if (self.genderTF.text?.isEmpty)!{
            showToast(message:"Please Select The Gender")
        }else if (self.dobTF.text?.isEmpty)!{
            showToast(message:"Please Select DOB")
        }else if (self.weightTF.text?.isEmpty)!{
            showToast(message:"Please Select Enter Weight")
        }else if (self.typeofweightTF.text?.isEmpty)!{
            showToast(message:"Please Select Type Of Weight")
        }else if (self.heightTF.text?.isEmpty)!{
            showToast(message:"Please Enter Height")
        }else if (self.typeofheight.text?.isEmpty)!{
            showToast(message:"Please Select Type Of Height")
        }else{
            let location:String = getSharedPrefrance(key:"location")
            let latitude:String = getSharedPrefrance(key:"latitude")
            let longitude:String = getSharedPrefrance(key:"longitude")
            simplearray.append(SimpleModle.init(firstname:self.nameTF.text, lastname:self.lastnameTF.text, emailaddress:self.emailTF.text, gender:self.genderTF.text, dob:self.dobTF.text, weight:self.weightTF.text, weight_type:typeofweightTF.text, height:self.heightTF.text, height_type:self.typeofheight.text, timezone:getCurrentTimeZone(), location: location, latitude:latitude, longitude:longitude, username: user_Name_TxtField.text))
            hittheapiforresaving()
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == genderTF {
        } else if textField == dobTF {
        } else if textField == typeofweightTF {
        } else if textField == typeofheight {
        } else {
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool{
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        return true
    }
    
    func openCamera() {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            let alert  = UIAlertController(title: "Warning", message: "Sorry, this device has no camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary() {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func hideLoader(view: UIView) {
        DispatchQueue.main.async(execute: {
            //Indicator.sharedInstance.hideIndicator()
            MBProgressHUD.hide(for: view, animated: true)
        })
    }
    
    func showLoader(view: UIView) {
        DispatchQueue.main.async(execute: {
            //Indicator.sharedInstance.showIndicator()
            MBProgressHUD.showAdded(to: view, animated: true)
        })
    }
    
    //MARK:-- ImagePicker delegate
    @objc  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        // The info dictionary may contain multiple representations of the image. You want to use the original.
        guard (info[.originalImage] as? UIImage) != nil else{
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        userImage = info[.originalImage] as? UIImage
        profileimage.image = userImage
        
        saveImage(image:userImage!)
        
        // Dismiss the picker.
        dismiss(animated: true, completion: nil)
    }
    
}

extension EditProfileVC: GMDatePickerDelegate,GMPickerDelegate {
    
    func gmPicker(_ gmPicker: GMPicker, didSelect string: String){
        if gmPicker == pickergenderview{
            genderTF.text = string
        }else if gmPicker == pickerweightview{
            weightTF.text = string
        }else if gmPicker == pickerpoundsview{
            self.typeofweightTF.text = string
        }else if gmPicker == pickerheightview{
            heightTF.text = string
        }else{
            self.typeofheight.text = string
        }
    }
    
    func gmPickerDidCancelSelection(_ gmPicker: GMPicker){
        
    }
    
    func gmDatePicker(_ gmDatePicker: GMDatePicker, didSelect date: Date){
        print(date)
        dobTF.text = dateFormatter.string(from: date)
    }
    
    func gmDatePickerDidCancelSelection(_ gmDatePicker: GMDatePicker) {
        
    }
    
    fileprivate func setupgenderPickerView(){
        pickergenderview.delegate = self
        pickergenderview.config.animationDuration = 0.5
        pickergenderview.config.cancelButtonTitle = "Cancel"
        pickergenderview.config.confirmButtonTitle = "Confirm"
        pickergenderview.config.contentBackgroundColor = UIColor(red: 253/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1)
        pickergenderview.config.headerBackgroundColor = UIColor(red: 244/255.0, green: 244/255.0, blue: 244/255.0, alpha: 1)
        pickergenderview.config.confirmButtonColor = UIColor.black
        pickergenderview.config.cancelButtonColor = UIColor.black
    }
    
    fileprivate func setupweightPickerView(){
        pickerweightview.delegate = self
        pickerweightview.config.animationDuration = 0.5
        pickerweightview.config.cancelButtonTitle = "Cancel"
        pickerweightview.config.confirmButtonTitle = "Confirm"
        pickerweightview.config.contentBackgroundColor = UIColor(red: 253/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1)
        pickerweightview.config.headerBackgroundColor = UIColor(red: 244/255.0, green: 244/255.0, blue: 244/255.0, alpha: 1)
        pickerweightview.config.confirmButtonColor = UIColor.black
        pickerweightview.config.cancelButtonColor = UIColor.black
    }
    
    fileprivate func setuppoundsPickerView(){
        pickerpoundsview.delegate = self
        pickerpoundsview.config.animationDuration = 0.5
        pickerpoundsview.config.cancelButtonTitle = "Cancel"
        pickerpoundsview.config.confirmButtonTitle = "Confirm"
        pickerpoundsview.config.contentBackgroundColor = UIColor(red: 253/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1)
        pickerpoundsview.config.headerBackgroundColor = UIColor(red: 244/255.0, green: 244/255.0, blue: 244/255.0, alpha: 1)
        pickerpoundsview.config.confirmButtonColor = UIColor.black
        pickerpoundsview.config.cancelButtonColor = UIColor.black
    }
    fileprivate func setupheightPickerView(){
        pickerheightview.delegate = self
        pickerheightview.config.animationDuration = 0.5
        pickerheightview.config.cancelButtonTitle = "Cancel"
        pickerheightview.config.confirmButtonTitle = "Confirm"
        pickerheightview.config.contentBackgroundColor = UIColor(red: 253/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1)
        pickerheightview.config.headerBackgroundColor = UIColor(red: 244/255.0, green: 244/255.0, blue: 244/255.0, alpha: 1)
        pickerheightview.config.confirmButtonColor = UIColor.black
        pickerheightview.config.cancelButtonColor = UIColor.black
    }
    
    fileprivate func setupinchesPickerView(){
        pickerinchesview.delegate = self
        pickerinchesview.config.animationDuration = 0.5
        pickerinchesview.config.cancelButtonTitle = "Cancel"
        pickerinchesview.config.confirmButtonTitle = "Confirm"
        pickerinchesview.config.contentBackgroundColor = UIColor(red: 253/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1)
        pickerinchesview.config.headerBackgroundColor = UIColor(red: 244/255.0, green: 244/255.0, blue: 244/255.0, alpha: 1)
        pickerinchesview.config.confirmButtonColor = UIColor.black
        pickerinchesview.config.cancelButtonColor = UIColor.black
    }
    
    fileprivate func setupDatePicker() {
        datePicker.delegate = self
        datePicker.config.startDate = Date()
        datePicker.datePicker.set18YearValidation()
        datePicker.config.animationDuration = 0.5
        datePicker.config.cancelButtonTitle = "Cancel"
        datePicker.config.confirmButtonTitle = "Confirm"
        datePicker.config.contentBackgroundColor = UIColor(red: 253/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1)
        datePicker.config.headerBackgroundColor = UIColor(red: 244/255.0, green: 244/255.0, blue: 244/255.0, alpha: 1)
        datePicker.config.confirmButtonColor = UIColor.black
        datePicker.config.cancelButtonColor = UIColor.black
    }
    
}

extension EditProfileVC{
    
    func logoutapi(){
        
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        let parameter = [
            "user_id":getSharedPrefrance(key:"id"),
            "unique_device_id":deviceID
        ]
        
        executePOST(view: self.view, path: Constants.APIs.LIVEURL+"/Api/User/logout", parameter: parameter){ response in
            let status = response["ResponseCode"].stringValue
            self.view.makeToast(response["MessageWhatHappen"].stringValue)
            if(status == "true"){
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "StartingVC") as! StartingVC
                self.navigationController?.pushViewController(vc, animated: true)
                savesharedprefrence(key:"loginsession", value:"false")
                self.view.makeToast(response["MessageWhatHappen"].stringValue)
            }else{
                self.view.makeToast(response["MessageWhatHappen"].stringValue)
            }
        }
    }
    
    func hittheapiforresaving() {
        
        let firstname:String = simplearray[0].firstname!
        let last_name:String = simplearray[0].lastname!
        let dob:String = simplearray[0].dob!
        let weight:String = simplearray[0].weight!
        let weight_unit:String = simplearray[0].weight_type!
        let height:String = simplearray[0].height!
        let height_unit:String = simplearray[0].height_type!
        let timezone:String = simplearray[0].timezone!
        let latitude:String = simplearray[0].latitude!
        let longitude:String = simplearray[0].longitude!
        let location:String = simplearray[0].location!
        let username: String = simplearray[0].username!
        
        let parameter       :[String:Any] = [
            "user_id"       : getSharedPrefrance(key:"id"),
            "first_name"    :firstname,
            "last_name"     :last_name,
            "dob"           :dob,
            "weight"        :weight,
            "weight_unit"   :weight_unit,
            "height"        :height,
            "height_unit"   :height_unit,
            "timezone"      :timezone,
            "latitude"      :latitude,
            "longitude"     :longitude,
            "location"      :location,
            "username"      :user_Name_TxtField.text!
        ]
        
        executePOST(view: self.view, path: Constants.APIs.LIVEURL+"/Api/Home/editProfile", parameter: parameter){ response in
            
            let status = response["ResponseCode"].stringValue
            self.view.makeToast(response["MessageWhatHappen"].stringValue)
            if(status == "true") {
                savesharedprefrence(key:"first_name", value:response["EditProfileResponse"]["first_name"].stringValue)
                savesharedprefrence(key:"last_name", value:response["EditProfileResponse"]["last_name"].stringValue)
                savesharedprefrence(key:"email", value:response["EditProfileResponse"]["email"].stringValue)
                savesharedprefrence(key:"latitude", value:response["EditProfileResponse"]["latitude"].stringValue)
                savesharedprefrence(key:"longitude", value:response["EditProfileResponse"]["longitude"].stringValue)
                savesharedprefrence(key:"location", value:response["EditProfileResponse"]["location"].stringValue)
                savesharedprefrence(key:"timezone", value:response["EditProfileResponse"]["timezone"].stringValue)
                savesharedprefrence(key:"gender", value:response["EditProfileResponse"]["gender"].stringValue)
                savesharedprefrence(key:"dob", value:response["EditProfileResponse"]["dob"].stringValue)
                savesharedprefrence(key:"weight", value:response["EditProfileResponse"]["weight"].stringValue)
                savesharedprefrence(key:"weight_type", value:response["EditProfileResponse"]["weight_unit"].stringValue)
                savesharedprefrence(key:"height", value:response["EditProfileResponse"]["height"].stringValue)
                savesharedprefrence(key:"height_type", value:response["EditProfileResponse"]["height_unit"].stringValue)
                savesharedprefrence(key:"profile_image", value:response["EditProfileResponse"]["profile_image"].stringValue)
                savesharedprefrence(key: "userName", value: response["EditProfileResponse"]["username"].stringValue)
                let profile_imageStr:String = getSharedPrefrance(key:"profile_image")
                self.profileimage.sd_setImage(with: URL(string:profile_imageStr), placeholderImage: UIImage(named: "placeholder.png"))
                self.nameTF.text = getSharedPrefrance(key:"first_name")
                self.lastnameTF.text = getSharedPrefrance(key:"last_name")
                self.emailTF.text = getSharedPrefrance(key:"email")
                self.genderTF.text = getSharedPrefrance(key:"gender")
                self.dobTF.text = getSharedPrefrance(key:"dob")
                self.weightTF.text = getSharedPrefrance(key:"weight")
                self.heightTF.text = getSharedPrefrance(key:"height")
                self.typeofweightTF.text = getSharedPrefrance(key:"weight_unit")
                self.typeofheight.text = getSharedPrefrance(key:"height_unit")
                self.timzoneTF.text = self.getCurrentTimeZone()
                self.locationTF.text = getSharedPrefrance(key:"location")
                self.user_Name_TxtField.text = getSharedPrefrance(key: "userName")
            }else{
                self.view.makeToast(response["MessageWhatHappen"].stringValue)
            }
        }
    }
    
    func saveImage(image:UIImage){
        
        let parameter:[String: Any] = [
                "user_id":getSharedPrefrance(key:"id")
        ]
        
        let url = Constants.APIs.LIVEURL + Constants.APIs.GETIMAGE
        
        self.showLoader(view: self.view)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameter{
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if let imageData = UIImage.jpegData(image)(compressionQuality:0.5){
                let r = arc4random()
                let str = "profile_pic"+String(r)+".jpg"
                let parameterName = "profile_image"
                multipartFormData.append(imageData, withName:parameterName, fileName:str, mimeType: "image/jpg")
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: nil) { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                    self.hideLoader(view: self.view)
                    
                    if let httpStatus = response.response , httpStatus.statusCode == 200 {
                        if response.result.isSuccess {
                            print(response)
                            if let dic = response.result.value as? [String: Any]{
                                if let dicnew = dic["ImageResponse"] as? [String: Any]{
                                    self.showAlert(dic["MessageWhatHappen"] as! String)
                                    savesharedprefrence(key:"profile_image", value:dicnew["profile_image"]! as! String)
                                    let profile_imageStr:String = getSharedPrefrance(key:"profile_image")
                                    let profilenewString = profile_imageStr.replacingOccurrences(of: "localhost", with: "192.168.1.90", options: .literal, range: nil)
                                    self.profileimage.sd_setImage(with: URL(string:profilenewString), placeholderImage: UIImage(named: "placeholder.png"))
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "imageupdating"), object:nil)
                                }else{}
                            }
                        }else{
                            
                            self.showAlert("Something went wrong.")
                            let profile_imageStr:String = getSharedPrefrance(key:"profile_image")
                            let profilenewString = profile_imageStr.replacingOccurrences(of: "localhost", with: "192.168.1.90", options: .literal, range: nil)
                            self.profileimage.sd_setImage(with: URL(string:profilenewString), placeholderImage: UIImage(named: "placeholder.png"))
                            self.hideLoader(view: self.view)
                        }
                    }
                }
                
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
            }
        }
    }
    
}
