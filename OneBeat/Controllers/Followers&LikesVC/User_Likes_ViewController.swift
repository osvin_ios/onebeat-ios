//
//  User_Likes_ViewController.swift
//  OneBeat
//
//  Created by osvinuser on 01/08/19.
//  Copyright © 2019 osvinuser. All rights reserved.
//

import UIKit

class User_Likes_ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var user_Likes_TableView: UITableView!
    var leaderBoardUsersListingData = [Users_listing_Model]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

         self.user_Likes_TableView.register(UINib(nibName: "UserLikes_Follow_TableViewCell", bundle: nil), forCellReuseIdentifier: "UserLikes_Follow_TableViewCell")
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UserLikes_Follow_TableViewCell = user_Likes_TableView.dequeueReusableCell(withIdentifier: "UserLikes_Follow_TableViewCell", for: indexPath) as!
        UserLikes_Follow_TableViewCell
        
        guard leaderBoardUsersListingData.count != 0 else { return cell }
        
        if let getImageInfo = self.leaderBoardUsersListingData[indexPath.row].profile_image {
            cell.user_ImageView?.sd_addActivityIndicator()
            cell.user_ImageView?.sd_setIndicatorStyle(.gray)
            cell.user_ImageView?.sd_setImage(with: URL(string: getImageInfo), placeholderImage: UIImage(named: "ic_sketchy_background") , options: .continueInBackground) { (downloadImage, error, cacheType, urlValue) in
            }
        }
        cell.user_Name_Label.text = self.leaderBoardUsersListingData[indexPath.row].user_Name ?? ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func users_Like_Api() {
        
        let parameter: [String:Any] = ["search"   : "search_TxtField.text!",
                                       "user_id"  : getSharedPrefrance(key: "id")
        ]
    // http://184.73.56.196/Api/LeaderBoard/UserImage
        executePOST(view: self.view, path: Constants.APIs.LIVEURL+Constants.APIs.userLike_Api, parameter: parameter){ response in
            
            let status = response["ResponseCode"].stringValue
            self.leaderBoardUsersListingData.removeAll()
            if(status == "true") {
                print(response)
//                self.leaderBoardUsersListingData.removeAll()
                for data in response["leaderBoardWorkoutResponse"].arrayValue {
                    self.leaderBoardUsersListingData.append(Users_listing_Model(json:data.dictionaryObject!)!)
                }
                
            } else {
                self.view.makeToast(response["MessageWhatHappen"].stringValue)
            }
            self.user_Likes_TableView.reloadData()
        }
    }
}
