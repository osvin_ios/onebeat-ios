//
//  AutoRenewable_SubscriptionViewController.swift
//  OneBeat
//
//  Created by osvinuser on 14/06/19.
//  Copyright © 2019 osvinuser. All rights reserved.
//

import UIKit
import StoreKit

class AutoRenewable_TableView_Controller: UITableViewController {
    
    var products: [SKProduct] = []
    let activity = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        show_Loader_Activity()
    
        self.navigationItem.title = "Subscription"
        let backBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(backbuttonaction))
        self.navigationItem.leftBarButtonItem  = backBarButtonItem
        let backImage = UIImage(named: "ic_back_arrow_black")
        
        navigationItem.leftBarButtonItem?.image = backImage
        navigationItem.leftBarButtonItem?.tintColor = UIColor.black
        
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(AutoRenewable_TableView_Controller.reload), for: .valueChanged)
        
//        let restoreButton = UIBarButtonItem(title: "Restore",
//                                            style: .plain,
//                                            target: self,
//                                            action: #selector(AutoRenewable_TableView_Controller.restoreTapped(_:)))
//        navigationItem.rightBarButtonItem = restoreButton
        
        NotificationCenter.default.addObserver(self, selector: #selector(AutoRenewable_TableView_Controller.handlePurchaseNotification(_:)),
                                               name: .IAPHelperPurchaseNotification,
                                               object: nil)
    }
    
    func show_Loader_Activity () {
        activity.backgroundColor = UIColor.lightGray
        activity.style = .gray
        activity.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        activity.center = CGPoint(x: view.frame.size.width / 2, y: view.frame.size.height / 2)
        activity.startAnimating()
        view.addSubview(activity)
    }
    
    func hide_Loader()  {
        activity.stopAnimating()
        activity.sendSubviewToBack(view)
    }
    
    @objc func backbuttonaction() {
        self.navigationController?.popViewController(animated:true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
        reload()
    }
    
    @objc func reload() {
        products = []
        
        tableView.reloadData()
        
        OneBeatProduct.store.requestProducts{ [weak self] success, products in
            guard let self = self else { return }
            if success {
                self.products = products!
                self.tableView.reloadData()
            }
            self.refreshControl?.endRefreshing()
        }
    }
    
    @objc func restoreTapped(_ sender: AnyObject) {
        OneBeatProduct.store.restorePurchases()
    }
    
    @objc func handlePurchaseNotification(_ notification: Notification) {
        guard
            let productID = notification.object as? String,
            let index = products.index(where: { product -> Bool in
                product.productIdentifier == productID
            })
            else { return }
        
        tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .fade)
    }
}

// MARK: - UITableViewDataSource

extension AutoRenewable_TableView_Controller {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ProductCell
        
        let product = products[(indexPath as NSIndexPath).row]
        
        cell.product = product
        hide_Loader()
        cell.buyButtonHandler = { product in
            OneBeatProduct.store.buyProduct(product)
        }
        return cell
    }
}

