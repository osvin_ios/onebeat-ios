//
//  LeaderboardVC.swift
//  OneBeat
//
//  Created by osvinuser on 11/12/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//

import UIKit
import ChameleonFramework

class LeaderboardVC: UIViewController{

    @IBOutlet weak var sortByButton: UIButton!
    @IBOutlet weak var leaderboardTableView: UITableView!
    var page = Int()
    var leaderBoardUsersListingData = [LeaderBoardUsersModel]()
    var leaderBoardUsersListingDataCountIntArr = [Int]()
    var newCount = [Int]()
    var stopApi = String()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        stopApi = "no"
        page = 1
        self.leaderboardTableView.register(UINib(nibName: "LeaderboardTableViewCellXib", bundle: nil), forCellReuseIdentifier: "LeaderboardTableViewCellXib")
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(true)
        navigationController?.setNavigationBarHidden(true, animated: true)
        if Reachability.isConnectedToNetwork() == true {
            self.leaderBoardUsersApi()
        } else {
            self.showAlert(Constants.AlertsMessages.noInternetConnecLocalize)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @IBAction func search_Btn(_ sender: UIButton) {
        let Vc = storyboard?.instantiateViewController(withIdentifier: "User_List_ViewController") as! User_List_ViewController
        navigationController?.pushViewController(Vc, animated: true)
    }
    
    @IBAction func sortByButtonPressed(_ sender: UIButton) {
        guard leaderBoardUsersListingData.count != 0 else {return}
        self.leaderBoardUsersListingData = self.leaderBoardUsersListingData.reversed()
//        self.leaderBoardUsersListingDataCountIntArr = self.leaderBoardUsersListingDataCountIntArr.reversed()
        self.leaderboardTableView.reloadData()
    }
}

extension LeaderboardVC : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.leaderBoardUsersListingData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : LeaderboardTableViewCellXib = tableView.dequeueReusableCell(withIdentifier: "LeaderboardTableViewCellXib", for: indexPath) as! LeaderboardTableViewCellXib
        cell.selectionStyle = .none
        guard leaderBoardUsersListingData.count != 0 else {return cell}
       
        if let getImageInfo = self.leaderBoardUsersListingData[indexPath.row].profile_image {
            cell.userImageView?.sd_addActivityIndicator()
            cell.userImageView?.sd_setIndicatorStyle(.gray)
            cell.userImageView?.sd_setImage(with: URL(string: getImageInfo), placeholderImage: UIImage(named: "ic_sketchy_background") , options: .continueInBackground) { (downloadImage, error, cacheType, urlValue) in
            }
        }
        
        cell.numbering_Label.text = "\(leaderBoardUsersListingDataCountIntArr[indexPath.row])" + "."

        cell.userNameLabel.text = self.leaderBoardUsersListingData[indexPath.row].username ?? ""
        cell.userScoreLabel.text = self.leaderBoardUsersListingData[indexPath.row].points ?? ""
        
        let points:Float? = Float(self.leaderBoardUsersListingData[indexPath.row].points ?? "")
        if let point = points{
            
            let maxValue = Float(1000)
            let scoreValue = point
            var reqValue = scoreValue/maxValue
            reqValue = reqValue*Float(100)
            let reqScreenSizeValue = Float(tableView.frame.width)/Float(100)
            reqValue = reqValue*reqScreenSizeValue
            reqValue = Float(tableView.frame.width)-reqValue
            cell.backgroundColorLabelTrailingConstraint.constant = CGFloat(reqValue)
            cell.backgroundColorLabel.layoutIfNeeded()
            
            /* Step 1 set the colors which you want to show in the view */
//            if let topColor = UIColor(hexString: "#FF9D00"){
//                if let bottomColor = UIColor(hexString: "#FF7D00"){
//                }
//            }

//            cell.backgroundColorView.backgroundColor = UIColor(gradientStyle: UIGradientStyle.radial, withFrame: cell.backgroundColorView.frame, andColors: [ UIColor.init(red: 248/255, green: 146/255, blue: 27/255, alpha: CGFloat(reqValue))])
            cell.backgroundColorView.backgroundColor = UIColor(gradientStyle: UIGradientStyle.radial, withFrame: cell.backgroundColorView.frame, andColors: [UIColor.init(red: 248/255, green: 146/255, blue: 27/255, alpha: CGFloat(1.0))])
            cell.backgroundColorView.layer.borderColor = UIColor(red: 255/255, green: 226/255, blue: 95/255, alpha: CGFloat(1.0)).cgColor
            cell.backgroundColorView.layer.borderWidth = 0.5
            
        }
            let likeStatus = (self.leaderBoardUsersListingData[indexPath.row].like_Status ?? "")
            cell.like_Btn_Method = {
                let to_id = (self.leaderBoardUsersListingData[indexPath.row].user_id ?? "")
                self.user_like_Api(user_id : to_id)
//                self.call_like_Api(to_id: to_id)
            }
                if likeStatus == "1" {
                    cell.like_Emoji_Btn.setImage(UIImage(named: "arm-muscles-silhouette-2"), for: UIControl.State.normal)
                }
                else {
                    cell.like_Emoji_Btn.setImage(UIImage(named: "arm-muscles-silhouette-3"), for: UIControl.State.normal)
                }
            
           // let topColor = UIColor().HexToColor(hexString: "#FF9D00", alpha: 1.0)
          //  let bottomColor = UIColor().HexToColor(hexString: "#FF7D00", alpha: 1.0)
            /* Step 2 create the gradient layer, add the colors and set the frame */
            //let gradientLayer: CAGradientLayer = CAGradientLayer()
//            //gradientLayer.frame.size.width = self.view.bounds.size.width
//            gradientLayer.frame.size.width = cell.backgroundColorLabel.bounds.size.width
//            gradientLayer.frame.size.height = cell.backgroundColorLabel.bounds.size.height
//            //gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
//            gradientLayer.colors = [bottomColor.cgColor, topColor.cgColor]
//            //gradientLayer.locations = [0.0, 0.8]
//            //gradientLayer.startPoint = CGPoint(x: 0.0, y: 1.0)
//            //gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.0)
//                //gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.0)
//                //gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)
//            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
//            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
//
//            cell.backgroundColorLabel.layer.insertSublayer(gradientLayer, at: 0)
//            //cell.backgroundColorLabel.layer.addSublayer(gradientLayer)

//            //self.view.backgroundColor = UIColor.greenColor()
//            gradientLayer.colors = [bottomColor.cgColor, topColor.cgColor]
//            //gradientLayer.locations = [0.0, 0.5, 1.0, 0.5]
//            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
//            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
//            //gradientLayer.frame = cell.backgroundColorView.bounds
//            gradientLayer.frame = cell.backgroundColorView.frame
////            if let topLayer =  cell.backgroundColorView.layer.sublayers?.first, topLayer is CAGradientLayer{
////                topLayer.removeFromSuperlayer()
////            }
//            cell.backgroundColorView.layer.addSublayer(gradientLayer)
            
//            cell.backgroundColorLabel.layoutIfNeeded()
            cell.backgroundColorView.layoutIfNeeded()
            
//        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        if stopApi == "no" {
//        page += 1
//        leaderBoardUsersApi()
//    }
//        else {
//        print("no")
//        }
    }
}

extension LeaderboardVC {
    
    func leaderBoardUsersApi(){
        
        let parameter = [
            "user_id" : getSharedPrefrance(key:"id"),
            "page":"\(page)"
        ]
        
        executePOST(view: self.view, path: Constants.APIs.LIVEURL+Constants.APIs.Api_LeaderBoard_leaderBoardUsers, parameter: parameter){ response in

            let status = response["ResponseCode"].stringValue
            if(status == "true"){
                print(response)
                self.leaderBoardUsersListingData.removeAll()
                for data in response["leaderBoardWorkoutResponse"].arrayValue {
                    self.leaderBoardUsersListingData.append(LeaderBoardUsersModel(json:data.dictionaryObject!)!)
                }
                self.leaderBoardUsersListingDataCountIntArr.removeAll()
                for i in 1...self.leaderBoardUsersListingData.count {
                    self.leaderBoardUsersListingDataCountIntArr.append(i)
                }
                self.leaderboardTableView.reloadData()
               } else {
                self.stopApi = "yes"
                self.view.makeToast(response["MessageWhatHappen"].stringValue)
              }
           }
        }
    
    
    func user_like_Api(user_id : String){
        let parameter = [
            "from_id" : getSharedPrefrance(key:"id"),
            "to_id"   : user_id
        ]
        
        executePOST(view: self.view, path: Constants.APIs.LIVEURL+Constants.APIs.User_Like_Api, parameter: parameter){ response in
            print(response)
            let status = response["ResponseCode"].stringValue
            if(status == "true"){
                self.showToast(message: response["MessageWhatHappen"].stringValue)
                self.leaderBoardUsersApi()
            } else {
                self.view.makeToast(response["MessageWhatHappen"].stringValue)
            }
        }
    }
}

extension UIColor{
    func HexToColor(hexString: String, alpha:CGFloat? = 1.0) -> UIColor {
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        let alpha = alpha!
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = NSCharacterSet(charactersIn: "#") as CharacterSet
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
}
