//
//  MyTextField.swift
//  Mether
//
//  Created by Chaithu on 10/04/18.
//  Copyright © 2018 MetherTech. All rights reserved.
//

import UIKit

@IBDesignable class MyView: UIView {

    
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        updateCornerRadius()
        
     }

    @IBInspectable var rounded: Bool = false
        {
        didSet
        {
            updateCornerRadius()
        }
    }
    
    func updateCornerRadius()
    {
        layer.cornerRadius = rounded ? frame.size.height / 2 : 0
    }
}
