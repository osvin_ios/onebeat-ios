//
//  WorkoutDetailsScreenTableviewCell.swift
//  OneBeat
//
//  Created by osvinuser on 21/12/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//

import UIKit

class WorkoutDetailsScreenTableviewCell: UITableViewCell {

    @IBOutlet weak var timelabel: UILabel!
    @IBOutlet weak var tsubtitle: UILabel!
    @IBOutlet weak var tname: UILabel!
    @IBOutlet weak var timage: UIImageView!
    @IBOutlet weak var titleoftask: UILabel!
    @IBOutlet weak var taskimage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
