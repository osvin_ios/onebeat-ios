//
//  WorkoutDetailVC.swift
//  OneBeat
//
//  Created by osvinuser on 18/03/19.
//  Copyright © 2019 osvinuser. All rights reserved.
//

import UIKit
import CountdownLabel

class WorkoutDetailVC: UIViewController, CountdownLabelDelegate, LTMorphingLabelDelegate {
    
    @IBOutlet weak var workoutDetailVcScrollView: UIScrollView!
    @IBOutlet weak var workoutImageView: UIImageView!
    @IBOutlet weak var workoutNameLabel: UILabel!
    @IBOutlet weak var workoutTimeLabel: UILabel!
    @IBOutlet weak var workoutDescriptionLabel: UILabel!
    @IBOutlet weak var instructorImageView: ImageViewDesign!
    @IBOutlet weak var instructorNameLabel: UILabel!
    @IBOutlet weak var locationNameLabel: UILabel!
    @IBOutlet weak var countOfJoinedPeopleLabel: UILabel!
    @IBOutlet weak var joinNowButton: ButtonDesign!
    @IBOutlet weak var startsInView: UIView!
    @IBOutlet weak var startsAtLabel: UILabel!
    @IBOutlet weak var startsTimeButton: UIButton!
    @IBOutlet weak var timer_Label: CountdownLabel!
    
    //Constraints:
    @IBOutlet weak var scrollViewBottomConstraint: NSLayoutConstraint!
    
    var preVC = ""
    var getWorkoutListingData:Getworkout?
    var prevLiveWorkoutListingData:Workoutdetailslive?
    var check_Joining_Time_status = "online"
    //TimeVar
    var serverDateTimeStr : String?
    var serverDateInDateFormat : Date?
    var startDateInDateFormat : Date?
    var timer_Value = TimeInterval()
    var startDate = String()
    var start = Int()
    var today = Int()
    var time_diff_Before_Start = TimeInterval()
    var time_Diff_After_Start = TimeInterval()
    var workoutId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if startDate == "no time" {
            
        }else {
        //get_Diff()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = true
        navigationController?.setNavigationBarHidden(true, animated: false)
        if Reachability.isConnectedToNetwork() == true {
            self.callWorkoutDetailApi()
//            call()
        } else {
            self.showAlert(Constants.AlertsMessages.noInternetConnecLocalize)
        }
        //self.updateData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.tabBarController?.tabBar.isHidden = false
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func updateData(){
        
        guard self.getWorkoutListingData != nil else {return}
        
        if let getImageInfo = self.getWorkoutListingData?.workout_image {
            self.workoutImageView?.sd_addActivityIndicator()
            self.workoutImageView?.sd_setIndicatorStyle(.gray)
            self.workoutImageView?.sd_setImage(with: URL(string: getImageInfo), placeholderImage: UIImage(named: "ic_sketchy_background") , options: .continueInBackground) { (downloadImage, error, cacheType, urlValue) in
            }
        }
        self.workoutNameLabel.text = self.getWorkoutListingData?.title ?? ""
        self.workoutTimeLabel.text = "\(self.getWorkoutListingData?.duration ?? "") minutes"
        self.workoutDescriptionLabel.text = self.getWorkoutListingData?.description ?? ""
        UserDefaults.standard.set(self.workoutTimeLabel.text, forKey: "duration")
        if let getImageInfo = self.getWorkoutListingData?.get_trainer?["trainer_profile_image"] as? String{
            self.instructorImageView?.sd_addActivityIndicator()
            self.instructorImageView?.sd_setIndicatorStyle(.gray)
            self.instructorImageView?.sd_setImage(with: URL(string: getImageInfo), placeholderImage: UIImage(named: "ic_sketchy_background") , options: .continueInBackground) { (downloadImage, error, cacheType, urlValue) in
            }
        }
        self.instructorNameLabel.text = self.getWorkoutListingData?.get_trainer?["trainer_name"] as? String ?? ""
        self.locationNameLabel.text = self.getWorkoutListingData?.get_trainer?["trainer_location"] as? String ?? ""
        self.countOfJoinedPeopleLabel.text = "\(self.getWorkoutListingData?.attendee_count ?? "") people joined"
        
        if self.preVC == "liveWorkoutDetailsScreenVC"{
            
            self.joinNowButton.isHidden = true
            self.startsInView.isHidden = false
            
            self.scrollViewBottomConstraint.constant = 60
            self.workoutDetailVcScrollView.layoutIfNeeded()
            
            let startDateAndTime = "\(self.getWorkoutListingData?.workout_date ?? "")"+" "+"\(self.getWorkoutListingData?.start_time ?? "")"
            let startDateString = self.convertTimeFormaters(startDateAndTime)
            getTimeInMillsec(dateString1: startDateAndTime)
            
            startDateInDateFormat = self.getDateFormatterDate(date: startDateAndTime, incomingDateFormat: "yyyy-MM-dd HH:mm:ss")
            
            if let serverDateInDateFormat = serverDateInDateFormat, let startDateInDateFormat = startDateInDateFormat{
                
                let startDateComparisionResult:ComparisonResult = serverDateInDateFormat.compare(startDateInDateFormat)
                
                if startDateComparisionResult == ComparisonResult.orderedAscending
                {
                    // Server date is smaller than Start date.
                    self.startsAtLabel.isHidden = false
                    self.startsTimeButton.setTitleColor(UIColor(red: 255/255, green: 78/255, blue: 78/255, alpha: 1), for: .normal)
                    timer_Label.setCountDownTime(minutes: time_diff_Before_Start)
                    timer_Label.start()

                   startsTimeButton.isUserInteractionEnabled = false
                }
                else if startDateComparisionResult == ComparisonResult.orderedDescending
                {
                    // Server date is greater than Start date.
                    self.startsAtLabel.isHidden = true
                    self.timer_Label.textColor = UIColor.white//(UIColor(red: 78/255, green: 153/255, blue: 255/255, alpha: 1))
                    timer_Label.text = "Go to Workout"
                    timer_Label.backgroundColor = UIColor.wahooGreen()
                    timer_Label.frame.origin.y = CGFloat(12)
                    timer_Label.frame.size.height = 36
                    
                    //self.startsTimeButton.setTitle("Get Started", for: .normal)
                }
                else if startDateComparisionResult == ComparisonResult.orderedSame
                {
                    // Server date and Start date are same.
                    self.startsAtLabel.isHidden = true
                    self.timer_Label.textColor = UIColor.white//(UIColor(red: 78/255, green: 153/255, blue: 255/255, alpha: 1))
                    timer_Label.text = "Go to Workout"
                    timer_Label.backgroundColor = UIColor.wahooGreen()
                    //self.startsTimeButton.setTitle("Get Started", for: .normal)
                }else{}
            }
        }else{
            self.joinNowButton.isHidden = false
            self.startsInView.isHidden = true
            
            self.scrollViewBottomConstraint.constant = 0
            self.workoutDetailVcScrollView.layoutIfNeeded()
        }
    }
    
    func countdownFinished() {
      
    }
    
    func getTimeInMillsec( dateString1: String) {
        let dateString2 = serverDateTimeStr ?? ""
        let Dateformatter = DateFormatter()
        Dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let date1 = Dateformatter.date(from: dateString1)
        let date2 = Dateformatter.date(from: dateString2)
        
        _ = getStart_InMillisecond(toTime: date1!)
        _ = getToday_InMillisecond(toTime: date2!)
        get_Diff_Before_Start()
        get_Diff_After_Start()
        let distanceBetweenDates: TimeInterval? = date1?.timeIntervalSince(date2!)
        
        let secondsInAnHour: Double = 3600 * 60 * 60
        let secondsInDays: Double = 86400
        let secondsInWeek: Double = 604800
        
        let hoursBetweenDates = Int((distanceBetweenDates! / secondsInAnHour))
        let daysBetweenDates = Int((distanceBetweenDates! / secondsInDays))
        let weekBetweenDates = Int((distanceBetweenDates! / secondsInWeek))
        
        print(weekBetweenDates,"weeks")//0 weeks
        print(daysBetweenDates,"days")//5 days
        print(hoursBetweenDates,"hours")//120 hours
    }
    
    func serverTime(){
        if self.serverDateTimeStr != nil && self.serverDateTimeStr != ""{
            let serverDateString = self.UTCToLocalStringValue(date: self.serverDateTimeStr ?? "", incomingDateFormat: "yyyy-MM-dd HH:mm:ss", outgoingDateFormat: "yyyy-MM-dd HH:mm:ss")
            
            serverDateInDateFormat = self.getDateFormatterDate(date: serverDateString, incomingDateFormat: "yyyy-MM-dd HH:mm:ss")
            
        }else{}
    }
    
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func joinNowButtonPressed(_ sender: ButtonDesign) {
        if Reachability.isConnectedToNetwork() == true {
            self.callJoinWorkoutApi()
        } else {
            self.showAlert(Constants.AlertsMessages.noInternetConnecLocalize)
        }
    }
    
    @IBAction func startsTimeButtonPressed(_ sender: UIButton) {
        
        print(time_Diff_After_Start)
//        if time_Diff_After_Start > 300 {
//            self.showAlert("Time Expired")
//        } else {
        let workOut_Id = getWorkoutListingData?.workout_id
      
        UserDefaults.standard.set(workOut_Id, forKey: "workOutId")
       // if self.startsTimeButton.currentTitle == "Get Started"{
            guard self.getWorkoutListingData != nil else {return}
            //if let meetingId = self.getWorkoutListingData?.meeting_id{
            if let meetingId = self.getWorkoutListingData?.meeting_id as? Int ?? Int(self.getWorkoutListingData?.meeting_id as? String ?? ""){
                if let name = getSharedPrefrance(key:"first_name") as? String{
                    ZoomService.sharedInstance.joinMeeting(name: name, number: meetingId, password: "", type: "Video", vc: self)
                }
            }
//        }
    }
    
    func getToday_InMillisecond(toTime:Date) -> Int{
        let elapsed = NSDate().timeIntervalSince(toTime)
        print(elapsed)
        today = Int(elapsed)
        return Int(elapsed * 1000)        
    }
    
    func getStart_InMillisecond(toTime:Date) -> Int{
        let elapsed = NSDate().timeIntervalSince(toTime)
        print(elapsed)
        start = Int(elapsed)
        return Int(elapsed * 1000)
    }
    
    func get_Diff_Before_Start()  {
        let differ = today - start
        time_diff_Before_Start = TimeInterval(differ)
        print(time_diff_Before_Start)
    }
    
    func get_Diff_After_Start()  {
        let differ = today - start
        time_Diff_After_Start = TimeInterval(differ)
        print(time_Diff_After_Start)
    }
    
    
    func call() {
        if self.preVC == "liveWorkoutDetailsScreenVC"{
            workoutId = self.prevLiveWorkoutListingData?.workout_id ?? ""
        }else{
            workoutId = self.getWorkoutListingData?.workout_id ?? ""
        }
        
        let user_id = getSharedPrefrance(key:"id")
        let request = NSMutableURLRequest(url: NSURL(string: Constants.APIs.LIVEURL+Constants.APIs.Api_Workout_workoutDetails)! as URL)
        request.httpMethod = "POST"
        let postString = "user_id=\(user_id)&workout_id=\(workoutId)"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                print("error=\(String(describing: error))")
                return
            }
            
            print("response = \(String(describing: response))")
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(String(describing: responseString))")
        }
        task.resume()
    }
}

extension WorkoutDetailVC{
    
    func callWorkoutDetailApi(){
               
        if self.preVC == "liveWorkoutDetailsScreenVC"{
            workoutId = self.prevLiveWorkoutListingData?.workout_id ?? ""
        }else{
            workoutId = self.getWorkoutListingData?.workout_id ?? ""
        }
        
        let parameter:[String:Any] = [
            "user_id":getSharedPrefrance(key:"id"),
            "workout_id":workoutId
        ]
        
        executePOST(view: self.view, path: Constants.APIs.LIVEURL+Constants.APIs.Api_Workout_workoutDetails, parameter: parameter){ response in
            print(response)
            let status = response["ResponseCode"].stringValue
            
            if(status == "true"){
                
                self.getWorkoutListingData = Getworkout.init(json:response["WorkoutJoinResponse"].dictionaryObject!)
                
                let serverDateTime = response["Server_time"].stringValue
                if serverDateTime.count != 0{
                    self.serverDateTimeStr = serverDateTime
                    self.serverTime()
                }
                self.updateData()
            }else{
                self.showToast(message:response["MessageWhatHappen"].stringValue)
            }
        }
    }
    
    func callJoinWorkoutApi(){
        
        let parameter:[String:Any] = [
            "user_id":getSharedPrefrance(key:"id"),
            "workout_id":self.getWorkoutListingData?.workout_id ?? ""
        ]
        
        executePOST(view: self.view, path: Constants.APIs.LIVEURL+Constants.APIs.Api_Workout_workoutJoin, parameter: parameter){ response in
            print(response)
            let status = response["ResponseCode"].stringValue
            
            if(status == "true"){
                //print(response)
                self.callWorkoutDetailApi()
                self.showToast(message:response["MessageWhatHappen"].stringValue)
            }else{
                self.showToast(message:response["MessageWhatHappen"].stringValue)
            }
        }        
    }
}


