//
//  ScheduleVC.swift
//  OneBeat
//
//  Created by osvinuser on 20/12/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ObjectMapper

class ScheduleVC: UIViewController, FSCalendarDataSource, FSCalendarDelegate {
    
    @IBOutlet weak var scheduletv: UITableView!
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!
    var page = 1
    var catId:String = String()
    var dateString = String()
    var stopApi = String()
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        //formatter.dateFormat = "yyyy/MM/dd"
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
   
    var getWorkoutListingData = [Getworkout]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        stopApi = "no"
        self.calendar.select(Date())
    
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let myString = formatter.string(from: Date()) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "yyyy-MM-dd"
        // again convert your date to string
        dateString = formatter.string(from: yourDate!)
        //2018-12-21
       
        self.calendar.scope = .week
        
        // For UITest
        self.calendar.accessibilityIdentifier = "calendar"
        
    }
   
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
        let backBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(backbuttonaction))
        self.navigationItem.leftBarButtonItem  = backBarButtonItem
        let sliderImage = UIImage(named: "ic_back_arrow_black")
        
        navigationItem.leftBarButtonItem?.image = sliderImage
        navigationItem.leftBarButtonItem?.tintColor = UIColor.black
        
        callGetWorkoutsAccordingToDateApi()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = false
        
    }
    
    deinit {
        print("\(#function)")
    }
    
    @objc func backbuttonaction(){
        
        self.navigationController?.popViewController(animated:false)
    }
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.calendarHeightConstraint.constant = bounds.height
        self.view.layoutIfNeeded()
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition){
        page = 1
        print("did select date \(self.dateFormatter.string(from: date))")
        let selectedDates = calendar.selectedDates.map({self.dateFormatter.string(from: $0)})
        print("selected dates is \(selectedDates)")
        
        if monthPosition == .next || monthPosition == .previous{
            calendar.setCurrentPage(date, animated: true)
        }
        
        let dateStr = self.dateFormatter.string(from: date)
        dateString = dateStr
        convertToEst(todayDateString: dateStr)
        callGetWorkoutsAccordingToDateApi()
    }
    
    func convertToEst(todayDateString: String) {
        
        let gmtDateformat: DateFormatter = DateFormatter()
        gmtDateformat.timeZone = NSTimeZone(name: "GMT") as TimeZone?
        gmtDateformat.dateFormat = "yyyy-MM-dd"
        let gmtDate: NSDate = gmtDateformat.date(from: todayDateString)! as NSDate
        let estDateformat: DateFormatter = DateFormatter()
        estDateformat.timeZone = NSTimeZone(name: "EST") as TimeZone?
        estDateformat.dateFormat = "yyyy-MM-dd"
        let estDate: NSDate = estDateformat.date(from: gmtDateformat.string(from: gmtDate as Date))! as NSDate
        print("EST Time: \(estDate)")
        let date = "\(estDate)".components(separatedBy: " ")
        let firstindex = date[0]
        dateString = firstindex
        print("EST Time \(dateString)")
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar){
        print("\(self.dateFormatter.string(from: calendar.currentPage))")
    }
}

extension ScheduleVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getWorkoutListingData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:ScheduleTVCell = tableView.dequeueReusableCell(withIdentifier:"ScheduleTVCell") as! ScheduleTVCell
        cell.selectionStyle = .none
        guard getWorkoutListingData.count != 0 else {return cell}
        
        if let getImageInfo = self.getWorkoutListingData[indexPath.row].workout_image {
            cell.workoutImageView?.sd_addActivityIndicator()
            cell.workoutImageView?.sd_setIndicatorStyle(.gray)
            cell.workoutImageView?.sd_setImage(with: URL(string: getImageInfo), placeholderImage: UIImage(named: "ic_sketchy_background") , options: .continueInBackground) { (downloadImage, error, cacheType, urlValue) in
            }
        }
        if let getImageInfo = self.getWorkoutListingData[indexPath.row].trainer_image {
            cell.trainerImageView?.sd_addActivityIndicator()
            cell.trainerImageView?.sd_setIndicatorStyle(.gray)
            cell.trainerImageView?.sd_setImage(with: URL(string: getImageInfo), placeholderImage: UIImage(named: "ic_sketchy_background") , options: .continueInBackground) { (downloadImage, error, cacheType, urlValue) in
            }
        }
        cell.trainerNameLabel.text = self.getWorkoutListingData[indexPath.row].trainer_name ?? ""
        cell.workoutNameLabel.text = self.getWorkoutListingData[indexPath.row].title ?? ""
        cell.timeLabel.text = "\(self.getWorkoutListingData[indexPath.row].duration ?? "") minutes"
        cell.joinNowButton.tag = indexPath.row
        cell.joinNowButton.addTarget(self, action: #selector(tapToJoinButtonPressed(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //return 155.0
        return 180
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
       
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WorkoutDetailVC") as! WorkoutDetailVC
        if self.getWorkoutListingData[indexPath.row] != nil{
            vc.getWorkoutListingData = self.getWorkoutListingData[indexPath.row]
            vc.startDate = self.getWorkoutListingData[indexPath.row].start_time!
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func tapToJoinButtonPressed(_ sender : UIButton){
        
        let row = sender.tag
        if Reachability.isConnectedToNetwork() == true {
            self.callJoinWorkoutApi(row: row)
        } else {
            self.showAlert(Constants.AlertsMessages.noInternetConnecLocalize)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if stopApi == "no" {
            page += 1
           callGetWorkoutsAccordingToDateApi()
        }
        else {
            print("no")
        }
    }
}

extension ScheduleVC{
    
    func callGetWorkoutsAccordingToDateApi(){
        
        let parameter:[String:Any] = [
            "cat_id": catId,
            "page":"\(page)",
            "date":dateString,
            "user_id":getSharedPrefrance(key:"id")
        ]
        if page == 1{
            self.getWorkoutListingData.removeAll()
        }
        
        executePOST(view: self.view, path: Constants.APIs.LIVEURL+Constants.APIs.Api_Workout_getWorkout, parameter: parameter){ response in
            
            print(response)
            let status = response["ResponseCode"].stringValue
            
            if(status == "true"){
                
                for i in response["GetWorkoutResponse"].arrayValue{
                    self.getWorkoutListingData.append(Getworkout(json:i.dictionaryObject!)!)
                }
                self.scheduletv.reloadData()
            }else{
                self.stopApi = "yes"
                self.scheduletv.reloadData()
                self.showToast(message:response["MessageWhatHappen"].stringValue)
            }
        }
    }
    
    func callJoinWorkoutApi(row:Int){
        
        let parameter:[String:Any] = [
            "user_id":getSharedPrefrance(key:"id"),
            "workout_id":self.getWorkoutListingData[row].workout_id ?? ""
        ]
        
        executePOST(view: self.view, path: Constants.APIs.LIVEURL+Constants.APIs.Api_Workout_workoutJoin, parameter: parameter){ response in
            print(response)
            let status = response["ResponseCode"].stringValue
            
            if(status == "true"){
                //print(response)
                self.showToast(message:response["MessageWhatHappen"].stringValue)
            }else{
                self.showToast(message:response["MessageWhatHappen"].stringValue)
            }
        }
    }
}
