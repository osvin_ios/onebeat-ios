//
//  CustomImageFlowLayoutForOne.m
//  OneBeat
//
//  Created by osvinuser on 20/12/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//

#import "CustomImageFlowLayoutForOne.h"

@implementation CustomImageFlowLayoutForOne

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        self.minimumLineSpacing = 1.0;
        self.minimumInteritemSpacing = 1.0;
        self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    }
    return self;
}

- (CGSize)itemSize
{
    NSInteger numberOfColumns = 1;
    CGFloat itemWidth = (CGRectGetWidth(self.collectionView.frame) - (numberOfColumns - 1)) / numberOfColumns;
    return CGSizeMake(itemWidth,itemWidth);
}

@end
