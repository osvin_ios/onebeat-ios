//
//  AppDelegate.swift
//  OneBeat
//
//  Created by osvinuser on 11/12/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import UserNotifications
import StoreKit

var push_Data: PostNotificationClass?

//var rtc_appkey      = "kv6GnTTHs4Bak3dUs8SIm9lLgwFUXSqXhDgw"
//var rtc_appsecret   = "mHoNM1hizmu2fCc5ewe25qsPSDsOchYdQKog"
//var rtc_domain      = "zoom.us"

let appDelegate = UIApplication.shared.delegate as! AppDelegate
var meetingId = String()
var workOutId = String()
var workoutTitle = String()
var action = Int()
var duration = String()
var checkPushData = false

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,WFHardwareConnectorDelegate{//,MobileRTCAuthDelegate {
    
    var hardwareConnector: WFHardwareConnector?
    var window: UIWindow?
    var deviceOrientation = UIInterfaceOrientationMask.portrait
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return deviceOrientation
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool{
        
         IQKeyboardManager.shared.enable = true
         //IQKeyboardManager.shared.previousNextDisplayMode = .alwaysShow

        //For Push Notifications:-
        self.registerForPushNotifications(application: application)

        UIApplication.shared.applicationIconBadgeNumber = 0
        let center = UNUserNotificationCenter.current()
        center.removeAllDeliveredNotifications()
        //HardWare
        hardwareConnector = WFHardwareConnector.shared()
        hardwareConnector!.enableBTLE(true)

        hardwareConnector!.delegate = self
        hardwareConnector!.sampleRate = 0.5
        //hardwareConnector?.settings.useMetricUnits = Bool(NSLocale.current[NSLocale.Key.usesMetricSystem])

        //sdkAuth()
        // Authenticate usage of Zoom MobileRTC SDK.
        ZoomService.sharedInstance.authenticateSDK()

        if let userInfo = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [String: AnyObject] {
            checkPushData = true
           
        }
        self.changeRootViewController(selectedIndexOfTabBar: 0)
        
        return true
    }

    func hardwareConnector(_ hwConnector: WFHardwareConnector?, connectedSensor connectionInfo: WFSensorConnection?){
        if let anInfo = connectionInfo
        {
            print("hardwareConnector:connectedSensor: \(anInfo)")
        }
    }
    //--------------------------------------------------------------------------------
    
    func hardwareConnector(_ hwConnector: WFHardwareConnector?, didDiscoverDevices connectionParams: Set<AnyHashable>?, searchCompleted bCompleted: Bool){
        
        if let aParams = connectionParams{
            print("hardwareConnector:didDiscoverDevices:searchCompleted: \(bCompleted) \n\(aParams)")
        }
    }
    
    func hardwareConnector(_ hwConnector: WFHardwareConnector?, disconnectedSensor connectionInfo: WFSensorConnection?) {
        if let anInfo = connectionInfo{
            print("hardwareConnector:disconnectedSensor: \(anInfo)")
        }
    }
    //--------------------------------------------------------------------------------
    
    func hardwareConnector(_ hwConnector: WFHardwareConnector?, stateChanged currentState: WFHardwareConnectorState_t){
        print(String(format: "hardwareConnector:stateChanged: \(currentState)"))
    }
    
    func hardwareConnectorHasData(){
    }
    //--------------------------------------------------------------------------------
    
    func hardwareConnector(_ hwConnector: WFHardwareConnector?, hasFirmwareUpdateAvailableFor connectionInfo: WFSensorConnection?, required `required`: Bool, withWahooUtilityAppURL wahooUtilityAppURL: URL?){
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        print("applicationWillResignActive")
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        print("applicationDidEnterBackground")
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        print("applicationWillEnterForeground")
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        print("applicationDidBecomeActive")
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        print("applicationWillTerminate")
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        
        MobileRTC.shared()?.getMeetingService()?.leaveMeeting(with:LeaveMeetingCmd_Leave)
        MobileRTC.shared()?.getMeetingService()?.leaveMeeting(with:LeaveMeetingCmd_End)
        
        self.saveContext()
        
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "OneBeat")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

extension AppDelegate {
    
    //Ios 10 delegates for Push Notifications
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options:   UNNotificationPresentationOptions) -> Void) {
        print("Handle push from foreground")
        
        if let pushDict = notification.request.content.userInfo["aps"] as? [String : AnyObject] {
            print("pushresponse = ---",pushDict)
            
            let action = pushDict["action"] as? Int
            if action == 3 {
                workOutId = pushDict["workout_id"] as! String
                meetingId = pushDict["meeting_id"] as! String

            }
        }
        completionHandler([.sound, .alert, .badge])
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Handle push from background or closed")
        
        
        // if you set a member variable in didReceiveRemoteNotification, you will know if this is from closed or background
        print("\(response.notification.request.content.userInfo)")
        if let responsePush = response.notification.request.content.userInfo["aps"] as? [String : AnyObject] {
            print("pushresponse = ---",response)
            
            action = responsePush["action"] as? Int ?? 0
            if action == 3 {
                workOutId = responsePush["workout_id"] as? String ?? ""
                meetingId = responsePush["meeting_id"] as? String ?? ""
                workoutTitle = responsePush["alert"] as? String ?? ""
                duration = responsePush["time"] as? String ?? ""
                print(workOutId,meetingId)
                UserDefaults.standard.set(workOutId, forKey: "workOutId")
                checkPushData = true

                instantiateViewController()
            }
        }
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        let center = UNUserNotificationCenter.current()
        center.removeAllDeliveredNotifications()
        
        completionHandler()
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        if application.applicationState == .inactive {
            // opened from a push notification when the app was on background
            if let object = userInfo["aps"] {
                print("userInfo->\(object)")
                //checkPushData = true
            }
        } else if application.applicationState == .active {
            // a push notification when the app is running. So that you can display an alert and push in any view
            if let object = userInfo["aps"] {
                print("userInfo->\(object)")
                
            }
        }
    }
    
    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print("APNs registration failed: \(error)")
    
        savesharedprefrence(key:"device_token", value:"123456789")
        
    }
        
    func registerForPushNotifications(application: UIApplication) {
        let center  = UNUserNotificationCenter.current()
        center.delegate = self
        // set the type as sound or badge
        center.requestAuthorization(options: [.sound,.alert,.badge]) { (granted, error) in
            // Enable or disable features based on authorization
        }
        application.registerForRemoteNotifications()
    }
    
    func application(_ application: UIApplication,didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        print("yoooooo------device_token----------",token)
          savesharedprefrence(key:"device_token", value:token)
    
    }
    
    func instantiateViewController() {
       
        if let currentVC = self.getTopViewControllers(viewType: 3) as? UIViewController {
          
            if let name = getSharedPrefrance(key:"first_name") as? String{
                if let mID = Int(meetingId){
                    ZoomService.sharedInstance.joinMeeting(name: name, number: mID, password: "", type: "Video",vc: currentVC)
                }
            }
         }else{
            let appdel: AppDelegate = UIApplication.shared.delegate as! AppDelegate
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let centerVC = mainStoryboard.instantiateViewController(withIdentifier: "NavViewController") as? NavViewController
            appdel.window?.rootViewController = centerVC
            appdel.window?.makeKeyAndVisible()
        }
    }
}

extension AppDelegate {
    
    func changeRootViewController(selectedIndexOfTabBar: Int = 0){
        if getSharedPrefrance(key:"loginsession") == "true"{
            let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let centerViewController = storyboard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
            centerViewController.selectedIndex = selectedIndexOfTabBar
            self.window?.rootViewController = centerViewController
        } else {
            
        }
    }
}

extension AppDelegate {
    
    //MARK:- Get Top VC For Normal User
    // Type 1 means, we will get the Tab Bar Object.
    // Type 2 means, we will get the View Controllers of current Tab Bar.
    // Type 3 means, we will get the current Navigation controller of Current View Controller.
    // Type 4 means, we will get the current view controller of stack.
    func getTopViewControllers(viewType: Int) -> AnyObject? {
        
        switch viewType {
        case 1:
            return self.getRootView()
        case 2:
            guard let rootView = self.getRootView() else { return nil }
            guard let currentControllers = rootView.viewControllers else { return nil }
            return currentControllers as AnyObject
        case 3:
            guard let rootView = self.getRootView() else { return nil }
            guard let currentControllers = rootView.viewControllers else { return nil }
            
            if currentControllers.count > self.getCurrentSelectedIndex(currentRootView: rootView) {
                guard let currentNav = currentControllers[self.getCurrentSelectedIndex(currentRootView: rootView)] as? UINavigationController else { return nil }
                return currentNav as AnyObject
            }
            return nil
        default:
            guard let rootView = self.getRootView() else { return nil }
            guard let currentControllers = rootView.viewControllers else { return nil }
            if currentControllers.count > self.getCurrentSelectedIndex(currentRootView: rootView) {
                guard let currentNav = currentControllers[self.getCurrentSelectedIndex(currentRootView: rootView)] as? UINavigationController else { return nil }
                return currentNav.topViewController as AnyObject
            }
            return nil
        }
    }
    
    // MARK:- Tab Bar Selected Index Value Return
    func getCurrentSelectedIndex(currentRootView:TabBarVC) -> Int {
        
        var currentTabController = 0
        
        let state = UIApplication.shared.applicationState
        
        switch state {
        case .active:
            currentTabController = currentRootView.selectedIndex
        case .background:
            currentTabController = currentRootView.selectedIndex
        default:
            // currentTabController = isAppActive ? currentRootView.selectedIndex : 0
            currentTabController = currentRootView.selectedIndex
        }
        return currentTabController
    }
    
    func getRootView() -> TabBarVC? {
        guard let currentRootView = self.window?.rootViewController as? TabBarVC else { return nil }
        return currentRootView
    }
}
