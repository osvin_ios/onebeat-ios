//
//  WorkoutDetailsScreenVC.swift
//  OneBeat
//
//  Created by osvinuser on 21/12/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//

import UIKit
import Foundation
import CountdownLabel

class liveWorkoutDetailsScreenVC: UIViewController {
    
 @IBOutlet weak var tblObj: UITableView!
    
//    var refreshTimer = Timer()
//    var timerArr = NSMutableArray()
//    var secondsArr = NSMutableArray()
    var workoutlive = [Workoutdetailslive]()
    
    //TimeVar
    var serverDateTimeStr : String?
    var serverDateInDateFormat : Date?
    var startDateInDateFormat : Date?
    var start = Int()
    var today = Int()
    var diff = TimeInterval()
    var startDate = String()
    var page = 1
    var stopApi = String()
    
    override func viewDidLoad() {
       super.viewDidLoad()

       self.tblObj.register(UINib(nibName: "liveWorkoutsTableViewCell", bundle: nil), forCellReuseIdentifier: "liveWorkoutsTableViewCell")
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.viewsettings()
        
        if Reachability.isConnectedToNetwork() == true {
            self.liveSessionsAPI()
        } else {
            self.showAlert(Constants.AlertsMessages.noInternetConnecLocalize)
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func getTimeInMillsec( dateString1: String) {
        let dateString2 = serverDateTimeStr ?? ""
        let Dateformatter = DateFormatter()
        Dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let date1 = Dateformatter.date(from: dateString1)
        let date2 = Dateformatter.date(from: dateString2)
        
        _ = getStart_InMillisecond(toTime: date1!)
        _ = getToday_InMillisecond(toTime: date2!)
        get_Diff()
        let distanceBetweenDates: TimeInterval? = date1?.timeIntervalSince(date2!)
        
        let secondsInAnHour: Double = 3600 * 60 * 60
        let secondsInDays: Double = 86400
        let secondsInWeek: Double = 604800
        
        let hoursBetweenDates = Int((distanceBetweenDates! / secondsInAnHour))
        let daysBetweenDates = Int((distanceBetweenDates! / secondsInDays))
        let weekBetweenDates = Int((distanceBetweenDates! / secondsInWeek))
        
        print(weekBetweenDates,"weeks")//0 weeks
        print(daysBetweenDates,"days")//5 days
        print(hoursBetweenDates,"hours")//120 hours
    }
    
    func getToday_InMillisecond(toTime:Date) -> Int{
        let elapsed = NSDate().timeIntervalSince(toTime)
        print(elapsed)
        today = Int(elapsed)
        return Int(elapsed * 1000)
        
    }
    
    func getStart_InMillisecond(toTime:Date) -> Int{
        let elapsed = NSDate().timeIntervalSince(toTime)
        print(elapsed)
        start = Int(elapsed)
        return Int(elapsed * 1000)
    }
    
    func get_Diff()  {
        let differ = today - start
        diff = TimeInterval(differ)
        print(diff)
    }
    
    func viewsettings(){
        
        self.title = "My Live Sessions"
        self.tabBarController?.tabBar.isHidden = true
        
        let backBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(backbuttonaction))
        self.navigationItem.leftBarButtonItem  = backBarButtonItem
        //let sliderImage = UIImage(named: "ic_back_black")
        let sliderImage = UIImage(named: "ic_back_arrow_black")

        navigationItem.leftBarButtonItem?.image = sliderImage
        navigationItem.leftBarButtonItem?.tintColor = UIColor.black
        
    }
    
    @objc func backbuttonaction(){
        self.navigationController?.popViewController(animated:false)
    }
    
    func serverTime(){
        if self.serverDateTimeStr != nil && self.serverDateTimeStr != ""{
            let serverDateString = self.UTCToLocalStringValue(date: self.serverDateTimeStr ?? "", incomingDateFormat: "yyyy-MM-dd HH:mm:ss", outgoingDateFormat: "yyyy-MM-dd HH:mm:ss")
            
            serverDateInDateFormat = self.getDateFormatterDate(date: serverDateString, incomingDateFormat: "yyyy-MM-dd HH:mm:ss")
           
        }else{}
    }
    
}

extension liveWorkoutDetailsScreenVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.workoutlive.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
       let cell : liveWorkoutsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "liveWorkoutsTableViewCell", for: indexPath) as! liveWorkoutsTableViewCell
       cell.selectionStyle = .none
        //cell.taskimage.sd_setImage(with: URL(string:(self.workoutlive[indexPath.row].userImage!)), placeholderImage: UIImage(named: "placeholder.png"))
        //cell.titleoftask.text = self.workoutlive[indexPath.row].tittle
        //cell.tname.text = self.workoutlive[indexPath.row].userName
        //cell.tsubtitle.text = self.workoutlive[indexPath.row].description
        //cell.timelabel.text = self.workoutlive[indexPath.row].start_time
        //Timernew.configureCell(cell, withTimerArr: timerArr, withSecondsArr: secondsArr, forAt: indexPath)
        
        guard workoutlive.count != 0 else {return cell}
        
        if let getImageInfo = self.workoutlive[indexPath.row].workout_image {
            cell.workoutImageView?.sd_addActivityIndicator()
            cell.workoutImageView?.sd_setIndicatorStyle(.gray)
            cell.workoutImageView?.sd_setImage(with: URL(string: getImageInfo), placeholderImage: UIImage(named: "ic_sketchy_background") , options: .continueInBackground) { (downloadImage, error, cacheType, urlValue) in
            }
        }
        
        cell.workoutName.text = self.workoutlive[indexPath.row].title ?? ""
        cell.durationLabel.text = "\(self.workoutlive[indexPath.row].duration ?? "") mins"
        if let getImageInfo = self.workoutlive[indexPath.row].trainer_image {
            cell.trainerImageView?.sd_addActivityIndicator()
            cell.trainerImageView?.sd_setIndicatorStyle(.gray)
            cell.trainerImageView?.sd_setImage(with: URL(string: getImageInfo), placeholderImage: UIImage(named: "ic_sketchy_background") , options: .continueInBackground) { (downloadImage, error, cacheType, urlValue) in
            }
        }
        cell.trainerNameLabel.text = self.workoutlive[indexPath.row].trainer_name ?? ""
        cell.catTypeLabel.text = self.workoutlive[indexPath.row].cat_name ?? ""
        //cell.startsInTimelabel.text =
        let startDateAndTime = "\(self.workoutlive[indexPath.row].workout_date ?? "")"+" "+"\(self.workoutlive[indexPath.row].start_time ?? "")"
        let startDateString = self.convertTimeFormaters(startDateAndTime)
        startDate = self.workoutlive[indexPath.row].start_time ?? ""
         getTimeInMillsec(dateString1: startDateAndTime)
        
        startDateInDateFormat = self.getDateFormatterDate(date: startDateAndTime, incomingDateFormat: "yyyy-MM-dd HH:mm:ss")
        
        if let serverDateInDateFormat = serverDateInDateFormat, let startDateInDateFormat = startDateInDateFormat{
            
            let startDateComparisionResult:ComparisonResult = serverDateInDateFormat.compare(startDateInDateFormat)
            
            if startDateComparisionResult == ComparisonResult.orderedAscending
            {
                // Server date is smaller than Start date.
                cell.startsTextLabel.isHidden = false
                cell.startsInTimelabel.textColor = UIColor(red: 255/255, green: 78/255, blue: 78/255, alpha: 1)
                cell.startsInTimelabel.setCountDownTime(minutes: diff)
                cell.startsInTimelabel.start()
         
            }
            else if startDateComparisionResult == ComparisonResult.orderedDescending
            {
                // Server date is greater than Start date.
                cell.startsTextLabel.isHidden = true
                cell.startsInTimelabel.textColor = UIColor(red: 78/255, green: 153/255, blue: 255/255, alpha: 1)
                cell.startsInTimelabel.text = "Live now"
            }
            else if startDateComparisionResult == ComparisonResult.orderedSame
            {
                // Server date and Start date are same.
                cell.startsTextLabel.isHidden = true
                cell.startsInTimelabel.textColor = UIColor(red: 78/255, green: 153/255, blue: 255/255, alpha: 1)
                cell.startsInTimelabel.text = "Live now"
            }else{}
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard workoutlive.count != 0 else {return}
//        if let indexPath = tableView.indexPathForSelectedRow{
//            let currentCell = tableView.cellForRow(at: indexPath) as! liveWorkoutsTableViewCell
//            if currentCell.startsInTimelabel.text == "Live now"{
//
//            }else{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "WorkoutDetailVC") as! WorkoutDetailVC
                if self.workoutlive[indexPath.row] != nil{
                    vc.preVC = "liveWorkoutDetailsScreenVC"
                    vc.prevLiveWorkoutListingData = self.workoutlive[indexPath.row]
                    vc.timer_Value = diff
                    self.navigationController?.pushViewController(vc, animated: true)
                }
//            }
//        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 218
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if stopApi == "no" {
            page += 1
            liveSessionsAPI()
        }
        else {
            print("no")
        }
    }
    
}

extension liveWorkoutDetailsScreenVC{
    
    func liveSessionsAPI(){
        
        let parameter = [
            "user_id":getSharedPrefrance(key:"id"),
            "page":"\(page)"
        ]
        
        self.workoutlive.removeAll()
        
        executePOST(view: self.view, path: Constants.APIs.LIVEURL+Constants.APIs.Api_Home_liveSessions, parameter: parameter){ response in
            let status = response["ResponseCode"].stringValue
            print(status)
            //self.view.makeToast(response["MessageWhatHappen"].stringValue)
            if(status == "true"){
                
                for live in response["LiveSessionResponse"].arrayValue{
                    self.workoutlive.append(Workoutdetailslive(json:live.dictionaryObject!)!)
                }
                
                let serverDateTime = response["Server_time"].stringValue
                if serverDateTime.count != 0{
                    self.serverDateTimeStr = serverDateTime
                    self.serverTime()
                }
              
                self.tblObj.reloadData()
                
            }else{
                self.stopApi = "yes"
                self.view.makeToast(response["MessageWhatHappen"].stringValue)
            }
        }
        
    }
    
}


//extension String {
//
//    func toDate(withFormat format: String = "yyyy-MM-dd HH:mm:ss")-> Date?{
//
//        let dateFormatter = DateFormatter()
//        dateFormatter.timeZone = TimeZone(identifier: "Asia/Tehran")
//        dateFormatter.locale = Locale(identifier: "fa-IR")
//        dateFormatter.calendar = Calendar(identifier: .gregorian)
//        dateFormatter.dateFormat = format
//        let date = dateFormatter.date(from: self)
//
//        return date
//
//    }
//}
//extension TimeInterval{
//
//    func stringFromTimeInterval() -> (String,String) {
//
//        let time = NSInteger(self)
//
//        let ms = Int((self.truncatingRemainder(dividingBy: 1)) * 1000)
//        let seconds = time % 60
//        let minutes = (time / 60) % 60
//        let hours = (time / 3600)
//
//        return (String(format:"0.2d",hours),String(format: "%0.2d:%0.2d:%0.2d.%0.3d",hours,minutes,seconds,ms))
//
//    }
//}
