//
//  SignupSecondVC.swift
//  OneBeat
//
//  Created by osvinuser on 11/12/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//

import UIKit
import FloatingTextField
import Alamofire
import SwiftyJSON

class SignupSecondVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var genderTF: FloatingTextField!
    @IBOutlet weak var dobTF: FloatingTextField!
    @IBOutlet weak var poundsTF: FloatingTextField!
    @IBOutlet weak var weightTF: FloatingTextField!
    @IBOutlet weak var inchesTF: FloatingTextField!
    @IBOutlet weak var heightTF: FloatingTextField!
    
    var imagePicker = UIImagePickerController()
    var userImage: UIImage?
    
    var signupDic = [SimpleModle]()
    var signupDic2:Dictionary = [String:Any]()
    
    let pickergenderview = GMPicker()
    var pickergender = [String]()
    let pickerweightview = GMPicker()
    var pickerweight = [String]()
    let pickerinchesview = GMPicker()
    var pickerinches = [String]()
    let pickerpoundsview = GMPicker()
    var pickerpounds = [String]()
    let pickerheightview = GMPicker()
    var pickerheight = [String]()
    var datePicker = GMDatePicker()
    var dateFormatter = DateFormatter()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        settingui()
    }
    
    func settingui(){
        pickergender = ["Male", "Female"]
        pickerweight = (40...140).map { String($0)}
        pickerinches = (50...999).map { String($0)}
        pickerheight = (50...999).map { String($0)}
        
        imagePicker.delegate = self
        
        addNavBarImage()
        userImageView.layer.borderWidth = 1
        userImageView.layer.masksToBounds = false
        userImageView.layer.borderColor = UIColor.clear.cgColor
        userImageView.layer.cornerRadius = userImageView.frame.height/2
        userImageView.clipsToBounds = true
    
        dobTF.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(chooseDOB)))
        dateFormatter.dateFormat = "dd/MM/YYYY"
        genderTF.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(chooseGender)))
       //weightTF.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(chooseweight)))
        poundsTF.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(choosepounds)))
       //heightTF.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(chooseheight)))
        inchesTF.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(chooseinches)))
        
        setupDatePicker()
        setupgenderPickerView()
        //setupweightPickerView()
        setuppoundsPickerView()
        //setupheightPickerView()
        setupinchesPickerView()
    }
    
    @IBAction func submitbuttonaction(_ sender: Any){
        if genderTF.text == ""{
            showToast(message:"Please Enter gender")
        }else if dobTF.text == ""{
            showToast(message:"Please Enter dob")
        }else if poundsTF.text == "" {
            showToast(message:"Please Enter pound")
        }else if weightTF.text == ""{
            showToast(message:"Please Enter weight")
        } else if inchesTF.text == ""{
            showToast(message:"Please Enter height")
        } else {
           signupAPI()
        }
    }
    
    @IBAction func backbuttonaction(_ sender: Any){
        self.navigationController?.popViewController(animated:false)
    }
  
    @IBAction func selectimagebuttonaction(_ sender: Any){
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func choosepounds(){
        pickerpoundsview.choosefortypeofweight()
        pickerpoundsview.show(inVC:self)
    }
    
    @objc func chooseinches(){
        pickerinchesview.chooseheightformate()
        pickerinchesview.show(inVC:self)
    }
    
    @objc func chooseheight(){
        pickerheightview.setupweight()
        pickerheightview.show(inVC:self)
    }
    
    @objc func chooseweight(){
        if self.weightTF.text == "Pounds"{
            pickerweightview.setuppounds()
        }else{
            pickerweightview.setupkgs()
        }
        pickerweightview.show(inVC:self)
    }
    
    @objc func chooseGender(){
        pickergenderview.setupGender()
        pickergenderview.show(inVC: self)
    }
    
    @objc func chooseDOB(){
        datePicker.show(inVC: self)
    }
    
    func settingmethods(){
    }
    
    func getCurrentMillis()->Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    
    func hideLoader(view: UIView) {
        DispatchQueue.main.async(execute: {
            //Indicator.sharedInstance.hideIndicator()
            MBProgressHUD.hide(for: view, animated: true)
        })
    }
    
    func showLoader(view: UIView) {
        DispatchQueue.main.async(execute: {
            //Indicator.sharedInstance.showIndicator()
            MBProgressHUD.showAdded(to: view, animated: true)
        })
    }
    
    func openCamera() {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            let alert  = UIAlertController(title: "Warning", message: "Sorry, this device has no camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary() {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    //MARK:-- ImagePicker delegate
    @objc  func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        
        // The info dictionary may contain multiple representations of the image. You want to use the original.
        guard (info[.originalImage] as? UIImage) != nil else{
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        userImage = info[.originalImage] as? UIImage
        userImageView.image = userImage
        
        // Dismiss the picker.
        dismiss(animated: true, completion: nil)
    }
}

extension SignupSecondVC:UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate{

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if pickerView ==  pickergenderview{
            return pickergender.count
        }else if pickerView == pickerweightview{
            return pickerweight.count
        }else if pickerView == pickerpoundsview{
            return pickerpounds.count
        }else if pickerView == pickerheightview{
            return pickerheight.count
        }else{
            return pickerinches.count
        }
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        if pickerView ==  pickergenderview{
            return pickergender[row]
        }else if pickerView == pickerweightview{
            return pickerweight[row]
        }else if pickerView == pickerpoundsview{
            return pickerpounds[row]
        }else if pickerView == pickerheightview{
            return pickerheight[row]
        }else{
            return pickerinches[row]
        }
    }
    
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        if pickerView ==  pickergenderview{
           genderTF.text = pickergender[row]
        }else if pickerView == pickerweightview{
            weightTF.text = pickerweight[row]
        }else if pickerView == pickerpoundsview{
            poundsTF.text = pickerpounds[row]
        }else if pickerView == pickerheightview{
            heightTF.text = pickerheight[row]
        }else{
            inchesTF.text = pickerinches[row]
        }
    }
 
}

extension SignupSecondVC: GMDatePickerDelegate,GMPickerDelegate {
    
    func gmPicker(_ gmPicker: GMPicker, didSelect string: String){
        if gmPicker == pickergenderview{
             genderTF.text = string
        }else if gmPicker == pickerweightview{
            weightTF.text = string
        }else if gmPicker == pickerpoundsview{
            poundsTF.text = string
        }else if gmPicker == pickerheightview{
            heightTF.text = string
        }else{
            inchesTF.text = string
        }
    }
    
    func gmPickerDidCancelSelection(_ gmPicker: GMPicker){
        
    }
    
    func gmDatePicker(_ gmDatePicker: GMDatePicker, didSelect date: Date){
        print(date)
        dobTF.text = dateFormatter.string(from: date)
    }
    
    func gmDatePickerDidCancelSelection(_ gmDatePicker: GMDatePicker) {
        
    }
    
    fileprivate func setupgenderPickerView(){
        pickergenderview.delegate = self
        pickergenderview.config.animationDuration = 0.5
        pickergenderview.config.cancelButtonTitle = "Cancel"
        pickergenderview.config.confirmButtonTitle = "Confirm"
        pickergenderview.config.contentBackgroundColor = UIColor(red: 253/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1)
        pickergenderview.config.headerBackgroundColor = UIColor(red: 244/255.0, green: 244/255.0, blue: 244/255.0, alpha: 1)
        pickergenderview.config.confirmButtonColor = UIColor.black
        pickergenderview.config.cancelButtonColor = UIColor.black
    }
    
    fileprivate func setupweightPickerView(){
        pickerweightview.delegate = self
        pickerweightview.config.animationDuration = 0.5
        pickerweightview.config.cancelButtonTitle = "Cancel"
        pickerweightview.config.confirmButtonTitle = "Confirm"
        pickerweightview.config.contentBackgroundColor = UIColor(red: 253/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1)
        pickerweightview.config.headerBackgroundColor = UIColor(red: 244/255.0, green: 244/255.0, blue: 244/255.0, alpha: 1)
        pickerweightview.config.confirmButtonColor = UIColor.black
        pickerweightview.config.cancelButtonColor = UIColor.black
    }
    
    fileprivate func setuppoundsPickerView(){
        pickerpoundsview.delegate = self
        pickerpoundsview.config.animationDuration = 0.5
        pickerpoundsview.config.cancelButtonTitle = "Cancel"
        pickerpoundsview.config.confirmButtonTitle = "Confirm"
        pickerpoundsview.config.contentBackgroundColor = UIColor(red: 253/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1)
        pickerpoundsview.config.headerBackgroundColor = UIColor(red: 244/255.0, green: 244/255.0, blue: 244/255.0, alpha: 1)
        pickerpoundsview.config.confirmButtonColor = UIColor.black
        pickerpoundsview.config.cancelButtonColor = UIColor.black
    }
    
    fileprivate func setupheightPickerView(){
        pickerheightview.delegate = self
        pickerheightview.config.animationDuration = 0.5
        pickerheightview.config.cancelButtonTitle = "Cancel"
        pickerheightview.config.confirmButtonTitle = "Confirm"
        pickerheightview.config.contentBackgroundColor = UIColor(red: 253/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1)
        pickerheightview.config.headerBackgroundColor = UIColor(red: 244/255.0, green: 244/255.0, blue: 244/255.0, alpha: 1)
        pickerheightview.config.confirmButtonColor = UIColor.black
        pickerheightview.config.cancelButtonColor = UIColor.black
    }
    
    fileprivate func setupinchesPickerView(){
        pickerinchesview.delegate = self
        pickerinchesview.config.animationDuration = 0.5
        pickerinchesview.config.cancelButtonTitle = "Cancel"
        pickerinchesview.config.confirmButtonTitle = "Confirm"
        pickerinchesview.config.contentBackgroundColor = UIColor(red: 253/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1)
        pickerinchesview.config.headerBackgroundColor = UIColor(red: 244/255.0, green: 244/255.0, blue: 244/255.0, alpha: 1)
        pickerinchesview.config.confirmButtonColor = UIColor.black
        pickerinchesview.config.cancelButtonColor = UIColor.black
    }
    
    fileprivate func setupDatePicker() {
        datePicker.delegate = self
        datePicker.config.startDate = Date()
        datePicker.config.animationDuration = 0.5
        datePicker.datePicker.set18YearValidation()
        datePicker.config.cancelButtonTitle = "Cancel"
        datePicker.config.confirmButtonTitle = "Confirm"
        datePicker.config.contentBackgroundColor = UIColor(red: 253/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1)
        datePicker.config.headerBackgroundColor = UIColor(red: 244/255.0, green: 244/255.0, blue: 244/255.0, alpha: 1)
        datePicker.config.confirmButtonColor = UIColor.black
        datePicker.config.cancelButtonColor = UIColor.black
    }
    
}

extension SignupSecondVC{
    
    func signupAPI(){
        if userImageView.image == UIImage.init(named:"ic_add_image") {
        showToast(message:"Please Enter image")
        } else{
            saveImage1(image:userImageView.image!)
        }
    }
    
    func saveImage1(image:UIImage){
        
        print(signupDic)
        
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        
        let first_name:String = self.signupDic[0].firstname!
        let last_name:String = self.signupDic[0].lastname!
        let email:String = self.signupDic[0].emailaddress!
        let password:String = self.signupDic[0].password!
        
        let gender:String = self.genderTF.text!
        let dob:String = self.dobTF.text!
        let weight_type:String = self.poundsTF.text!
        let height_type:String = self.inchesTF.text!
        let weight:String = self.weightTF.text!
        let height:String = self.heightTF.text!
        let token_id = getSharedPrefrance(key:"device_token")
        let device_id = "1"
        let unique_device_id = deviceID
        let parameter:[String: Any] = [
            "first_name":first_name,
            "last_name":last_name,
            "email":email,
            "gender":gender,
            "password":password,
            "dob":dob,
            "weight":weight,
            "height":height,
            "token_id":token_id,
            "device_id":device_id,
            "unique_device_id":unique_device_id,
            "weight_type":weight_type,
            "height_type":height_type
        ]
        
        let url = Constants.APIs.LIVEURL + Constants.APIs.REGISTER
        print(url)
        self.showLoader(view: self.view)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameter{
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if let imageData = UIImage.jpegData(image)(compressionQuality:0.5){
                let r = arc4random()
                let str = "profile_pic"+String(r)+".jpg"
                
                let parameterName = "profile_image"
                multipartFormData.append(imageData, withName:parameterName, fileName:str, mimeType: "image/jpeg")
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: nil) { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                    self.hideLoader(view: self.view)
                    
                    if let httpStatus = response.response , httpStatus.statusCode == 200 {
                        if response.result.isSuccess{
                            
                            if let dic = response.result.value as? [String: Any]{
                                
                                if let dicnew = dic["signUpResponse"] as? [String: Any]{
                                    savesharedprefrence(key:"dob", value:dicnew["dob"] as! String)
                                    savesharedprefrence(key:"email", value:dicnew["email"] as! String)
                                    savesharedprefrence(key:"first_name", value:dicnew["first_name"] as! String)
                                    savesharedprefrence(key:"gender", value:dicnew["gender"] as! String)
                                    savesharedprefrence(key:"height", value:dicnew["height"] as! String)
                                    savesharedprefrence(key:"height_type", value:dicnew["height_unit"] as! String)
                                    savesharedprefrence(key:"id", value:dicnew["id"] as! String)
                                    savesharedprefrence(key:"last_name", value:dicnew["last_name"] as! String)
                                    savesharedprefrence(key:"profile_image", value:dicnew["profile_image"] as! String)
                                    savesharedprefrence(key:"weight", value:dicnew["weight"] as! String)
                                    savesharedprefrence(key:"weight_type", value:dicnew["weight_unit"] as! String)
                                    savesharedprefrence(key: "userName", value: dicnew["username"] as! String)
                                    savesharedprefrence(key:"loginsession", value:"true")
                                    AppConstants.appDelegete.changeRootViewController(selectedIndexOfTabBar:0)
                                    
                                    self.showAlert(dic["MessageWhatHappen"] as! String)
                                }
                            }
                        }else{
                            self.showAlert("Something went wrong.")
                            self.hideLoader(view: self.view)
                        }
                    } else {
                        self.showAlert("Something went wrong.")
                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                
            }
        }        
    } 
}
