//
//  HistoryTableViewCell.swift
//  OneBeat
//
//  Created by osvinuser on 09/04/19.
//  Copyright © 2019 osvinuser. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var workout_Image: ImageViewDesign!
    @IBOutlet weak var workout_Name_Label: UILabel!
    @IBOutlet weak var workOut_Time_Label: UILabel!
    @IBOutlet weak var userProfile_Image: ImageViewDesign!
    @IBOutlet weak var workOutUser_Name_Label: UILabel!
    @IBOutlet weak var points_Label: UILabel!
    @IBOutlet weak var Heart_Rate_Label: NZLabel!
    @IBOutlet weak var calories_burnt_Label: NZLabel!
    @IBOutlet weak var total_Time_Label: NZLabel!
        
    @IBOutlet weak var trainer_Image: ImageViewDesign!
    @IBOutlet weak var workOut_Image: UIImageView!
    @IBOutlet weak var user_Name_Label: UILabel!
    @IBOutlet weak var detail_Time_Label: UILabel!
    @IBOutlet weak var detail_Description_Label: UILabel!
    @IBOutlet weak var trainer_Name_Label: UILabel!
    @IBOutlet weak var detail_Location_Label: UILabel!
    @IBOutlet weak var people_Joined_Label: UILabel!
    
    @IBOutlet weak var second_Cell_user_Image: ImageViewDesign!
    @IBOutlet weak var secondCell_User_Name: UILabel!
    @IBOutlet weak var secondCell_User_Points: UILabel!
    
    @IBOutlet weak var seeAll_Btn: UIButton!
    var data : WorkoutHistoryClass!
    
    @IBOutlet weak var attendiesCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
