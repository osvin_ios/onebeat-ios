//
//  Videos_TableViewCell.swift
//  OneBeat
//
//  Created by osvinuser on 17/06/19.
//  Copyright © 2019 osvinuser. All rights reserved.
//

import UIKit

class Videos_TableViewCell: UITableViewCell {

    @IBOutlet weak var video_Image: ImageViewDesign!
    
    @IBOutlet weak var title_Label: UILabel!
    
    @IBOutlet weak var description_Label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
