//
//  ScheduleTVCell.swift
//  OneBeat
//
//  Created by osvinuser on 24/12/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//

import UIKit
import CountdownLabel
class ScheduleTVCell: UITableViewCell {

    @IBOutlet weak var workoutImageView: ImageViewDesign!
    @IBOutlet weak var trainerImageView: ImageViewDesign!
    @IBOutlet weak var trainerNameLabel: UILabel!
    @IBOutlet weak var workoutNameLabel: UILabel!
    @IBOutlet weak var clockImageView: ImageViewDesign!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var joinNowButton: ButtonDesign!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
