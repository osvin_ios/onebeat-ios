//
//  Extension+ViewControllers.swift
//  FittCube
//
//  Created by osvinuser on 16/10/18.
//  Copyright © 2018 Osvin. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

extension UIViewController{
    
    func getDateFormatterFromServer(stringDate: String) -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: stringDate)
        return date
    }

    
    //MARK: - Alert Methods
    internal func showAlertView(title OfAlert:String, message OfBody:String) {
        
        let alertController = UIAlertController(title: OfAlert, message: OfBody, preferredStyle: .alert)
        let someAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(someAction)
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    //toast
    
    internal func showToast(message : String) {
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height-100, width: self.view.frame.size.width - 100, height: 60))
        toastLabel.center = self.view.center
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.numberOfLines = 3
        //toastLabel.font = constantsNaming.fontType.kOpenSans_RegularMedium
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 6.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.isHidden = true
        })
    }
    
    internal func show_Image_Toast(image: String) {
        let toastImage = UIImageView(frame: CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height-100, width: 80, height: 80))
        toastImage.center = self.view.center
        toastImage.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastImage.alpha = 1.0
        toastImage.layer.cornerRadius = 10
        toastImage.clipsToBounds  =  true
        toastImage.image = UIImage(named: image)
         self.view.addSubview(toastImage)
        UIView.animate(withDuration: 14.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastImage.alpha = 0.0
        }, completion: {(isCompleted) in
            toastImage.isHidden = true
        })
    }
    
    internal func setGradientColorToView(view : AnyObject, hexString1:String, hexString2:String) {
        
        /// Step 1 set the colors which you want to show in the view /
        //        let colorTop = UIColor().hexStringToUIColor(hex: hexString1)
        //        let colorBottom = UIColor().hexStringToUIColor(hex: hexString2)
        
        /// Step 2 create the gradient layer, add the colors and set the frame /
        let gradient: CAGradientLayer = CAGradientLayer()
        //      gradient.colors = [colorTop.cgColor, colorBottom.cgColor]
        gradient.locations = [0.0, 0.7]
        
        gradient.frame = view.bounds
        view.layer.insertSublayer(gradient, at: 0)
    }
    
    //MARK: - Convert Date to String
    internal func convertDateToString(instance OfDate:Date, instanceOf DateFormat:String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")!
        return dateFormatter.string(from:OfDate)
    }
    
    //MARK: - Convert String to Date
    internal func convertStringToDate(dateofString:String,DateFormat:String) -> Date {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let date = dateFormatter.date(from: dateofString)// create   date from string
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let timeStamp =  dateFormatter.date(from: dateofString)
        return  timeStamp ?? Date()
        
    }
    
    internal func convertStringDateToLocal(dateofString:String/*,DateFormat:String*/) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")!
        let date = dateFormatter.date(from: dateofString)
        
        // change to a readable time format and change to local time zone
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone.local
        let timeStamp = dateFormatter.string(from: date ?? Date())
        
        return timeStamp
    }
    
    func convertstringToDate(stringDate: String) -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")!
        let date = dateFormatter.date(from: stringDate)
        
        if let dateObject = date {
            return dateObject // return the date if date is not equal to null
        }        
        return date
    }
    
    //MARK: - Keyboard Hide Method
    internal func hideKeyboardWhenTappedAround() {
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc internal func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func showAlert(_ message: String)
    {
        let alertController = UIAlertController(title: Constants.appTitle.alertTitle, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alertController, animated: true, completion:nil)
        
    }
    
        
    func roundedof(toPlaces places:Double) -> Double {
        let numberOfPlaces = 2.0
        let multiplier = pow(10.0, numberOfPlaces)
        let rounded1 = round(places * multiplier) / multiplier
        return rounded1
    }
    
    func showAlertWithActions(_ message:String) {
        
        let alertController = UIAlertController(title: Constants.appTitle.alertTitle, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
        { action -> Void in
            
            _ = self.navigationController?.popViewController(animated: true)
            
        })
        present(alertController, animated: true, completion:nil)
    }
    
    func  showAlertWithTwoActions(_message:String) {
        
        let alertController = UIAlertController(title: Constants.appTitle.alertTitle, message: _message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default)
        { action -> Void in
            
            _ = self.navigationController?.popViewController(animated: true)
            
        })
        present(alertController, animated: true, completion:nil)
    }
    
    func  showAlertForDeleteRecording(_message:String) {
        
        let alertController = UIAlertController(title: Constants.appTitle.alertTitle, message: _message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default)
        { action -> Void in
            
            //PrepareToWinViewController.sharedInstance
            
        })
        present(alertController, animated: true, completion:nil)
    }
    
    func addNavBarImage() {
        
        let image = UIImage(named: "ic_logo_small.png") //Your logo url here
        let imageView = UIImageView(image: image)
        navigationItem.titleView = imageView
    }
    
    func getCurrentTimeZone() -> String{
        return String (TimeZone.current.identifier)
    }
   
    func convertTimeFormaters(_ date: String) -> String? {
        
        if date.isEmpty == false {
            let myDateString = date
            
            let dateFormatter = DateFormatter()
            
            dateFormatter.amSymbol = "AM"
            dateFormatter.pmSymbol = "PM"
            
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let myDate = dateFormatter.date(from: myDateString)!
            
            dateFormatter.dateFormat = "dd MMM, h:mm a"
            //            dateFormatter.dateFormat = "HH:mm a"
            let somedateString = dateFormatter.string(from: myDate as Date)
            return somedateString
        }
        return ""
    }
    
    func UTCToLocalStringValue(date:String, incomingDateFormat: String, outgoingDateFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = incomingDateFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = outgoingDateFormat
        
        return dateFormatter.string(from: dt ?? Date())
    }
    
    func getDateFormatterDate(date:String, incomingDateFormat: String) -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = incomingDateFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        return dt
    }
    
    func setGradientColorToView(view : AnyObject, colorTop:UIColor, colorBottom:UIColor) {
        
        /* Step 1 set the colors which you want to show in the view */
        //        let colorTop = UIColor().hexStringToUIColor(hex: hexString1)
        //        let colorBottom = UIColor().hexStringToUIColor(hex: hexString2)
        
        /* Step 2 create the gradient layer, add the colors and set the frame */
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = [colorTop.cgColor, colorBottom.cgColor]
        gradient.locations = [0.0, 0.8]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.cornerRadius = 6
        gradient.frame = view.bounds
        gradient.masksToBounds = true
        view.layer.insertSublayer(gradient, at: 0)
    }
    
    func setGradientColorToViewDiagonally(view : AnyObject, topGradientColor:UIColor, bottomGradientColor:UIColor) {
        
        /* Step 1 set the colors which you want to show in the view */
        //      let colorTop = UIColor().hexStringToUIColor(hex: hexString1)
        //      let colorBottom = UIColor().hexStringToUIColor(hex: hexString2)
        
        /* Step 2 create the gradient layer, add the colors and set the frame */
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        gradientLayer.frame.size.width = self.view.bounds.size.width
        gradientLayer.frame.size.height = view.bounds.size.height
        gradientLayer.colors = [topGradientColor.cgColor, bottomGradientColor.cgColor]
        gradientLayer.locations = [0.0, 0.8]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.0)
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
}
extension UIDatePicker {
    func set18YearValidation() {
        let currentDate: Date = Date()
        var calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        calendar.timeZone = TimeZone(identifier: "UTC")!
        var components: DateComponents = DateComponents()
        components.calendar = calendar
        components.year = -1
        let maxDate: Date = calendar.date(byAdding: components, to: currentDate)!
        components.year = -150
        let minDate: Date = calendar.date(byAdding: components, to: currentDate)!
        self.minimumDate = minDate
        self.maximumDate = maxDate
    }
}

enum DateFormat: String {
    case dateTimeAMPM = "dd/MM/yyyy, hh:mm a"
    case requiredFormat = "dd EEEE, yyyy hh:mm a"
    case requiredFormatNew = "dd MMM, yyyy hh:mm a"
    case dateMonthYear = "MM/yyyy"
    case dateMonthYearWeb = "MM-YYYY"
    case dateTimeUTC = "yyyy-MM-dd'T'HH:mm:ss"
    case dateTime = "yyyy-MM-dd HH:mm:ss"
    case dateOnly = "yyyy-MM-dd"
    case dateNow = "yyyy/MM/dd"
    case timeAmPM = "hh:mm a"
    case timeOnly = "hh:mm"
    case ISTtime = "HH:mm"
    case mmyy = "MM/yy"
    case simpleDate = "dd MM yyyy"
    case datemonthNameYear = "dd MMMM yyyy hh:mm a"
    case requiredFormat_ = "dd MMM yyyy"
    case completeInfoDate = "dd EEEE MMMM yyyy"
    case dateWithHalfMonthStr = "dd EEEE MMM yyyy"
    case dateFormatWithout_a = "dd MMM, yyyy hh:mm"
    case getReviewDateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
}


extension UIView{
    
    func UTCToLocal(format: DateFormat, date1:String, convertedFormat: DateFormat) -> String {
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = format.rawValue
        dateFormatter1.timeZone = TimeZone(abbreviation: "UTC")
        guard let date = dateFormatter1.date(from: date1)else{
            return date1
        }
        dateFormatter1.dateFormat = convertedFormat.rawValue
        dateFormatter1.timeZone = NSTimeZone.local
        
        dateFormatter1.locale = Locale(identifier: "en_US_POSIX") // for solving 24hr issue
        
        let timeStamp = dateFormatter1.string(from: date)
        return timeStamp
    }
}
