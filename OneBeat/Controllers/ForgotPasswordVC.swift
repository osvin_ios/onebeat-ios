//
//  ForgotPasswordVC.swift
//  OneBeat
//
//  Created by osvinuser on 11/12/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//

import UIKit
import FloatingTextField
import Alamofire
import SwiftyJSON

class ForgotPasswordVC: UIViewController{
    
    @IBOutlet weak var emailTF: FloatingTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addNavBarImage()
    }
    
    @IBAction func sendbuttonaction(_ sender: Any){
        if (!isValidEmail(testStr: emailTF.text!)){
            showToast(message:"Please Enter Vaild Email")
        }else{
          self.forgotAPI()
        }
    }
    
    @IBAction func backbuttonaction(_ sender: Any){
        self.navigationController?.popViewController(animated:false)
    }
    
    func isValidEmail(testStr:String) -> Bool{
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}

extension ForgotPasswordVC{
    
    func forgotAPI(){
       
        let email:String = (self.emailTF?.text)!
        let parameter:[String:Any] = [
            "email": email
        ]
        
        print(parameter)
        
        executePOST(view: self.view, path: Constants.APIs.LIVEURL+"/Api/User/forgotpassword", parameter: parameter){ response in
            print(response)
            let status = response["ResponseCode"].stringValue
            if(status == "true"){
                self.showToast(message:response["MessageWhatHappen"].stringValue)
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotmailpopVC") as! ForgotmailpopVC
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                self.showToast(message:response["MessageWhatHappen"].stringValue)
            }
        }
    }
    
}
