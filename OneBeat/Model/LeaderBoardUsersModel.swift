//
//  LeaderBoardUsersModel.swift
//  OneBeat
//
//  Created by osvinuser on 20/03/19.
//  Copyright © 2019 osvinuser. All rights reserved.
//

import Foundation
import Gloss

class LeaderBoardUsersModel:JSONDecodable{
    
    var workout_id : String? = ""
    var user_id : String? = ""
    var username: String? = ""
    var profile_image : String? = ""
    var points : String? = ""
    var like_Status : String? = ""
    
    required init?(json: JSON) {
        
        self.workout_id = "workout_id" <~~ json
        self.user_id = "user_id" <~~ json
        self.username = "username" <~~ json
        self.profile_image = "profile_image" <~~ json
        self.points = "points" <~~ json
        self.like_Status = "likestatus" <~~ json
        
    }
}
