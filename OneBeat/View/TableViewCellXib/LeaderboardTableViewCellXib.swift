//
//  LeaderboardTableViewCellXib.swift
//  OneBeat
//
//  Created by osvinuser on 20/03/19.
//  Copyright © 2019 osvinuser. All rights reserved.
//

import UIKit

class LeaderboardTableViewCellXib: UITableViewCell {

    @IBOutlet weak var backgroundColorLabel: UILabel!    
    @IBOutlet weak var backgroundColorView: UIView!
    @IBOutlet weak var userImageView: ImageViewDesign!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userScoreLabel: UILabel!
    
    var like_Btn_Method : (() -> Void)? = nil
    
    @IBOutlet weak var like_Emoji_Btn: UIButton!
    
    @IBOutlet weak var like_ImageView: UIImageView!
    @IBOutlet weak var numbering_Label: UILabel!
    //Constraints:
    @IBOutlet weak var backgroundColorLabelTrailingConstraint: NSLayoutConstraint!
        
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

        
    @IBAction func like_Btn_Action(_ sender: UIButton) {
        if let likeButtonAction = self.like_Btn_Method {
            likeButtonAction()
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
