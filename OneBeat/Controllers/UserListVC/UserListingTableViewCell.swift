//
//  UserListingTableViewCell.swift
//  OneBeat
//
//  Created by osvinuser on 19/06/19.
//  Copyright © 2019 osvinuser. All rights reserved.
//

import UIKit

class UserListingTableViewCell: UITableViewCell {

    @IBOutlet weak var user_name_Label: UILabel!
    @IBOutlet weak var user_Image: UIImageView!
    @IBOutlet weak var follow_Btn: ButtonDesign!
    @IBOutlet weak var add_Friend_Btn: ButtonDesign!
    
    var follow_btn_Method : (() -> Void)? = nil
    var add_Frnd_Btn_Method : (() -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func follow_Btn_Action(_ sender: ButtonDesign) {
        if let follow_BtnAction = follow_btn_Method {
            follow_BtnAction()
        }
    }
    
    @IBAction func friend_Request_Btn(_ sender: ButtonDesign) {
        if let add_Friend = add_Frnd_Btn_Method {
            add_Friend()
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
