//
//  Getworkout.swift
//  OneBeat
//
//  Created by osvinuser on 20/12/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//


import Foundation
import Gloss

class Getworkout:JSONDecodable {
    
    var workout_id: String? = ""
    var tittle: String? = ""
    var user_id: String? = ""
    var start_time: String? = ""
    var end_time: String? = ""
    var workout_date:String? = ""
    var duration: String? = ""
    var description: String? = ""
    //var meeting_id: String? = ""
    //var meeting_id: Int? = 0
    var meeting_id: Any? = nil
    var join_url: String? = ""
    var cat_id: String? = ""
    var catName: String? = ""
    var userName: String? = ""
    var heart_rate : String? = ""
    var max_hr : String? = ""
    var title: String? = ""
    var trainer_id: String? = ""
    var workout_image: String? = ""
    var cat_name: String? = ""
    var trainer_name: String? = ""
    var trainer_image: String? = ""
    var is_join: Int? = 0
    var calories : String? = ""
    var instructor_id: String? = ""
    var join_created_datetime: String? = ""
    var workout_created_datetime: String? = ""
    var attendee_count: String? = ""
    var points :String? = ""
    var attendee_Workout_time : String? = ""
    var get_trainer: Dictionary<String,Any>?
    var get_attendee_image: [Dictionary<String,Any>]?
    
//    "get_trainer": {
//    "trainer_id": "55",
//    "trainer_name": "Mary Jaim",
//    "trainer_profile_image": "",
//    "trainer_location": ""
//    },
//    "get_attendee_image": [
//    {
//    "id": "45",
//    "attendee_image": "http://192.168.1.90/onebeat/assets/profile_image/20181217_180036.jpg"
//    },
//    {
//    "id": "54",
//    "attendee_image": "http://192.168.1.90/onebeat/assets/profile_image/20181219_130927.jpg"
//    },
//    {
//    "id": "55",
//    "attendee_image": ""
//    }
//    ]
    
    required init?(json: JSON) {
        
        self.workout_id = "workout_id" <~~ json
        self.tittle = "tittle" <~~ json
        self.user_id = "user_id" <~~ json
        self.start_time = "start_time" <~~ json
        self.end_time = "end_time" <~~ json
        self.workout_date = "workout_date" <~~ json
        self.duration = "duration" <~~ json
        self.description = "description" <~~ json
        self.meeting_id = "meeting_id" <~~ json
        self.join_url = "join_url" <~~ json
        self.cat_id = "cat_id" <~~ json
        self.catName = "catName" <~~ json
        self.userName = "userName" <~~ json
        self.points = "points" <~~ json
        self.title = "title" <~~ json
        self.trainer_id = "trainer_id" <~~ json
        self.workout_image = "workout_image" <~~ json
        self.cat_name = "cat_name" <~~ json
        self.trainer_name = "trainer_name" <~~ json
        self.trainer_image = "trainer_image" <~~ json
        self.is_join = "is_join" <~~ json
        self.calories = "calories" <~~ json
        self.heart_rate =  "heart_rate" <~~ json
        self.max_hr =  "max_hr" <~~ json
        self.instructor_id = "instructor_id" <~~ json
        self.join_created_datetime = "join_created_datetime" <~~ json
        self.workout_created_datetime = "workout_created_datetime" <~~ json
        self.attendee_count = "attendee_count" <~~ json
        self.get_trainer = "get_trainer" <~~ json
        self.get_attendee_image = "get_attendee_image" <~~ json
        self.attendee_Workout_time = "attendee_workout_time" <~~ json

    }    
}
