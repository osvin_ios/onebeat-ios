//
//  GetUsersPointsDetailClass.swift
//  OneBeat
//
//  Created by osvinuser on 08/04/19.
//  Copyright © 2019 osvinuser. All rights reserved.
//

import Foundation
import Gloss

class GetUsersPointsDetailClass:JSONDecodable{
    
    var workout_id : String? = ""
    var user_id : String? = ""
    var username: String? = ""
    var profile_image : String? = ""
    var points : String? = ""
    var heart_rate: String? = ""
    var attendee_workout_time: String? = ""
    var name: String? = ""
    var id:String? = ""
    var user_Image : String? = ""
    
    required init?(json: JSON) {
        
        self.workout_id = "workout_id" <~~ json
        self.user_id = "user_id" <~~ json
        self.username = "name" <~~ json
        self.profile_image = "profile_image" <~~ json
        self.points = "points" <~~ json
        self.heart_rate = "heart_rate" <~~ json
        self.attendee_workout_time = "attendee_workout_time" <~~ json
        self.name = "user_name" <~~ json
        self.id = "id" <~~ json
        self.user_Image =  "user_image" <~~ json
    }    
}
