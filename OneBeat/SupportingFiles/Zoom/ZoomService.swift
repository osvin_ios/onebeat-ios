//
//  ZoomService.swift
//  OneBeat
//
//  Created by osvinuser on 27/03/19.
//  Copyright © 2019 osvinuser. All rights reserved.
//

import Foundation

var VC: UIViewController!

private struct ZoomAPI {
    // SDK Info for using MobileRTC.
    
//    static let domain = "zoom.us"
//    static let appKey = "xUzmDgJNQIkIGi2ubnrAF1KjrlosPrupOfPE"        //Unetwrk Keys
//    static let appSecret = "qOXWKV6kmfTsbfXptTYeGH3Bhb6QYL8hLyWB"
    
    static let domain = "zoom.us"
    static let appKey = "xUzmDgJNQIkIGi2ubnrAF1KjrlosPrupOfPE"
    static let appSecret = "qOXWKV6kmfTsbfXptTYeGH3Bhb6QYL8hLyWB"
    
    // API User info for starting calls as API user.
    static let userID = ""
    static let userToken = ""
    
    // Default display name for meetings.
    static let defaultName = ""
}

public struct ZoomMeeting {
    var number: Int
    var password: String = ""
    var topic: String?
    var startTime: Date?
    var timeZone: TimeZone?
    var durationInMinutes: UInt
}

// MARK: - ZoomService API Authentication

class ZoomService: NSObject, MobileRTCAuthDelegate {
    
    static let sharedInstance = ZoomService()
    
    var isAPIAuthenticated = false
    var isUserAuthenticated = false
    var userMeetings: [ZoomMeeting] = []
    
    // Authenticates user to use MobileRTC.
    func authenticateSDK() {
        guard let zoomSDK = MobileRTC.shared() else { return }
        //zoomSDK.mobileRTCDomain = ZoomAPI.domain
        zoomSDK.setMobileRTCDomain(ZoomAPI.domain)
        
        guard let authService = zoomSDK.getAuthService() else { return }
        authService.delegate = self
        authService.clientKey = ZoomAPI.appKey
        authService.clientSecret = ZoomAPI.appSecret
        authService.sdkAuth()
    }
    
    // Handled by MobileRTCAuthDelegate, returns result of authenticateSDK function call.
    func onMobileRTCAuthReturn(_ returnValue: MobileRTCAuthError) {
        guard returnValue == MobileRTCAuthError_Success else {
            print("Zoom: API authentication task failed, error code: \(returnValue.rawValue)")
            return
        }
        isAPIAuthenticated = true
        print("Zoom: API authentication task completed.")
    }
}

// MARK: - ZoomService Meeting Management

extension ZoomService {
    
    // Join a Zoom meeting.
    //func joinMeeting(name: String = ZoomAPI.defaultName, number: Int, password: String = "",type: String = "") {
    func joinMeeting(name: String = ZoomAPI.defaultName, number: Int, password: String = "",type: String = "",vc: UIViewController) {
        guard isAPIAuthenticated || isUserAuthenticated, let meetingService = MobileRTC.shared().getMeetingService() else { return }
        
        VC = vc
        
        var paramDict: [String : Any] = [
            kMeetingParam_Username : name,
            //kMeetingParam_Username : "",
            kMeetingParam_MeetingNumber : "\(number)"
        ]
        
        if password.count > 0 {
            paramDict[kMeetingParam_MeetingPassword] = password
        }
        
        let returnValue = meetingService.joinMeeting(with: paramDict)
        
        guard returnValue == MobileRTCMeetError_Success else {
            print("Zoom: Join meeting task failed, error code: \(returnValue.rawValue)")
            return
        }
        
        print("Zoom: Join meeting task completed.")
        
        //MobileRTC.shared()?.getMeetingSettings()?.enableCustomMeeting = true
        meetingService.delegate = self
        meetingService.customizedUImeetingDelegate = self
        
        //*******************
        MobileRTC.shared()?.getMeetingSettings()?.enableCustomMeeting = true
        //*******************
        
        MobileRTC.shared()?.getMeetingSettings()?.meetingTitleHidden = true
        MobileRTC.shared()?.getMeetingSettings()?.meetingAudioHidden = false
        
        MobileRTC.shared()?.getMeetingSettings()?.meetingInviteHidden = true
        MobileRTC.shared()?.getMeetingSettings()?.meetingParticipantHidden = true
        MobileRTC.shared()?.getMeetingSettings()?.meetingShareHidden = true
        MobileRTC.shared()?.getMeetingSettings()?.meetingMoreHidden = true
        MobileRTC.shared()?.getMeetingSettings()?.setAutoConnectInternetAudio(true)
        
        MobileRTC.shared()?.getMeetingSettings()?.disableDriveMode(true)
        //MobileRTC.shared()?.getMeetingSettings()?.hintHidden = true
        //MobileRTC.shared()?.getMeetingSettings()?.closeCaptionHidden = true
        
        MobileRTC.shared()?.getMeetingSettings()?.bottomBarHidden = true
        
        if type == "Audio"{
            
            MobileRTC.shared()?.getMeetingSettings()?.meetingVideoHidden = true
            
            MobileRTC.shared()?.getMeetingSettings()?.setMuteVideoWhenJoinMeeting(false)
            
            //            MobileRTC.shared()?.getMeetingSettings()?.waitingHUDHidden = true
            //            MobileRTC.shared()?.getMeetingSettings()?.topBarHidden =
            //            MobileRTC.shared()?.getMeetingSettings()?.bottomBarHidden =
            //            MobileRTC.shared()?.getMeetingSettings()?.meetingPasswordHidden =
            //            MobileRTC.shared()?.getMeetingSettings()?.meetingLeaveHidden =
            //            MobileRTC.shared()?.getMeetingSettings()?.autoConnectInternetAudio =
            //            MobileRTC.shared()?.getMeetingSettings()?.muteAudioWhenJoinMeeting =
            //            MobileRTC.shared()?.getMeetingSettings()?.setMuteAudioWhenJoinMeeting =
            //            MobileRTC.shared()?.getMeetingSettings()?.muteVideoWhenJoinMeeting =
            //            MobileRTC.shared()?.getMeetingSettings()?.setMuteVideoWhenJoinMeeting =
            
        }else if type == "Video"{
            
            MobileRTC.shared()?.getMeetingSettings()?.meetingVideoHidden = false
            
            MobileRTC.shared()?.getMeetingSettings()?.setMuteVideoWhenJoinMeeting(false)
            
            //            MobileRTC.shared()?.getMeetingSettings()?.meetingPasswordHidden =
            //            MobileRTC.shared()?.getMeetingSettings()?.meetingLeaveHidden =
            //            MobileRTC.shared()?.getMeetingSettings()?.waitingHUDHidden = true
            //            MobileRTC.shared()?.getMeetingSettings()?.topBarHidden =
            //            MobileRTC.shared()?.getMeetingSettings()?.bottomBarHidden =
            //            MobileRTC.shared()?.getMeetingSettings()?.autoConnectInternetAudio =
            //            MobileRTC.shared()?.getMeetingSettings()?.muteAudioWhenJoinMeeting =
            //            MobileRTC.shared()?.getMeetingSettings()?.setMuteAudioWhenJoinMeeting =
            //            MobileRTC.shared()?.getMeetingSettings()?.muteVideoWhenJoinMeeting =
            //            MobileRTC.shared()?.getMeetingSettings()?.setMuteVideoWhenJoinMeeting =
            
            //MobileRTC.shared()?.getMeetingSettings()?.meetingVideoHidden
            
        }else{}
        
    }
    
    // Start a Zoom meeting immediately.
    func startMeeting(name: String = ZoomAPI.defaultName, number: Int = -1, password: String = "") {
        guard isAPIAuthenticated || isUserAuthenticated, let meetingService = MobileRTC.shared().getMeetingService() else { return }
        
        var paramDict: [String : Any] = [kMeetingParam_Username : name]
        
        if isAPIAuthenticated && !isUserAuthenticated {
            paramDict[kMeetingParam_UserID] = ZoomAPI.userID
            paramDict[kMeetingParam_UserToken] = ZoomAPI.userToken
        }
        
        if number != -1 {
            paramDict[kMeetingParam_MeetingNumber] = "\(number)"
        }
        
        if password.count > 0 {
            paramDict[kMeetingParam_MeetingPassword] = password
        }
        
        let returnValue = meetingService.startMeeting(with: paramDict)
        
        guard returnValue == MobileRTCMeetError_Success else {
            print("Zoom: Start meeting task failed, error code: \(returnValue.rawValue)")
            return
        }
        
        print("Zoom: Start meeting task completed.")
    }
}

extension ZoomService : MobileRTCMeetingServiceDelegate,MobileRTCCustomizedUIMeetingDelegate{
    func onInitMeetingView() {
        print("onInitMeetingView")
        //        //let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewControllerThird") as! ViewControllerThird
        //        //self.present(vc, animated: true, completion: nil)
        
        //        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //        let vc: FilterVC = storyboard.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        //
        //        vc.view.addSubview((MobileRTC.shared()?.getMeetingService()?.meetingView())!)
        //
        //        let navigationController: UINavigationController = UINavigationController(rootViewController: vc)
        //        AppConstants.appDelegete.window?.rootViewController? = navigationController
        
        //        var vc = CustomMeetingViewController()
        //        customMeetingVC = vc
        //        rootVC.addChild(customMeetingVC)
        //        rootVC.view.addSubview(customMeetingVC.view)
        //        customMeetingVC.didMove(toParent: rootVC)
        //        customMeetingVC.view.frame = rootVC.view.bounds
        
//        DispatchQueue.main.async(execute: {
//            MBProgressHUD.hide(for: VC.view, animated: true)
//        })
        VC.view.hideToastActivity()
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LiveViewWithDetailVC") as! LiveViewWithDetailVC
        //VC.navigationController?.pushViewController(vc, animated: true)
        VC.present(vc, animated: true, completion: nil)
        
    }
    
    func onDestroyMeetingView() {
        print("onDestroyMeetingView")
        //self.dismiss(animated: true, completion: nil)
        
        //        customMeetingVC.willMove(toParent: nil)
        //        customMeetingVC.view.removeFromSuperview()
        //        customMeetingVC.removeFromParent()
        //        customMeetingVC = nil
        
    }
    
    func onMeetingStateChange(_ state: MobileRTCMeetingState) {
        
//        DispatchQueue.main.async(execute: {
//            MBProgressHUD.showAdded(to: VC.view, animated: true)
//        })
        VC.view.makeToastActivity(.center)
        
        if state == MobileRTCMeetingState_Idle{
            print("No meeting is running.")
            VC.view.hideToastActivity()
            self.leftEventOrMeetingApi()
            
        }else if state == MobileRTCMeetingState_Connecting{
            print("Connect to the meeting server status.")
        }else if state == MobileRTCMeetingState_InMeeting{
            print("Meeting is ready, in meeting status.")
            
//            DispatchQueue.main.async(execute: {
//                MBProgressHUD.hide(for: VC.view, animated: true)
//            })
            VC.view.hideToastActivity()
            
//            if MobileRTC.shared()?.getMeetingService()?.meetingView() != nil{
//                let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                let vc = storyboard.instantiateViewController(withIdentifier: "LiveViewWithDetailVC") as! LiveViewWithDetailVC
//                VC.navigationController?.pushViewController(vc, animated: true)
//            }else{
//                print("Nil-meetingView")
//            }
            
        }else if state == MobileRTCMeetingState_WebinarPromote{
            print("Upgrade the attendees to panelist in webinar.")
        }else if state == MobileRTCMeetingState_WebinarDePromote{
            print("Demote the attendees from the panelist.")
        }else{
            print("Temp - 5")
        }
        
    }
    
    
    func leftEventOrMeetingApi(){
//        if Constants.runningEventOrMeetingType == ""{
//        }else{
//            if Constants.runningEventOrMeetingType == "Event"{
//                guard let userInfoModel = Methods.sharedInstance.getloginData() else { return }
//                let paramsStr = "userid=\(userInfoModel.id ?? 0)&eventid=\(Constants.runningEventOrMeetingId ?? 0)"
//                print(paramsStr)
//                let url = Constants.APIs.baseURL + Constants.APIs.Events_join
//
//                let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
//                request.httpMethod = "POST"
//                let postString = paramsStr
//                request.httpBody = postString.data(using: String.Encoding.utf8)
//                let task = URLSession.shared.dataTask(with: request as URLRequest){data, response, error in
//                    guard error == nil && data != nil else{
//                        print("error")
//                        return
//                    }
//                    if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200{
//                        print("statusCode should be 200, but is \(httpStatus.statusCode)")
//                        //print("response = \(response)")
//                    }
//                    let responseString = String(data: data!, encoding: String.Encoding.utf8)
//                    //print("responseString = \(responseString)")
//                    Constants.runningEventOrMeetingType = ""
//                    Constants.runningEventOrMeetingId = 0
//                }
//                task.resume()
//
//            }else if Constants.runningEventOrMeetingType == "Meeting"{
//                guard let userInfoModel = Methods.sharedInstance.getloginData() else { return }
//                let paramsStr = "userid=\(userInfoModel.id ?? 0)&meetid=\(Constants.runningEventOrMeetingId ?? 0)"
//                print(paramsStr)
//                let url = Constants.APIs.baseURL + Constants.APIs.Meetings_join
//
//                let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
//                request.httpMethod = "POST"
//                let postString = paramsStr
//                request.httpBody = postString.data(using: String.Encoding.utf8)
//                let task = URLSession.shared.dataTask(with: request as URLRequest){data, response, error in
//                    guard error == nil && data != nil else{
//                        print("error")
//                        return
//                    }
//                    if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200{
//                        print("statusCode should be 200, but is \(httpStatus.statusCode)")
//                        //print("response = \(response)")
//                    }
//                    let responseString = String(data: data!, encoding: String.Encoding.utf8)
//                    //print("responseString = \(responseString)")
//                    Constants.runningEventOrMeetingType = ""
//                    Constants.runningEventOrMeetingId = 0
//
//                    let dataDict:[String: Any] = [:]
//                    // post a notification
//                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadMyMeetingWhenLeaveMeetingNotification"), object: nil, userInfo: dataDict)
//
//                }
//                task.resume()
//
//            }else{}
//        }
    }
    
    
}

// MARK: - ZoomService User + Meeting Scheduling Management

extension ZoomService : MobileRTCPremeetingDelegate{
    
    // Authenticate user as a Zoom member.
    func login(email: String, password: String) {
        guard isAPIAuthenticated, let authService = MobileRTC.shared().getAuthService() else { return }
        authService.login(withEmail: email, password: password, remeberMe: true)
        
    }
    
    // Handled by MobileRTCPremeetingDelegate, returns result of login function call.
    func onMobileRTCLoginReturn(_ returnValue: Int) {
        guard returnValue == 0 else {
            print("Zoom (User): Login task failed, error code: \(returnValue)")
            return
        }
        
        isUserAuthenticated = true
        
        guard let preMeetingService = MobileRTC.shared().getPreMeetingService() else { return }
        preMeetingService.delegate = self
        
        print("Zoom (User): Login task completed.")
    }
    
    // Logout as Zoom member if user is authenticated as Zoom member.
    func logout() {
        guard isUserAuthenticated, let authService = MobileRTC.shared().getAuthService() else { return }
        authService.logoutRTC()
    }
    
    // Handled by MobileRTCPremeetingDelegate, returns result of logout function call.
    func onMobileRTCLogoutReturn(_ returnValue: Int) {
        guard returnValue == 0 else {
            print("Zoom (User): Logout task failed, error code: \(returnValue)")
            return
        }
        
        isUserAuthenticated = false
        
        print("Zoom (User): Logout task completed.")
    }
   
    func sinkListMeeting(_ result: PreMeetingError, withMeetingItems array: [Any]) {
        guard Int(result.rawValue) == 0 else {
            print("Zoom (User): List meeting task failed, error code: \(result)")
            return
        }
        
        userMeetings = []
        
        for meeting in array {
            if let meeting = meeting as? MobileRTCMeetingItem {
                let number = Int(meeting.getMeetingNumber())
                let password = meeting.getMeetingPassword() ?? ""
                let topic = meeting.getMeetingTopic()
                let startTime = meeting.getStartTime()
                
                let timeZone: TimeZone?
                if let timeZoneID = meeting.getTimeZoneID() {
                    timeZone = TimeZone(abbreviation: timeZoneID)
                } else {
                    timeZone = nil
                }
                
                let durationInMinutes = meeting.getDurationInMinutes()
                let zoomMeeting = ZoomMeeting(number: number, password: password, topic: topic, startTime: startTime, timeZone: timeZone, durationInMinutes: durationInMinutes)
                userMeetings.append(zoomMeeting)
            }
        }
        
        print("\nZoom (User): Found \(userMeetings.count) meetings.")
        
        for (index, meeting) in userMeetings.enumerated() {
            print("\n\(index + 1). Meeting number: \(meeting.number)")
            
            if meeting.password.count > 0 {
                print("   Meeting password: \(meeting.password)")
            }
            
            if meeting.topic != nil {
                print("   Meeting Topic: \(meeting.topic!)")
            } else {
                print("   Meeting Topic: <NO TOPIC>")
            }
            
            if meeting.startTime != nil {
                print("   Meeting Start Time: \(meeting.startTime!)")
            } else {
                print("   Meeting Start Time: <NO START TIME>")
            }
            
            if meeting.timeZone != nil {
                print("   Meeting Time Zone: \(meeting.timeZone!)")
            } else {
                print("   Meeting Time Zone: <NO TIME ZONE>")
            }
            
            print("   Meeting Duration: \(meeting.durationInMinutes) minute(s)")
        }
        
        print()
    }
    
    // Schedule a Zoom meeting as a Zoom member.
    func scheduleMeeting(topic: String, startTime: Date, timeZone: TimeZone = NSTimeZone.local, durationInMinutes: TimeInterval) {
        guard isUserAuthenticated, let preMeetingService = MobileRTC.shared().getPreMeetingService(), let meeting = preMeetingService.createMeetingItem() else { return }
        meeting.setMeetingTopic(topic)
        meeting.setStartTime(startTime)
        meeting.setTimeZoneID(timeZone.abbreviation()!)
        meeting.setDurationInMinutes(UInt(durationInMinutes))
        
        preMeetingService.scheduleMeeting(meeting, withScheduleFor: "ABC")
        preMeetingService.destroy(meeting)
    }

    func sinkSchedultMeeting(_ result: PreMeetingError, meetingUniquedID uniquedID: UInt64) {
        guard Int(result.rawValue) == 0 else {
            print("Zoom (User): Schedule meeting task failed, error code: \(result)")
            return
        }
        
        print("Zoom (User): Schedule meeting task completed.")
    }
    
    // Edit an existing Zoom meeting as a Zoom member by providing a ZoomMeeting object.
    func editMeeting(_ zoomMeeting: ZoomMeeting, topic: String? = nil, startTime: Date? = nil, timeZone: TimeZone? = nil, durationInMinutes: TimeInterval? = nil) {
        editMeeting(number: zoomMeeting.number, topic: topic, startTime: startTime, timeZone: timeZone, durationInMinutes: durationInMinutes)
    }
    
    // Edit an existing Zoom meeting as a Zoom member by providing a Zoom meeting number.
    func editMeeting(number: Int, topic: String? = nil, startTime: Date? = nil, timeZone: TimeZone? = nil, durationInMinutes: TimeInterval? = nil) {
        
        guard isUserAuthenticated, let preMeetingService = MobileRTC.shared().getPreMeetingService(), let meeting = preMeetingService.getMeetingItem(byUniquedID: UInt64(UInt(number))) else { return }
        
        if let topic = topic {
            meeting.setMeetingTopic(topic)
        }
        
        if let startTime = startTime {
            meeting.setStartTime(startTime)
        }
        
        if let timeZone = timeZone {
            meeting.setTimeZoneID(timeZone.abbreviation()!)
        }
        
        if let durationInMinutes = durationInMinutes {
            meeting.setDurationInMinutes(UInt(durationInMinutes))
        }
        
        preMeetingService.editMeeting(meeting)
    }
    
    func sinkEditMeeting(_ result: PreMeetingError, meetingUniquedID uniquedID: UInt64) {
        guard Int(result.rawValue) == 0 else {
            print("Zoom (User): Edit meeting task failed, error code: \(result)")
            return
        }
        
        print("Zoom (User): Edit meeting task completed.")
    }
    
    // Delete an existing event as a Zoom member by providing a ZoomMeeting object.
    func deleteMeeting(_ zoomMeeting: ZoomMeeting) {
        deleteMeeting(number: zoomMeeting.number)
    }
    
    // Delete an existing event as a Zoom member by providing a Zoom meeting number.
    func deleteMeeting(number: Int) {
        guard isUserAuthenticated, let preMeetingService = MobileRTC.shared().getPreMeetingService(), let meeting = preMeetingService.getMeetingItem(byUniquedID: UInt64(UInt(number))) else { return }
        preMeetingService.deleteMeeting(meeting)
    }

    func sinkDeleteMeeting(_ result: PreMeetingError) {
        guard Int(result.rawValue) == 0 else {
            print("Zoom (User): Delete meeting task failed, error code: \(result)")
            return
        }
        
        print("Zoom (User): Delete meeting task completed.")
    }
    
}
