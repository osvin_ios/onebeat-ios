//
//  User_Profile_ViewController.swift
//  OneBeat
//
//  Created by osvinuser on 19/06/19.
//  Copyright © 2019 osvinuser. All rights reserved.
//

import UIKit
import JNPhotosViewer

class User_Profile_ViewController: UIViewController {

    @IBOutlet weak var points_Label: UILabel!
    @IBOutlet weak var name_Label: UILabel!
    @IBOutlet weak var user_Image: UIImageView!
    @IBOutlet weak var heart_RateLabel: UILabel!
    @IBOutlet weak var workout_Label: UILabel!
    @IBOutlet weak var total_Time_Label: UILabel!
    @IBOutlet weak var age_Label: UILabel!
    @IBOutlet weak var weight_Label: UILabel!
    @IBOutlet weak var likes_Label: UILabel!
    @IBOutlet weak var follower_Label: UILabel!
    @IBOutlet weak var location_Label: UILabel!
    @IBOutlet weak var total_Friends_Label: UILabel!
    
    var user_profile : Users_listing_Model?
    var user_ID = String()
    var hours = String()
    var jnPhoto = JNPhoto()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        User_Profile_Api()
        let getAge = getSharedPrefrance(key: "dob")
        calcAge(age: getAge)
        // Do any additional setup after loading the view.
    }
    
    func calcAge(age: String) {
        
        let myFormatte = DateFormatter()
        myFormatte.dateFormat = "dd/MM/yyyy"
        let finalDate : Date = myFormatte.date(from: age)!
        let now = Date()
        let calendar = Calendar.current
        let ageComponents = calendar.dateComponents([.year], from: finalDate, to: now)
        let User_age = Double("\(ageComponents.year!)") as! Double
        age_Label.text = "\(User_age)"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = true
    }
    
    func convertTo_Min_Hrs_Sec(value: Int) {
        
        let min = (value/60)
        let hrs = value/3600
        if min > 60 {
            let total_Hours = "\(hrs)" + "+" + " " + "hrs"
            hours = "\(total_Hours)"
            print(hours)
            total_Time_Label.text = hours
        } else {
            hours = "\(min)" + "+" + " " + "min"
            print(hours)
            total_Time_Label.text = hours
        }
    }

    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func full_Image_Btn(_ sender: UIButton) {
        var jnPhoto = JNPhoto()
        jnPhoto.image = user_Image.image
        
        let viewController = JNPhotosViewerViewController()
        viewController.imagesList = [jnPhoto]
//        viewController.delegate = self
        viewController.showDownloadButton = false
        self.present(viewController, animated: true, completion: nil)
    }
    
    func updateUI() {
        points_Label.text = user_profile?.points ?? ""
        name_Label.text = user_profile?.user_Name ?? ""
        heart_RateLabel.text = user_profile?.maxhr ?? ""
        workout_Label.text = user_profile?.workout ?? ""
        weight_Label.text = user_profile?.weight ?? ""
        likes_Label.text = user_profile?.likes ?? ""
        follower_Label.text = user_profile?.followers ?? ""
        total_Friends_Label.text = user_profile?.total_Friends ?? ""
        location_Label.text = getSharedPrefrance(key: "location")
        let image = user_profile?.profile_image ?? ""
        user_Image.sd_setImage(with: URL(string:image), placeholderImage: UIImage(named: "placeholder.png"))
        
        guard let time = Int(user_profile?.time ?? "") else { return  }
        convertTo_Min_Hrs_Sec(value: time)
    }
    
    @IBAction func likes_Btn(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "User_Likes_ViewController") as? User_Likes_ViewController
        navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func follow_Btn(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "Followers_ViewController") as? Followers_ViewController
        navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func friends_Btn(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "Friends_ViewController") as? Friends_ViewController
        navigationController?.pushViewController(vc!, animated: true)
    }
    
    func User_Profile_Api(){
        let parameter = [
            "user_id": user_ID
        ]
        
        executePOST(view: self.view, path: Constants.APIs.LIVEURL+"/Api/LeaderBoard/likeDetail", parameter: parameter){ response in
            print(response)
            let status = response["ResponseCode"].stringValue
            print(status)

            if(status == "true") {
                self.user_profile = Users_listing_Model.init(json:response["TotalUserWorkoutResponse"].dictionaryObject!)
                self.updateUI()
            } else {
                self.view.makeToast(response["MessageWhatHappen"].stringValue)
            }
        }
    }
}
