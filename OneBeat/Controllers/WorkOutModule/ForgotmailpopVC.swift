//
//  ForgotmailpopVC.swift
//  OneBeat
//
//  Created by osvinuser on 14/12/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//

import UIKit

class ForgotmailpopVC: UIViewController {

    @IBOutlet weak var emaillabel: UILabel!
    
    let messagestr:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emaillabel.text = messagestr
    }
    
    @IBAction func signinbuttonaction(_ sender: Any){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SigninVC") as! SigninVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
