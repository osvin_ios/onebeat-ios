//
//  TabBarVC.swift
//  OneBeat
//
//  Created by osvinuser on 11/12/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//

import UIKit

class TabBarVC: UITabBarController, UITabBarControllerDelegate, UINavigationControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        // tell our UITabBarController subclass to handle its own delegate methods
        self.delegate = self
        
    }
    
   func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool{
        if viewController.children[0] is MyAccountVC || viewController.children[0] is BTDevicesVC || viewController.children[0] is WorkoutsVC || viewController.children[0] is HomeViewController || viewController.children[0] is LeaderboardVC {
            //viewController.viewDidLoad()
            return true
        }
        return false
    }
    
    // called whenever a tab button is tapped
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController){
        if viewController.children[0] is MyAccountVC ||  viewController.children[0] is BTDevicesVC || viewController.children[0] is WorkoutsVC || viewController.children[0] is HomeViewController || viewController.children[0] is LeaderboardVC{
            viewController.viewDidLoad()
        }
    }
    
    // alternate method if you need the tab bar item
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem){
        
    }
}
