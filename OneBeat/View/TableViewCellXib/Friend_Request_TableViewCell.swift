//
//  Friend_Request_TableViewCell.swift
//  OneBeat
//
//  Created by osvinuser on 06/08/19.
//  Copyright © 2019 osvinuser. All rights reserved.
//

import UIKit

class Friend_Request_TableViewCell: UITableViewCell {

    @IBOutlet weak var user_Image_View: ImageViewDesign!
    @IBOutlet weak var accept_Btn: ButtonDesign!
    @IBOutlet weak var decline_Btn: ButtonDesign!
    @IBOutlet weak var notification_Label: UILabel!
    
    var accept_Method : (() -> Void)? = nil
    var decline_Method : (() -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func accept_Btn_Action(_ sender: ButtonDesign) {
        if let accept = self.accept_Method {
            accept()
        }
    }
    
    @IBAction func decline_Btn_Action(_ sender: ButtonDesign) {
        if let decline = self.decline_Method {
            decline()
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
