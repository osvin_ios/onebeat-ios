//
//  CustomTabBar.swift
//  GetBlushh
//
//  Created by Chaithu on 29/03/18.
//  Copyright © 2018 Arvind Mehta. All rights reserved.
//

import UIKit

class CustomTabBar: UITabBar{
    
    @IBInspectable var height: CGFloat = 0.0
    
    override func sizeThatFits(_ size: CGSize) -> CGSize{
        
        var sizeThatFits = super.sizeThatFits(size)
        print(height)
        
        if height > 0.0{
            sizeThatFits.height = height
        }
        return sizeThatFits
    }

}
