//
//  WorkOutCollectionviewCell.swift
//  OneBeat
//
//  Created by osvinuser on 12/12/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//

import UIKit
import CountdownLabel

class WorkOutCollectionviewCell: UICollectionViewCell{
    
    @IBOutlet weak var workoutImageView: ImageViewDesign!
    @IBOutlet weak var workoutName: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var trainerImageView: ImageViewDesign!
    @IBOutlet weak var trainerNameLabel: UILabel!
    @IBOutlet weak var catTypeLabel: UILabel!
    @IBOutlet weak var startsTextLabel: UILabel!
    @IBOutlet weak var startsInTimelabel: CountdownLabel!
    
    //@IBOutlet weak var startsInTimelabel: SACountingLabel!
}
