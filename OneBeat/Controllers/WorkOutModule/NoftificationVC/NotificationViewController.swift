//
//  FullLiveViewController.swift
//  OneBeat
//
//  Created by osvinuser on 04/04/19.
//  Copyright © 2019 osvinuser. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var notification_TableView: UITableView!
    var meetId = String()
    var stopApi = "no"
    var page = 1
    var leaderBoardUsersListingData = [Notification_Model_Class]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        call()
        likes_Notification_Api()
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(true)
        navigationController?.setNavigationBarHidden(true, animated: true)        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return leaderBoardUsersListingData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = notification_TableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as! NotificationTableViewCell
        
        let message = self.leaderBoardUsersListingData[indexPath.row].message ?? ""
        cell.notification_Label.text =  message
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if stopApi == "no" {
            page += 1
//            likes_Notification_Api()
        }
        else {
            print("no")
        }
    }
    
    func likes_Notification_Api(){

        let parameter = [
            "user_id" : getSharedPrefrance(key:"id"),
            "page":"\(page)"
        ]
    
        executePOST(view: self.view, path: Constants.APIs.LIVEURL+Constants.APIs.like_listing_Api, parameter: parameter){ response in

            let status = response["ResponseCode"].stringValue
            if(status == "true"){
                print(response)
                // self.leaderBoardUsersListingData.removeAll()
                for data in response["leaderBoardLikeResponse"].arrayValue{
                    self.leaderBoardUsersListingData.append(Notification_Model_Class(json:data.dictionaryObject!)!)
                }
                self.notification_TableView.reloadData()
            }else{
                self.stopApi = "yes"
                self.view.makeToast(response["MessageWhatHappen"].stringValue)
            }
        }
    }
}
