//
//  MyButton.swift
//  Mether
//
//  Created by Chaithu on 10/04/18.
//  Copyright © 2018 MetherTech. All rights reserved.
//

import UIKit

    @IBDesignable class MyButton: UIButton
    {
        override func layoutSubviews() {
            super.layoutSubviews()
            
            updateCornerRadius()
        }
        
        @IBInspectable var rounded: Bool = false
            {
            didSet
            {
                updateCornerRadius()
            }
        }
        
        func updateCornerRadius()
        {
            layer.cornerRadius = rounded ? frame.size.height / 2 : 0
            backgroundColor = .clear
            layer.borderWidth = 1
            layer.borderColor = UIColor.white.cgColor
            
        }
    }

