//
//  HomeViewController.swift
//  OneBeat
//
//  Created by osvinuser on 17/06/19.
//  Copyright © 2019 osvinuser. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var videos_TableView: UITableView!
    var player = AVPlayer()
    var leaderBoardUsersListingData = [WorkoutHistoryClass]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        fetch_Videos_Api()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leaderBoardUsersListingData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = videos_TableView.dequeueReusableCell(withIdentifier: "videos_Cell", for: indexPath) as! Videos_TableViewCell
        
        guard leaderBoardUsersListingData.count != 0 else {return cell}
        
        if let getImageInfo = self.leaderBoardUsersListingData[indexPath.row].video_Image_Url {
            cell.video_Image?.sd_addActivityIndicator()
            cell.video_Image?.sd_setIndicatorStyle(.gray)
            cell.video_Image?.sd_setImage(with: URL(string: getImageInfo), placeholderImage: UIImage(named: "ic_sketchy_background") , options: .continueInBackground) { (downloadImage, error, cacheType, urlValue) in
            }
         cell.title_Label.text = self.leaderBoardUsersListingData[indexPath.row].workout_title ?? ""
         cell.description_Label.text = self.leaderBoardUsersListingData[indexPath.row].description ?? ""
        }
        return cell
    }
    
    func play_Video(video_Url: String) {
        let videoURL = URL(string: video_Url)
        player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        if let get_video_Url = self.leaderBoardUsersListingData[indexPath.row].Videos_Url {
            play_Video(video_Url: get_video_Url)
            }
        }

    
    func fetch_Videos_Api(){
        let parameter = [
            "page" : "1",
        ]
        
        executePOST(view: self.view, path: Constants.APIs.LIVEURL+Constants.APIs.workout_Listing, parameter: parameter){ response in
            print(response)
            
            self.videos_TableView.reloadData()
            let status = response["ResponseCode"].stringValue
            if(status == "true"){
                for data in response["WorkoutDetailResponse"].arrayValue {
                  self.leaderBoardUsersListingData.append(WorkoutHistoryClass(json:data.dictionaryObject!)!)
                }
                self.videos_TableView.reloadData()
            } else {
                  self.view.makeToast(response["MessageWhatHappen"].stringValue)
            }
        }
    }
}
