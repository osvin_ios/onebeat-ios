//
//  UserLikes_Follow_TableViewCell.swift
//  OneBeat
//
//  Created by osvinuser on 01/08/19.
//  Copyright © 2019 osvinuser. All rights reserved.
//

import UIKit

class UserLikes_Follow_TableViewCell: UITableViewCell {

    @IBOutlet weak var user_ImageView: ImageViewDesign!
    @IBOutlet weak var user_Name_Label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
