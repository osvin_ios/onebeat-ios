//
//  User_List_ViewController.swift
//  OneBeat
//
//  Created by osvinuser on 19/06/19.
//  Copyright © 2019 osvinuser. All rights reserved.
//

import UIKit

class User_List_ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var User_TableView: UITableView!
    @IBOutlet weak var search_TxtField: UITextField!
    var leaderBoardUsersListingData = [Users_listing_Model]()
    var stopApi = "no"
    var page = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        search_TxtField.delegate = self
        leaderBoardUsersApi()
   
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
         navigationController?.isNavigationBarHidden = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leaderBoardUsersListingData.count
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text?.count == 0 {
            print("empty")
            leaderBoardUsersApi()
        } else {
            print("full")
            leaderBoardUsersApi()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = User_TableView.dequeueReusableCell(withIdentifier: "User_Cell", for: indexPath) as! UserListingTableViewCell
        
        guard leaderBoardUsersListingData.count != 0 else { return cell }
        
        if let getImageInfo = self.leaderBoardUsersListingData[indexPath.row].profile_image {
            cell.user_Image?.sd_addActivityIndicator()
            cell.user_Image?.sd_setIndicatorStyle(.gray)
            cell.user_Image?.sd_setImage(with: URL(string: getImageInfo), placeholderImage: UIImage(named: "ic_sketchy_background") , options: .continueInBackground) { (downloadImage, error, cacheType, urlValue) in
            }
            let add_likeStatus = (self.leaderBoardUsersListingData[indexPath.row].add_Friend_LikeStatus ?? "")
            let follow_likeStatus = (self.leaderBoardUsersListingData[indexPath.row].follow_Status ?? "")
            
            if follow_likeStatus == "1" {
                cell.follow_Btn.setTitle("Unfollow", for: .normal)
                cell.follow_Btn.backgroundColor = UIColor.red
            } else {
                cell.follow_Btn.setTitle("Follow", for: .normal)
                cell.follow_Btn.backgroundColor = UIColor.lightGray
            }

            if add_likeStatus == "1" {
                cell.add_Friend_Btn.setTitle("Unfriend", for: .normal)
                cell.add_Friend_Btn.backgroundColor = UIColor.red
            } else {
               cell.add_Friend_Btn.setTitle("Add Friend", for: .normal)
                cell.add_Friend_Btn.backgroundColor = UIColor(red: 80/255, green: 219/255, blue: 255/255, alpha: 1.0)
            }
            
            cell.follow_btn_Method = {
                let toId = (self.leaderBoardUsersListingData[indexPath.row].user_id ?? "")
                self.follow_Friend_Api(to_id: toId)
//                if cell.follow_Btn.titleLabel?.text == "Follow" {
//                    cell.follow_Btn.setTitle("Unfollow", for: .normal)
//                    cell.follow_Btn.backgroundColor = UIColor.red
//                } else {
//                    cell.follow_Btn.setTitle("Follow", for: .normal)
//                    cell.follow_Btn.backgroundColor = UIColor.lightGray
//                }
            }
            
            cell.add_Frnd_Btn_Method = {
                let toId = (self.leaderBoardUsersListingData[indexPath.row].user_id ?? "")
                self.add_Friend_Api(to_id: toId)
//                if cell.add_Friend_Btn.titleLabel?.text == "Add Friend" {
//                    cell.add_Friend_Btn.setTitle("Unfriend", for: .normal)
//                    cell.add_Friend_Btn.backgroundColor = UIColor.red
//                } else {
//                    cell.add_Friend_Btn.setTitle("Add Friend", for: .normal)
//                    cell.add_Friend_Btn.backgroundColor = UIColor(red: 80/255, green: 219/255, blue: 255/255, alpha: 1.0)
//                }
            }
        }

        cell.user_name_Label.text = self.leaderBoardUsersListingData[indexPath.row].user_Name ?? ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "User_Profile_ViewController") as! User_Profile_ViewController
        vc.user_ID = self.leaderBoardUsersListingData[indexPath.row].user_id ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 96
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if stopApi == "no" {
            page += 1
//            leaderBoardUsersApi()
        }
        else {
            print("no")
        }
    }
    
    func leaderBoardUsersApi() {
        
        let parameter: [String:Any] = ["search"   : search_TxtField.text!,
                                       "user_id"  : getSharedPrefrance(key: "id")
        ]
        
        executePOST(view: self.view, path: "http://184.73.56.196/Api/LeaderBoard/UserImage", parameter: parameter){ response in
            
            let status = response["ResponseCode"].stringValue
            self.leaderBoardUsersListingData.removeAll()
            if(status == "true") {
                print(response)
//                self.leaderBoardUsersListingData.removeAll()
                for data in response["leaderBoardWorkoutResponse"].arrayValue {
                    self.leaderBoardUsersListingData.append(Users_listing_Model(json:data.dictionaryObject!)!)
                }

            } else {
                self.stopApi = "yes"
                self.view.makeToast(response["MessageWhatHappen"].stringValue)
            }
            self.User_TableView.reloadData()
        }
    }
    
    func add_Friend_Api(to_id : String) {
        
        let parameter: [String:Any] = ["from_id"   : getSharedPrefrance(key:"id"),
                                       "to_id"     : to_id
        ]
        //  http://184.73.56.196/Api/LeaderBoard/Useradd
        executePOST(view: self.view, path: Constants.APIs.LIVEURL+Constants.APIs.adddFriendApi, parameter: parameter){ response in
            
            let status = response["ResponseCode"].stringValue
//            self.leaderBoardUsersListingData.removeAll()
            if(status == "true") {
                print(response)
            self.leaderBoardUsersApi()
            } else {
                self.view.makeToast(response["MessageWhatHappen"].stringValue)
            }
            self.User_TableView.reloadData()
        }
    }
    
    func follow_Friend_Api(to_id : String) {
        
        let parameter: [String:Any] = ["from_id"   : getSharedPrefrance(key:"id"),
                                       "to_id"     : to_id
        ]
        //"http://184.73.56.196/Api/LeaderBoard/Userfollow"
        executePOST(view: self.view, path: Constants.APIs.LIVEURL+Constants.APIs.follow_Api, parameter: parameter){ response in
            
            let status = response["ResponseCode"].stringValue
//            self.leaderBoardUsersListingData.removeAll()
            if(status == "true") {
                print(response)
                self.leaderBoardUsersApi()
            } else {
                self.view.makeToast(response["MessageWhatHappen"].stringValue)
            }
            self.User_TableView.reloadData()
        }
    }
}
