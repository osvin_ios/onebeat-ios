//
//  CircleButton.swift
//  Mether
//
//  Created by Chaithu on 10/04/18.
//  Copyright © 2018 MetherTech. All rights reserved.
//

import UIKit

class CircleButton: UIButton
{

   
    override func layoutSubviews()
    {
        super.layoutSubviews()        
        updateCornerRadius()
        
    }
    
    @IBInspectable var rounded: Bool = false
        {
        didSet
        {
            updateCornerRadius()
        }
    }
    
    func updateCornerRadius()
    {
        //layer.cornerRadius = rounded ? frame.size.height / 2 : 0
        
        layer.cornerRadius = 0.5 * bounds.size.width
      //  layer.borderColor = UIColor(red: 224.0/255.0, green: 125.0/255.0, blue: 60.0/255.0, alpha: 1.0) as! CGColor
        
        layer.borderWidth = 2.0
        clipsToBounds = true
        
        
        
        

    }

}
