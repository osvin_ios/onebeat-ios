//
//  liveWorkoutsTableViewCell.swift
//  OneBeat
//
//  Created by osvinuser on 20/03/19.
//  Copyright © 2019 osvinuser. All rights reserved.
//

import UIKit
import CountdownLabel

class liveWorkoutsTableViewCell: UITableViewCell {

    @IBOutlet weak var workoutImageView: ImageViewDesign!
    @IBOutlet weak var workoutName: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var trainerImageView: ImageViewDesign!
    @IBOutlet weak var trainerNameLabel: UILabel!
    @IBOutlet weak var catTypeLabel: UILabel!
    @IBOutlet weak var startsTextLabel: UILabel!
   // @IBOutlet weak var startsInTimelabel: UILabel!
    @IBOutlet weak var startsInTimelabel: CountdownLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
