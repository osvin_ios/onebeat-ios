//
//  Friends_ViewController.swift
//  OneBeat
//
//  Created by osvinuser on 05/08/19.
//  Copyright © 2019 osvinuser. All rights reserved.
//

import UIKit

class Friends_ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var friends_TableView: UITableView!
    var leaderBoardUsersListingData = [Users_listing_Model]()
    var index = 0
    var count = 8
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.friends_TableView.register(UINib(nibName: "UserLikes_Follow_TableViewCell", bundle: nil), forCellReuseIdentifier: "UserLikes_Follow_TableViewCell")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
        friends_TableView.insertSubview(refreshControl, at: 0)
    }
    
    @objc func refresh(sender: UIRefreshControl) {
        sender.beginRefreshing()
        sender.endRefreshing()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if index == 1 {
        return count
        } else {
            return count + 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if index == 0 {
        let cell : UserLikes_Follow_TableViewCell = friends_TableView.dequeueReusableCell(withIdentifier: "UserLikes_Follow_TableViewCell", for: indexPath) as! UserLikes_Follow_TableViewCell
        
        guard leaderBoardUsersListingData.count != 0 else { return cell }
        
        if let getImageInfo = self.leaderBoardUsersListingData[indexPath.row].profile_image {
            cell.user_ImageView?.sd_addActivityIndicator()
            cell.user_ImageView?.sd_setIndicatorStyle(.gray)
            cell.user_ImageView?.sd_setImage(with: URL(string: getImageInfo), placeholderImage: UIImage(named: "ic_sketchy_background") , options: .continueInBackground) { (downloadImage, error, cacheType, urlValue) in
            }
        }
        cell.user_Name_Label.text = self.leaderBoardUsersListingData[indexPath.row].user_Name ?? ""
        return cell
        } else {
            let cell : Friend_Request_TableViewCell = friends_TableView.dequeueReusableCell(withIdentifier: "Friend_Request_TableViewCell", for: indexPath) as! Friend_Request_TableViewCell
            
            guard leaderBoardUsersListingData.count != 0 else { return cell }
            
            if let getImageInfo = self.leaderBoardUsersListingData[indexPath.row].profile_image {
                cell.user_Image_View?.sd_addActivityIndicator()
                cell.user_Image_View?.sd_setIndicatorStyle(.gray)
                cell.user_Image_View?.sd_setImage(with: URL(string: getImageInfo), placeholderImage: UIImage(named: "ic_sketchy_background") , options: .continueInBackground) { (downloadImage, error, cacheType, urlValue) in
                }
            }
            
            cell.accept_Method = {
                self.count -= 1
                self.friends_TableView.reloadData()
            }
            cell.decline_Method = {
                self.count -= 1
                self.friends_TableView.reloadData()
            }
            
            cell.notification_Label.text = self.leaderBoardUsersListingData[indexPath.row].user_Name ?? ""
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       if index == 0 {
           return 80
        } else {
            return 106
        }
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func friend_Segment_Control(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            index = 0
            self.friends_TableView.register(UINib(nibName: "UserLikes_Follow_TableViewCell", bundle: nil), forCellReuseIdentifier: "UserLikes_Follow_TableViewCell")
            self.friends_TableView.reloadData()
        } else {
            index = 1
            self.friends_TableView.register(UINib(nibName: "Friend_Request_TableViewCell", bundle: nil), forCellReuseIdentifier: "Friend_Request_TableViewCell")
            self.friends_TableView.reloadData()
        }
    }
    
    func Followers_Api() {
        
        let parameter: [String:Any] = ["search"   : "search_TxtField.text!",
                                       "user_id"  : getSharedPrefrance(key: "id")
        ]
        //"http://184.73.56.196/Api/LeaderBoard/UserImage"
        executePOST(view: self.view, path: Constants.APIs.LIVEURL+Constants.APIs.followersAPi, parameter: parameter){ response in
            
            let status = response["ResponseCode"].stringValue
            self.leaderBoardUsersListingData.removeAll()
            if(status == "true") {
                print(response)
                // self.leaderBoardUsersListingData.removeAll()
                for data in response["leaderBoardWorkoutResponse"].arrayValue {
                    self.leaderBoardUsersListingData.append(Users_listing_Model(json:data.dictionaryObject!)!)
                }
            } else {
                self.view.makeToast(response["MessageWhatHappen"].stringValue)
            }
            self.friends_TableView.reloadData()
        }
    }
    

}
