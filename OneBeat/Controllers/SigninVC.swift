//
//  SigninVC.swift
//  OneBeat
//
//  Created by osvinuser on 13/12/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//

import UIKit
import FloatingTextField
import Alamofire
import SwiftyJSON
import CommonCrypto

class SigninVC: UIViewController{
    
    @IBOutlet weak var emailTF: FloatingTextField!
    @IBOutlet weak var passwordTF: FloatingTextField!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        addNavBarImage()
   }
    
    @IBAction func backbuttonaction(_ sender: Any){
        self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func signinbuttonaction(_ sender: Any){
        if (!isValidEmail(testStr: emailTF.text!)){
              showToast(message:"Please Enter Vaild Email")
        }else if passwordTF.text == nil{
             showToast(message:"Please Enter Vaild Password")
        }else{
            self.signinapi()
        }
    }
    
    @IBAction func forgotbuttonaction(_ sender: Any){
        //ForgotPasswordVC
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func MD5(_ string: String) -> String? {
        let length = Int(CC_MD5_DIGEST_LENGTH)
        var digest = [UInt8](repeating: 0, count: length)
        
        if let d = string.data(using: String.Encoding.utf8) {
            _ = d.withUnsafeBytes { (body: UnsafePointer<UInt8>) in
                CC_MD5(body, CC_LONG(d.count), &digest)
            }
        }
        
        return (0..<length).reduce("") {
            $0 + String(format: "%02x", digest[$1])
        }
    }
    
    func isValidEmail(testStr:String) -> Bool{
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}

extension SigninVC{
    
    func signinapi(){
        
        let email:String = (self.emailTF?.text)!
        let password:String = (self.passwordTF?.text)!
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        var token_id:String = ""
        if getSharedPrefrance(key:"device_token") .isEmpty{
            token_id = "123345455445543"
        }else{
            token_id = getSharedPrefrance(key:"device_token")
        }
        
        let parameter:[String:Any] = [
            "email": email,
            "password":password,
            "token_id":token_id,
            "device_id":"1",
            "unique_device_id":deviceID,
            ]
        
        print(parameter)
        
        executePOST(view: self.view, path: Constants.APIs.LIVEURL+"/Api/User/login", parameter: parameter){ response in
            print(response)
            let status = response["ResponseCode"].stringValue
            if(status == "true"){
                savesharedprefrence(key:"id", value:response["loginResponse"]["id"].stringValue)
                savesharedprefrence(key:"first_name", value:response["loginResponse"]["first_name"].stringValue)
                savesharedprefrence(key:"email", value:response["loginResponse"]["email"].stringValue)
                savesharedprefrence(key:"last_name", value:response["loginResponse"]["last_name"].stringValue)
                savesharedprefrence(key:"gender", value:response["loginResponse"]["gender"].stringValue)
                savesharedprefrence(key:"dob", value:response["loginResponse"]["dob"].stringValue)
                savesharedprefrence(key:"weight", value:response["loginResponse"]["weight"].stringValue)
                savesharedprefrence(key:"height", value:response["loginResponse"]["height"].stringValue)
                savesharedprefrence(key:"profile_image", value:response["loginResponse"]["profile_image"].stringValue)
                //height_unit
                savesharedprefrence(key:"height_unit", value:response["loginResponse"]["height_unit"].stringValue)
                //weight_unit
                savesharedprefrence(key:"weight_unit", value:response["loginResponse"]["weight_unit"].stringValue)
                savesharedprefrence(key:"userName", value:response["loginResponse"]["username"].stringValue)
                savesharedprefrence(key:"loginsession", value:"true")
                
                AppConstants.appDelegete.changeRootViewController(selectedIndexOfTabBar:0)
                
            }else{
                self.showToast(message:response["MessageWhatHappen"].stringValue)
            }
        }
    }
    
}
