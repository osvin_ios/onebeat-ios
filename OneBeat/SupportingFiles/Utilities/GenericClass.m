//
//  GenericClass.m
//  BitFinex
//
//  Created by Tarun Sachdeva on 6/29/17.
//  Copyright © 2017 Pixel. All rights reserved.
//

#import "GenericClass.h"


@implementation GenericClass

+ (id)genericMethod
{
    static GenericClass * sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}








-(void)renderUIObject: (CGFloat)value borderWidth:(CGFloat)width borderColor:(UIColor*)borderColor object:(UIView*)obj
{
    obj.clipsToBounds      = YES;
    obj.layer.masksToBounds = NO;
    [obj.layer setCornerRadius:value];
    [obj.layer setBorderWidth:width];
    obj.layer.borderColor =  borderColor.CGColor;
    
}
-(void)shadowUIObject : (UIView *)object
{
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:object.bounds];
    object.layer.masksToBounds = NO;
    object.layer.shadowColor = [UIColor clearColor].CGColor;
    object.layer.shadowOffset = CGSizeMake(3.0f, 3.0f);
    object.layer.shadowOpacity = 0.5f;
    object.layer.shadowPath = shadowPath.CGPath;
    object.layer.cornerRadius = 3.0;
}

-(UIAlertController*)showAlertWithMessage :(NSString *)message title:(NSString*)title
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
    {
                             [alertController dismissViewControllerAnimated:YES completion:nil];
                         }];
    [alertController addAction:ok];
    return alertController;
}

-(NSString*)checkIsNull:(NSString*)str
{
    if([str isEqual:[NSNull null]]||[str isEqualToString:@"<null>"]||[str isEqualToString:@"(null)"]||[str isEqualToString:@""]||str==nil)
    {
        return @"";
    }
    return str;
}
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(void)addBottomLine:(UIControl*)obj
{
    CALayer *bottomBorder = [CALayer layer];
    
    bottomBorder.frame = CGRectMake(0.0f, obj.frame.size.height-1, obj.frame.size.width, 1.0f);
    
    bottomBorder.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8].CGColor;
    
    [obj.layer addSublayer:bottomBorder];
}


-(void)addBottomLinediffrentColor:(UIControl*)obj
{
    CALayer *bottomBorder = [CALayer layer];
    
    bottomBorder.frame = CGRectMake(0.0f, obj.frame.size.height-1, obj.frame.size.width, 1.0f);
    
    bottomBorder.backgroundColor = [[UIColor colorWithRed:39.0/255.0 green:38.0/255.0 blue:61.0/255.0 alpha:0.8] colorWithAlphaComponent:0.8].CGColor;
    
    [obj.layer addSublayer:bottomBorder];
}






-(void)addBottomLineForMode:(UIControl *)obj
{
    
    CALayer *bottomBorder = [CALayer layer];
    
    bottomBorder.frame = CGRectMake(0.0f, obj.frame.size.height-1, obj.frame.size.width, 1.0f);
    
    bottomBorder.backgroundColor = [[UIColor colorWithRed:197.0/225.0 green:197.0/225.0 blue:197.0/225.0 alpha:1.0] colorWithAlphaComponent:0.8].CGColor;
    
    [obj.layer addSublayer:bottomBorder];
    
    
    
}


    

-(CGSize)sizeOfText:(NSString*)text font:(UIFont*)font bundingSize:(CGSize)size
{
    NSMutableParagraphStyle *mutableParagraphStyle = [[NSMutableParagraphStyle alloc] init];
    mutableParagraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    NSDictionary *attributes = @{NSFontAttributeName: font,
                                 NSParagraphStyleAttributeName: [mutableParagraphStyle copy]};
    CGRect rect = [text boundingRectWithSize:(CGSize){size.width, size.height}
                                     options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes
                                     context:nil];
    CGFloat height = ceilf(rect.size.height);
    CGFloat width  = ceilf(rect.size.width);
    
    return CGSizeMake(width, height);
}



- (NSString *)getIPAddress
{
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0)
    {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL)
        {
            if(temp_addr->ifa_addr->sa_family == AF_INET)
            {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"])
                {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            temp_addr = temp_addr->ifa_next;
        }
    }
    return address;
}




-(CGFloat)heightForText:(NSString*)text font:(UIFont*)font withinWidth:(CGFloat)width
{
    CGSize constraint = CGSizeMake(width, 20000.0f);
    CGSize size;
    
    CGSize boundingBox = [text boundingRectWithSize:constraint
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{NSFontAttributeName:font}
                                            context:nil].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}


- (NSString *)capitalizeFirstLetterOnlyOfString:(NSString *)string
{
    NSMutableString *result = [string lowercaseString].mutableCopy;
    [result replaceCharactersInRange:NSMakeRange(0, 1) withString:[[result substringToIndex:1] capitalizedString]];
    
    return result;
}



- (void)showAnimate:(UIView*)view
{
    view.transform = CGAffineTransformMakeScale(1.3, 1.3);
    view.alpha = 0;
    [UIView animateWithDuration:.25 animations:^{
        view.alpha = 1;
        view.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

- (void)removeAnimate:(UIView*)view
{
    [UIView animateWithDuration:.25 animations:^{
        view.transform = CGAffineTransformMakeScale(1.3, 1.3);
        view.alpha = 0.0;
    } completion:^(BOOL finished)
    {
        if (finished) {
            [view removeFromSuperview];
        }
    }];
}


- (NSString *)sentenceCapitalizedString:(NSString*)str
{
    if (![str length])
    {
        return [NSString string];
    }
    NSString *uppercase = [[str substringToIndex:1] uppercaseString];
    NSString *lowercase = [[str substringFromIndex:1] lowercaseString];
    return [uppercase stringByAppendingString:lowercase];
}


-(NSAttributedString*)createattributestring:(UIColor*)color simpleString:(NSString*)simpleString otherString:(NSString*)otherString otherStringColor:(UIColor*)otherStringColor
{
    
    
    
    NSDictionary *attrs = @{ NSForegroundColorAttributeName : color };
    
    NSDictionary *attrs1 = @{ NSForegroundColorAttributeName : otherStringColor};
    
    
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:simpleString attributes:attrs];
    
    NSMutableAttributedString *newAttString1 = [[NSMutableAttributedString alloc] initWithString:otherString attributes:attrs1];
    
    [attrStr appendAttributedString:newAttString1];
    
    
    
    
    return attrStr;
}




@end
