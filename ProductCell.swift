
//
//  ProductCell.swift
//  OneBeat
//
//  Created by osvinuser on 14/06/19.
//  Copyright © 2019 osvinuser. All rights reserved.


import UIKit
import StoreKit

class ProductCell: UITableViewCell {
    
  static let priceFormatter: NumberFormatter = {
    let formatter = NumberFormatter()
    
    formatter.formatterBehavior = .behavior10_4
    formatter.numberStyle = .currency
    
    return formatter
  }()
  
    @IBOutlet weak var product_Title: UILabel!
    @IBOutlet weak var product_Price_label: UILabel!
    @IBOutlet weak var subscribe_Btn: ButtonDesign!
    @IBOutlet weak var description_Label: UILabel!
    
    var buyButtonHandler: ((_ product: SKProduct) -> Void)?
  
      var product: SKProduct? {
        didSet {
          guard let product = product else { return }

    //      textLabel?.text = product.localizedTitle

          if OneBeatProduct.store.isProductPurchased(product.productIdentifier) {
            subscribe_Btn.setTitle("Purchased", for: .normal)
            product_Price_label.text = ProductCell.priceFormatter.string(from: product.price)
            product_Title.text = product.localizedTitle
            description_Label.text = product.localizedDescription
            subscribe_Btn.backgroundColor = UIColor.red.withAlphaComponent(0.2)
            subscribe_Btn.isUserInteractionEnabled = false
    //        accessoryType = .checkmark
    //        accessoryView = nil
    //        detailTextLabel?.text = ""
          } else if IAPHelper.canMakePayments() {
            ProductCell.priceFormatter.locale = product.priceLocale
            product_Price_label.text = ProductCell.priceFormatter.string(from: product.price)
            product_Title.text = product.localizedTitle
            description_Label.text = product.localizedDescription
            subscribe_Btn.setTitle("Purchase", for: .normal)
            subscribe_Btn.isUserInteractionEnabled = true
            subscribe_Btn.addTarget(self, action: #selector(buyButtonTapped(_:)), for: .touchUpInside)
    //        accessoryType = .none
    //        accessoryView = self.newBuyButton()
          } else {
            product_Price_label.text = "Not available"
          }
        }
      }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    
//    textLabel?.text = ""
//    detailTextLabel?.text = ""
    accessoryView = nil
  }
  
    @objc func newBuyButton() -> UIButton {
        let button = UIButton(type: .system)
        button.setTitleColor(tintColor, for: .normal)
        button.setTitle("Subscribe", for: .normal)
        button.addTarget(self, action: #selector(ProductCell.buyButtonTapped(_:)), for: .touchUpInside)
        button.sizeToFit()
        return button
  }
  
  @objc func buyButtonTapped(_ sender: AnyObject) {
    buyButtonHandler?(product!)
    show_Loader.show_Loader_Activity()
  }
}
