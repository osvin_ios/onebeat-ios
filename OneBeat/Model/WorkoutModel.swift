//
//  WorkoutModel.swift
//  OneBeat
//
//  Created by osvinuser on 14/03/19.
//  Copyright © 2019 osvinuser. All rights reserved.
//

//import Foundation
//import ObjectMapper
//
//class WorkoutModel: Mappable {
//
//    var workout_id: String?
//    var title: String?
//    var trainer_id: String?
//    var start_time: String?
//    var end_time: String?
//    var workout_date: String?
//    var workout_image: String?
//    var duration: String?
//    var description: String?
//    var meeting_id: String?
//    var join_url: String?
//    var cat_id: String?
//    var cat_name: String?
//    var trainer_name: String?
//    var trainer_image: String?
//    var is_join: Int?
//
//    required init?(map: Map) {
//        mapping(map: map)
//    }
//    
//    //Mapple:-
//    open func mapping(map: Map) {
//        
//        workout_id     <- map["workout_id"]
//        title          <- map["title"]
//        trainer_id     <- map["trainer_id"]
//        start_time     <- map["start_time"]
//        end_time        <- map["end_time"]
//        workout_date     <- map["workout_date"]
//        workout_image     <- map["workout_image"]
//        duration     <- map["duration"]
//        description     <- map["description"]
//        meeting_id     <- map["meeting_id"]
//        join_url     <- map["join_url"]
//        cat_id     <- map["cat_id"]
//        cat_name     <- map["cat_name"]
//        trainer_name     <- map["trainer_name"]
//        trainer_image     <- map["trainer_image"]
//        is_join     <- map["is_join"]
//        
//    }
//    
//}
