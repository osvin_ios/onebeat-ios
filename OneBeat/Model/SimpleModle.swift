//
//  SimpleModle.swift
//  OneBeat
//
//  Created by osvinuser on 17/12/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//

import Foundation
import SwiftyJSON

class SimpleModle{
    
    var firstname: String?
    var password: String?
    var lastname: String?
    var emailaddress: String?
    var gender: String?
    var dob: String?
    var weight:String?
    var weight_type:String?
    var height:String?
    var height_type:String?
    var timezone:String?
    var location:String?
    var latitude:String?
    var longitude:String?
    var username : String?
    
    init(firstname: String?,password: String?,lastname: String?,emailaddress: String?){
        self.firstname = firstname
        self.password = password
        self.lastname = lastname
        self.emailaddress = emailaddress
    }
    
    init(firstname: String?,lastname: String?,emailaddress: String?,gender:String?,dob:String?,weight:String?,weight_type:String?,height:String?,height_type:String?,timezone:String?,location:String?,latitude:String?,longitude:String?, username:String?){
        
        self.firstname = firstname
        self.lastname = lastname
        self.emailaddress = emailaddress
        self.gender = gender
        self.dob = dob
        self.weight = weight
        self.weight_type = weight_type
        self.height = height
        self.height_type = height_type
        self.timezone = timezone
        self.location = location
        self.latitude = latitude
        self.longitude = longitude
        self.username = username
    }
}

class AppInstance: NSObject {
    
    static let shared = AppInstance()
    var userData : User?
    
    override init() {
        super.init()
    }
}

class User: NSObject {
    
    var points = ""
    var workout = ""
    var heart_Rate = ""
    var total_Time = ""
}
