//
//  WorkoutLiveCollectionViewCell.swift
//  OneBeat
//
//  Created by osvinuser on 21/12/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//

import UIKit
import CountdownLabel

class WorkoutLiveCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var livesessionimage: UIImageView!
    @IBOutlet weak var namesubtiitle: UILabel!
    @IBOutlet weak var titile: UILabel!
    @IBOutlet weak var countlabel: CountdownLabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var timerlabel:UIView!
    @IBOutlet weak var userimageoftrainer:UIImageView!
    @IBOutlet weak var countdownLabel1: CountdownLabel!
    
    var livework: Workoutdetailslive?
    var timerT = Timer()
    var timeS = Int()
    
    override func awakeFromNib(){
        super.awakeFromNib()
    
    }
  
}

