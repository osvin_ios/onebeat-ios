//
//  WorkoutsVC.swift
//  OneBeat
//
//  Created by osvinuser on 11/12/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ObjectMapper
import CountdownLabel
import Foundation

class WorkoutsVC: UIViewController, CountdownLabelDelegate, LTMorphingLabelDelegate {
    
    @IBOutlet weak var liveSessionsContainerView: UIView!
    @IBOutlet weak var livesessioncollectionview: UICollectionView!
    @IBOutlet weak var workoutcollectionview: UICollectionView!
    
    //Constraints:
    @IBOutlet weak var liveSessionsContainerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var workoutsContainerViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var workoutCollectionViewHeightConstraint: NSLayoutConstraint!
    //Timer
//    var refreshTimer = Timer()
//    var timerArr = NSMutableArray()
//    var secondsArr = NSMutableArray()
//    let cellPercentWidth: CGFloat = 1.0
//    var Workoutdetails = [Workout_DetailsClass]()
    var customimageflowlayout: CustomImageFlowLayout!
    var workout_categories = [Workout_categories]()
    var workoutlive = [Workoutdetailslive]()
    var workout = [Any]()
    //var peekImplementation: MSPeekCollectionViewDelegateImplementation!
    var workouticonimages = [UIImage]()
    var workoutarray = [Workout_categories]()

    //TimeVar
    var serverDateTimeStr : String?
    var serverDateInDateFormat : Date?
    var startDateInDateFormat : Date?
    var startDate = String()
    var start = Int()
    var today = Int()
    var diff = TimeInterval()
    var checkInterval = String()
    var page = 1
    var stopApi = String()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        checkInterval = "Execute"
         stopApi = "no"

        livesessioncollectionview.delegate = self
        livesessioncollectionview.dataSource = self
        
    }
    
    override func viewWillAppear(_ animated: Bool){
        page = 1
        settingui()
        settingmethods()
    }
    
    func settingui(){
        self.navigationController?.title = "Workouts"
        self.workoutcollectionview.collectionViewLayout = CustomImageFlowLayout()
        //self.livesessioncollectionview.register(UINib(nibName: "WorkoutLiveCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "WorkoutLiveCollectionViewCell")
    }
    func settingmethods(){
        self.liveSessionsContainerView.isHidden = true
        self.liveSessionsContainerViewHeightConstraint.constant = 0
        self.liveSessionsContainerView.layoutIfNeeded()
        liveSessionsAPI()
        //workoutCategoryAPI()
    }
    
    func serverTime(){
        if self.serverDateTimeStr != nil && self.serverDateTimeStr != ""{
            let serverDateString = self.UTCToLocalStringValue(date: self.serverDateTimeStr ?? "", incomingDateFormat: "yyyy-MM-dd HH:mm:ss", outgoingDateFormat: "yyyy-MM-dd HH:mm:ss")
            serverDateInDateFormat = self.getDateFormatterDate(date: serverDateString, incomingDateFormat: "yyyy-MM-dd HH:mm:ss")
            
        }else{}
    }
    
    @IBAction func seeallbuttonaction(_ sender: Any){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "liveWorkoutDetailsScreenVC") as! liveWorkoutDetailsScreenVC
        //vc.workoutlive = self.workoutlive
        //vc.serverDateTimeStr = self.serverDateTimeStr
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension WorkoutsVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == livesessioncollectionview{
            return self.workoutlive.count
        }else{
            return self.workoutarray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        if collectionView == livesessioncollectionview{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WorkOutCollectionviewCell", for: indexPath) as! WorkOutCollectionviewCell
            guard workoutlive.count != 0 else {return cell}
            
            if let getImageInfo = self.workoutlive[indexPath.row].workout_image {
//                cell.workoutImageView?.sd_addActivityIndicator()
//                cell.workoutImageView?.sd_setIndicatorStyle(.gray)
                cell.workoutImageView?.sd_setImage(with: URL(string: getImageInfo), placeholderImage: UIImage(named: "ic_sketchy_background") , options: .continueInBackground) { (downloadImage, error, cacheType, urlValue) in
                }
            }
            let time = self.workoutlive[indexPath.row].title ?? ""
            
            cell.workoutName.text = time
            cell.durationLabel.text = "\(self.workoutlive[indexPath.row].duration ?? "") mins"
            if let getImageInfo = self.workoutlive[indexPath.row].trainer_image {
                cell.trainerImageView?.sd_addActivityIndicator()
                cell.trainerImageView?.sd_setIndicatorStyle(.gray)
                cell.trainerImageView?.sd_setImage(with: URL(string: getImageInfo), placeholderImage: UIImage(named: "ic_sketchy_background") , options: .continueInBackground) { (downloadImage, error, cacheType, urlValue) in
                }
            }
            cell.trainerNameLabel.text = self.workoutlive[indexPath.row].trainer_name ?? ""
            cell.catTypeLabel.text = self.workoutlive[indexPath.row].cat_name ?? ""
            //cell.startsInTimelabel.text =
            let startDateAndTime = "\(self.workoutlive[indexPath.row].workout_date ?? "")"+" "+"\(self.workoutlive[indexPath.row].start_time ?? "")"
            
            getTimeInMillsec(dateString1: startDateAndTime)
            let startDateString = self.convertTimeFormaters(startDateAndTime)
            startDate = self.workoutlive[indexPath.row].start_time ?? ""
            startDateInDateFormat = self.getDateFormatterDate(date: startDateAndTime, incomingDateFormat: "yyyy-MM-dd HH:mm:ss")
            
//            getStart()
//            getToday()
//            get_Diff()
            
            if let serverDateInDateFormat = serverDateInDateFormat, let startDateInDateFormat = startDateInDateFormat {
                
                let startDateComparisionResult:ComparisonResult = serverDateInDateFormat.compare(startDateInDateFormat)
                
                if startDateComparisionResult == ComparisonResult.orderedAscending
                {
                    // Server date is smaller than Start date.
                    cell.startsTextLabel.isHidden = false
                    cell.startsInTimelabel.isHidden = false
                     cell.startsTextLabel.text = "Starts at"
                    cell.startsInTimelabel.textColor = UIColor(red: 255/255, green: 78/255, blue: 78/255, alpha: 1)
                    cell.startsInTimelabel.setCountDownTime(minutes: diff)
                    cell.startsInTimelabel.start()
                    cell.startsInTimelabel.countdownDelegate = self
                }
                else if startDateComparisionResult == ComparisonResult.orderedDescending
                {
                    // Server date is greater than Start date.
                   // cell.startsTextLabel.isHidden = true
                    cell.startsInTimelabel.isHidden = true
                    cell.startsInTimelabel.textColor = UIColor(red: 78/255, green: 153/255, blue: 255/255, alpha: 1)
//                    cell.startsInTimelabel.text = "Live now"
                    cell.startsTextLabel.text = "Live"
                }
                else if startDateComparisionResult == ComparisonResult.orderedSame
                {
                    // Server date and Start date are same.
                    //cell.startsTextLabel.isHidden = true
                     cell.startsInTimelabel.isHidden = true
                    cell.startsInTimelabel.textColor = UIColor(red: 78/255, green: 153/255, blue: 255/255, alpha: 1)
                   // cell.startsInTimelabel.text = "Live now"
                    cell.startsTextLabel.text = "Live"
                }else{}
            }
            
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"WorkOutListCollectionviewCell", for: indexPath) as! WorkOutListCollectionviewCell
            guard workoutarray.count != 0 else {return cell}
            
            cell.workouttitle.text = self.workoutarray[indexPath.row].tittle
            cell.workoutlistimage.sd_setImage(with: URL(string:(self.workoutarray[indexPath.row].image!)), placeholderImage: UIImage(named: "placeholder.png"))
            return cell
        }
    }
    
    func countdownFinished() {
       liveSessionsAPI()
    }
    
    func getTimeInMillsec( dateString1: String) {
        let dateString2 = serverDateTimeStr ?? ""
        let Dateformatter = DateFormatter()
        Dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let date1 = Dateformatter.date(from: dateString1)
        let date2 = Dateformatter.date(from: dateString2)
        
        _ = getStart_InMillisecond(toTime: date1!)
        _ = getToday_InMillisecond(toTime: date2!)
        get_Diff()
        let distanceBetweenDates: TimeInterval? = date1?.timeIntervalSince(date2!)
        
        let secondsInAnHour: Double = 3600 * 60 * 60
        let secondsInDays: Double = 86400
        let secondsInWeek: Double = 604800
        
        let hoursBetweenDates = Int((distanceBetweenDates! / secondsInAnHour))
        let daysBetweenDates = Int((distanceBetweenDates! / secondsInDays))
        let weekBetweenDates = Int((distanceBetweenDates! / secondsInWeek))
        
        print(weekBetweenDates,"weeks")//0 weeks
        print(daysBetweenDates,"days")//5 days
        print(hoursBetweenDates,"hours")//120 hours
    }
    
//    func getToday(){
//
//        let today : String!
//        today = getTodayDateString()
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "HH:mm:ss"
//      //  dateFormatter.timeZone = TimeZone(abbreviation: "EST")
//        let date1 = dateFormatter.date(from: today)
//        let millieseconds = self.getToday_InMillisecond(toTime: date1!)
//        print(millieseconds)
//    }

//    func getStart(){
//
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "HH:mm:ss"
//        let date1 = dateFormatter.date(from: startDate)
//        let millieseconds = self.getStart_InMillisecond(toTime: date1!)
//        print(millieseconds)
//    }
    
//    func getTodayDateString() -> String{
//
//        let date = Date()
//        let calender = Calendar.current
//        let components = calender.dateComponents([.day,.hour,.minute,.second], from: date)
//        let hour = components.hour
//        let minute = components.minute
//        let second = components.second
//        let today_string = String(String(hour!)  + ":" + String(minute!) + ":" +  String(second!))
//        return today_string
//    }
    
    func getToday_InMillisecond(toTime:Date) -> Int{
        let elapsed = NSDate().timeIntervalSince(toTime)
        print(elapsed)
        today = Int(elapsed)
        return Int(elapsed * 1000)
    }
    
    func getStart_InMillisecond(toTime:Date) -> Int{
        let elapsed = NSDate().timeIntervalSince(toTime)
        print(elapsed)
        start = Int(elapsed)
        return Int(elapsed * 1000)
    }
    
    func get_Diff()  {
        let differ = today - start
        diff = TimeInterval(differ)
        print("-------diffrence time--------",diff)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if collectionView == livesessioncollectionview{
            
            checkInterval = "noExecution"
            print(workoutlive.count)
            guard workoutlive.count != 0 else {return}
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "WorkoutDetailVC") as! WorkoutDetailVC
            if self.workoutlive[indexPath.row] != nil{
                vc.preVC = "liveWorkoutDetailsScreenVC"
                vc.prevLiveWorkoutListingData = self.workoutlive[indexPath.row]
                
                vc.startDate = self.workoutlive[indexPath.row].start_time!
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }else{
            
            guard workoutarray.count != 0 else {return}
            let vc:ScheduleVC = self.storyboard?.instantiateViewController(withIdentifier: "ScheduleVC") as! ScheduleVC
            if workoutarray[indexPath.row] != nil{
                vc.catId = self.workoutarray[indexPath.row].id ?? ""
                vc.title = self.workoutarray[indexPath.row].tittle ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == livesessioncollectionview{
            return CGSize(width: collectionView.frame.size.width/1.2, height: collectionView.frame.size.height)
        }else{
           return CGSize(width: collectionView.frame.size.width/2.08, height: (collectionView.frame.size.width/2.08))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == livesessioncollectionview{
            return 0
        }else{
            return 10
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        if collectionView == livesessioncollectionview{
            return 10
        }else{
            return 10
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if stopApi == "no" {
            page += 1
            liveSessionsAPI()
        }
        else {
            print("no")
        }
    }
}

extension WorkoutsVC{
    
    func liveSessionsAPI(){
        
        let parameter = [
            "user_id":getSharedPrefrance(key:"id"),
            "page":"\(page)"
        ]
        
//        self.workoutlive.removeAll()
        
        executePOST(view: self.view, path: Constants.APIs.LIVEURL+Constants.APIs.Api_Home_liveSessions, parameter: parameter){ response in
            let status = response["ResponseCode"].stringValue
            print(status)
            if(status == "true"){
                self.workoutlive.removeAll()

                for live in response["LiveSessionResponse"].arrayValue {
                    self.workoutlive.append(Workoutdetailslive(json:live.dictionaryObject!)!)
                }
                
                let serverDateTime = response["Server_time"].stringValue
                if serverDateTime.count != 0{
                    self.serverDateTimeStr = serverDateTime
                    print(serverDateTime)
                    self.serverTime()
                }
                self.liveSessionsContainerView.isHidden = false
                self.liveSessionsContainerViewHeightConstraint.constant = 250
                self.liveSessionsContainerView.layoutIfNeeded()
                self.livesessioncollectionview.reloadData()                
                self.workoutCategoryAPI()
                
            }else{
                self.stopApi = "yes"
                self.workoutCategoryAPI()
                self.view.makeToast(response["MessageWhatHappen"].stringValue)
            }
        }
    }
    
    func workoutCategoryAPI(){
        
//        let parameter = [
//            "user_id":getSharedPrefrance(key:"id")
//        ]
//        self.workoutarray.removeAll()
//
//        executePOST(view: self.view, path: Constants.LIVEURL+"/Api/Home/workoutCategory", parameter: parameter){ response in
//            print(response)
//            let status = response["ResponseCode"].stringValue
//            print(status)
//            self.view.makeToast(response["MessageWhatHappen"].stringValue)
//            if(status == "true"){
//
//                for i in response["WorkoutCategoriesResponse"].arrayValue{
//                    self.workoutarray.append(Workout_categories(json:i.dictionaryObject!)!)
//                }
//
//                self.workoutcollectionview.reloadData()
//
//            }else{
//                self.view.makeToast(response["MessageWhatHappen"].stringValue)
//            }
//        }
        
        self.workoutarray.removeAll()
        
        executeGET(view: self.view, path: Constants.APIs.LIVEURL+Constants.APIs.Api_Home_workoutCategory) { (response) in
            print(response)
            let status = response["ResponseCode"].stringValue
            print(status)
            self.view.makeToast(response["MessageWhatHappen"].stringValue)
            if(status == "true"){
                
                for i in response["WorkoutCategoriesResponse"].arrayValue{
                    self.workoutarray.append(Workout_categories(json:i.dictionaryObject!)!)
                }
                
                let oneRowHeight = (self.workoutcollectionview.frame.size.width/2.08)+10
                let workoutsArrCount = self.workoutarray.count
                if workoutsArrCount > 2{
                    if workoutsArrCount % 2 == 0 {
                        print("Even=",workoutsArrCount)
                        let resultedValue = workoutsArrCount / 2
                        self.workoutCollectionViewHeightConstraint.constant = CGFloat(resultedValue)*(oneRowHeight)
                    } else {
                        print("Odd=",workoutsArrCount)
                        let resultedValue = (workoutsArrCount+1) / 2
                        self.workoutCollectionViewHeightConstraint.constant = CGFloat(resultedValue)*(oneRowHeight)
                    }
                }else{
                    self.workoutCollectionViewHeightConstraint.constant = oneRowHeight
                }
               
                self.workoutcollectionview.layoutIfNeeded()
                self.workoutcollectionview.reloadData()
                
            }else{
                self.view.makeToast(response["MessageWhatHappen"].stringValue)
            }
        }
    }
}

