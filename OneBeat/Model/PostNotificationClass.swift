//
//  PostNotificationClass.swift
//  OneBeat
//
//  Created by osvinuser on 26/12/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//

import Foundation
import Gloss

class PostNotificationClass: JSONDecodable{
    
    var notification_status:NSString? = ""
    var workout_id : String?
    var title : Int?
    var duration : String? = ""
    var meeting_id : String? = ""
    var action : String? = ""
    var message : String? = ""
    
    required init?(json: JSON){
        self.notification_status = "notification_status" <~~ json
        self.workout_id = "workout_id" <~~ json
        self.title = "title" <~~ json
        self.duration = "duration" <~~ json
        self.meeting_id = "meeting_id" <~~ json
        self.action = "action" <~~ json
        self.message = "message" <~~ json
    } 
}
