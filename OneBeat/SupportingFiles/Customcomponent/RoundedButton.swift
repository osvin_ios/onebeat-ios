//
//  RoundedButton.swift
//  GetBlushh
//
//  Created by Arvind Mehta on 09/02/18.
//  Copyright © 2018 Arvind Mehta. All rights reserved.
//

import Foundation
import UIKit


@IBDesignable class RoundedButton: UIButton {
    
    @IBInspectable var borderColor: UIColor = UIColor.white {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
 
    @IBInspectable var backColor: String = "#fffff" {
        didSet {
            layer.backgroundColor = hexStringToUIColor(hex: backColor).cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 2.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var roundRectCornerRadius: CGFloat = 2.0 {
        didSet {
            layer.cornerRadius = roundRectCornerRadius
        }
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        clipsToBounds = true
        layoutRoundRectLayer()
    }
    
    // MARK: Private
    private var roundRectLayer: CAShapeLayer?
    
    private func layoutRoundRectLayer() {
        if let existingLayer = roundRectLayer {
            existingLayer.removeFromSuperlayer()
        }
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = UIBezierPath(roundedRect: self.bounds, cornerRadius: roundRectCornerRadius).cgPath
        shapeLayer.fillColor = borderColor.cgColor
        shapeLayer.backgroundColor = hexStringToUIColor(hex: backColor).cgColor
        self.layer.insertSublayer(shapeLayer, at: 0)
        self.roundRectLayer = shapeLayer
    }
}
