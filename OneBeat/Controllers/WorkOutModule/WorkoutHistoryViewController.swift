//
//  WorkoutHistoryViewController.swift
//  OneBeat
//
//  Created by osvinuser on 09/04/19.
//  Copyright © 2019 osvinuser. All rights reserved.
//

import UIKit

class WorkoutHistoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var leaderboardTableView: UITableView!
    var leaderBoardUsersListingData = [WorkoutHistoryClass]()
    var labelCount = Int()
    var page = Int()
    var stopApi = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        stopApi = "no"
        page = 1
        navigationItem.title = "Workout History"
        self.leaderboardTableView.register(UINib(nibName: "LiveLeaderboardTableViewCellXib", bundle: nil), forCellReuseIdentifier: "LiveLeaderboardTableViewCellXib")
       labelCount = 0
        if Reachability.isConnectedToNetwork() == true {
            workout_History_Api()
        } else {
            self.showAlert(Constants.AlertsMessages.noInternetConnecLocalize)
        }
        
        let backBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(backbuttonaction))
        self.navigationItem.leftBarButtonItem  = backBarButtonItem
        let sliderImage = UIImage(named: "ic_back_arrow_black")
        navigationItem.leftBarButtonItem?.image = sliderImage
        navigationItem.leftBarButtonItem?.tintColor = UIColor.black
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    @objc func backbuttonaction(){
        self.navigationController?.popViewController(animated:true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leaderBoardUsersListingData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = leaderboardTableView.dequeueReusableCell(withIdentifier: "historyCell", for: indexPath) as! HistoryTableViewCell
        cell.selectionStyle = .none
        guard leaderBoardUsersListingData.count != 0 else {return cell}

        if let getImageInfo = self.leaderBoardUsersListingData[indexPath.row].workOut_Image {
            
        cell.workout_Image?.sd_setImage(with: URL(string: getImageInfo), placeholderImage: UIImage(named: "ic_sketchy_background") , options: .continueInBackground) { (downloadImage, error, cacheType, urlValue) in
            }
        }
        if let getImageInfo = self.leaderBoardUsersListingData[indexPath.row].trainer_Image {
            
            cell.userProfile_Image?.sd_setImage(with: URL(string: getImageInfo), placeholderImage: UIImage(named: "ic_sketchy_background") , options: .continueInBackground) { (downloadImage, error, cacheType, urlValue) in
            }
        }
        
        cell.workOutUser_Name_Label.text = self.leaderBoardUsersListingData[indexPath.row].trainer_Name ?? ""
        cell.workOut_Time_Label.text = self.leaderBoardUsersListingData[indexPath.row].workout_Points ?? ""
        cell.workout_Name_Label.text =  self.leaderBoardUsersListingData[indexPath.row].workout_Title ?? ""
      
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 186
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let VC = storyboard?.instantiateViewController(withIdentifier: "LeaderBoardWorkoutDetailViewController") as! LeaderBoardWorkoutDetailViewController
        VC.workOutID = self.leaderBoardUsersListingData[indexPath.row].workout_id ?? ""
        navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func backBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if stopApi == "no" {
        page += 1
        workout_History_Api()
        leaderboardTableView.reloadData()
        }
        else {
            print("no")
             }
    }
    
    func workout_History_Api(){

        let parameter: [String:Any] = ["user_id" : getSharedPrefrance(key: "id"),
                                       "page" : "\(page)"]
     
        executePOST_WithoutToast(view: self.view, path: Constants.APIs.LIVEURL + "/Api/Home/userHistory", parameter: parameter){ response in
           print(response)
            let status = response["ResponseCode"].stringValue
            if(status == "true"){
//                self.leaderBoardUsersListingData.removeAll()
                for data in response["UserWorkoutHistoryResponse"].arrayValue{
                    self.leaderBoardUsersListingData.append(WorkoutHistoryClass(json:data.dictionaryObject!)!)
                }
                self.leaderboardTableView.reloadData()
                
            }else{
                self.stopApi = "yes"
                //self.view.makeToast(response["MessageWhatHappen"].stringValue)
            }
        }
    }
}
