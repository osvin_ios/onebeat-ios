//
//  WorkoutHistoryClass.swift
//  OneBeat
//
//  Created by osvinuser on 09/04/19.
//  Copyright © 2019 osvinuser. All rights reserved.
//

import Foundation
import Gloss

class WorkoutHistoryClass:JSONDecodable{
    
    var workout_id : String? = ""
    var user_id : String? = ""
    var username: String? = ""
    var profile_image : String? = ""
    var points : String? = ""
    var heart_rate: String? = ""
    var attendee_workout_time: String? = ""
    var total_Workout: String? = ""
    var workout_Points: String? = ""
    var workout_time: String? = ""
    var workOut_Name: String? = ""
    var workout_Title: String? = ""
    var workOut_Image: String? = ""
    var cat_name: String? = ""
    var trainer_Name : String? = ""
    var trainer_Image : String? = ""
    var Videos_Url : String? = ""
    var video_Image_Url: String? = ""
    var workout_title: String? = ""
    var description: String? = ""
    
    required init?(json: JSON) {
        
        self.workout_id = "workout_id" <~~ json
        self.user_id = "user_id" <~~ json
        self.username = "username" <~~ json
        self.profile_image = "profile_image" <~~ json
        self.points = "points" <~~ json
        self.heart_rate = "max_heart_rate" <~~ json
        self.attendee_workout_time = "attendee_workout_time" <~~ json
        self.total_Workout = "total_Workouts" <~~ json
        self.workout_Points = "workout_point" <~~ json
        self.workout_time = "attendee_workout_time" <~~ json
        self.workOut_Name = "workout_name" <~~ json
        self.workOut_Image = "workout_image" <~~ json
        self.cat_name = "cat_name" <~~ json
        self.trainer_Name = "trainer_name" <~~ json
        self.trainer_Image = "trainer_image" <~~ json
        self.workout_Title = "title" <~~ json
        self.Videos_Url = "workout_video" <~~ json
        self.video_Image_Url = "workout_image" <~~ json
        self.workout_title = "Workout_title" <~~ json
        self.description = "description" <~~ json
        
    }    
}
