//
//  MyAccountVC.swift
//  OneBeat
//
//  Created by osvinuser on 11/12/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//

import UIKit
import SDWebImage
import CoreLocation
import Alamofire
import SwiftyJSON
import CountdownLabel

class MyAccountVC: UIViewController,CLLocationManagerDelegate {

    @IBOutlet weak var pointlabel: UILabel!
    @IBOutlet weak var totaltimelabel: NZLabel!
    @IBOutlet weak var workoutlabel: NZLabel!
    @IBOutlet weak var locationofuser: UILabel!
    @IBOutlet weak var usernamelabel: UILabel!
    @IBOutlet weak var profileimage: UIImageView!
    @IBOutlet weak var highesthrlabel: NZLabel!
    var hours = String()
    var minutes = String()
    var seconds = String()
    var totalTimeValue = String()
    var meetId = String()
    
    let locationManager = CLLocationManager()
    var userWorkoutClass:UserWorkoutClass? = nil
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        if checkPushData == true {
           // checkPushData = false
            meetId = meetingId
            meetingId = ""
            view.makeToastActivity(.center)
            
            _ = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block:  { result in
                if let name = getSharedPrefrance(key:"first_name") as? String{
                    if let mID = Int(self.meetId){
                        ZoomService.sharedInstance.joinMeeting(name: name, number: mID, password: "", type: "Video",vc: self)
                    }
                }
            })
        }
            printSecondsToHoursMinutesSeconds()
            
            self.navigationItem.title = ""
            let profile_imageStr:String = getSharedPrefrance(key:"profile_image")
            let profilenewString = profile_imageStr.replacingOccurrences(of: "localhost", with: "192.168.1.90", options: .literal, range: nil)
            profileimage.sd_setImage(with: URL(string:profilenewString), placeholderImage: UIImage(named: "placeholder.png"))
            
            isAuthorizedtoGetUserLocation()
            
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            }
            NotificationCenter.default.addObserver(self, selector: #selector(showSpinningWheel(notification:)), name:NSNotification.Name(rawValue: "imageupdating"), object: nil)
    }
    
    func isAuthorizedtoGetUserLocation() {
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse{
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(true)
        
        usernamelabel.text = getSharedPrefrance(key:"first_name") + " " + getSharedPrefrance(key:"last_name") + " " + "(" + getSharedPrefrance(key: "userName") + ")"
        if let isPurchased = UserDefaults.standard.value(forKey: "isPurchased") as? String, isPurchased == "1" {
        
        } else {
            self.showAlert("You are not subscribed to our monthly subscription.")
        }
            getTime()
            self.tabBarController?.tabBar.isHidden = false
            navigationController?.setNavigationBarHidden(true, animated: true)
            if Reachability.isConnectedToNetwork() == true {
                settingmethods()
            } else {
                self.showAlert(Constants.AlertsMessages.noInternetConnecLocalize)
            }
        }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @IBAction func notification_Btn(_ sender: UIButton) {
        let Vc = storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        navigationController?.pushViewController(Vc, animated: true)
    }
    
    func getTime() {
        
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
    }
    
    func diffmill() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" //Your date format
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        let date = dateFormatter.date(from: "2019-04-31 18:59:00")
        let startDate = date!
    
        let now = Date()
        let dateString = dateFormatter.string(from:now)
        NSLog("%@", dateString)
        let endDate = dateFormatter.date(from: dateString)!
        let calendar = Calendar.current
        let dateComponents = calendar.compare(endDate, to: startDate, toGranularity: .second)
        let seconds = dateComponents.rawValue
        print("Seconds: \(seconds)")
    }
    
    //this method will be called each time when a user change his location access preference.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            
            print("User allowed us to access location")            
            guard let locValue:CLLocationCoordinate2D = manager.location!.coordinate else { return }
            print("locations = \(locValue.latitude) \(locValue.longitude)")
            getAddressFromLatLon(pdblLatitude:"\(locValue.latitude)", withLongitude:"\(locValue.longitude)")
            print("Did location updates is called")
        }
    }
    
    //this method is called by the framework on locationManager.requestLocation();
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        //  getAddressFromLatLon(pdblLatitude:"\(locValue.latitude)", withLongitude:"\(locValue.longitude)")
        self.locationofuser.text = "locations = \(locValue.latitude) \(locValue.longitude)"
        print("Did location updates is called")
        //store the user location here to firebase or somewhere
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Did location updates is called but failed getting location \(error)")
    }
   
//    func settingui(){
//
////        let firstfont:UIFont = UIFont(name: "nunito_semibold", size: 20)!
////        let boldFont:UIFont = UIFont(name: "nunito_regular", size: 12)!
//        let firstfont:UIFont = UIFont(name: "Helvetica", size: 20)!
//        let boldFont:UIFont = UIFont(name: "Helvetica", size: 12)!
//
//        let firstDict:NSDictionary = NSDictionary(object: firstfont, forKey:
//            NSAttributedString.Key.font as NSCopying)
//        let boldDict:NSDictionary = NSDictionary(object: boldFont, forKey:
//            NSAttributedString.Key.font as NSCopying)
//
//        let firstText = "132"
//        let attributedString = NSMutableAttributedString(string: firstText,
//                                                         attributes: firstDict as? [NSAttributedString.Key : Any])
//
//        let boldText  = " bpm"
//        let boldString = NSMutableAttributedString(string:boldText,
//                                                   attributes:boldDict as? [NSAttributedString.Key : Any])
//
//        attributedString.append(boldString)
//        highesthrlabel.attributedText = attributedString
//
//        let workouttime = "36:32"
//        let workouttimedisplayattribute = NSMutableAttributedString(string:workouttime,
//                                                         attributes: firstDict as? [NSAttributedString.Key : Any])
//
//        let minsstring  = " mins"
//        let minsworkout = NSMutableAttributedString(string:minsstring,
//                                                   attributes:boldDict as? [NSAttributedString.Key : Any])
//
//        workouttimedisplayattribute.append(minsworkout)
//        workoutlabel.attributedText = workouttimedisplayattribute
//        totaltimelabel.attributedText = workouttimedisplayattribute
//
//    }
    
    @objc func showSpinningWheel(notification: NSNotification){
        
        let profile_imageStr:String = getSharedPrefrance(key:"profile_image")
        let profilenewString = profile_imageStr.replacingOccurrences(of: "localhost", with: "192.168.1.90", options: .literal, range: nil)
        profileimage.sd_setImage(with: URL(string:profilenewString), placeholderImage: UIImage(named: "placeholder.png"))        
    }
    
    @IBAction func workout_Btn_Action(_ sender: UIButton) {
        let Vc = storyboard?.instantiateViewController(withIdentifier: "WorkoutHistoryViewController") as! WorkoutHistoryViewController
        navigationController?.pushViewController(Vc, animated: true)
    }
    
    func settingmethods(){
        UserWorkoutDataResponse()
    }

    @IBAction func editprofiledetailsbuttonaction(_ sender: Any){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension MyAccountVC{
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String){
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        
        let lon: Double = Double("\(pdblLongitude)")!
       
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        var addressString : String = ""
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil){
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                
                if let pm = placemarks as? [CLPlacemark]{
                    if pm.count > 0{
                        let pm = placemarks![0]
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                        }
                        if pm.country != nil {
                            addressString = addressString + pm.country! + ", "
                        }
                        
                        savesharedprefrence(key:"location", value:addressString)
                        savesharedprefrence(key:"latitude", value:pdblLatitude)
                        savesharedprefrence(key:"longitude", value:pdblLongitude)
                        
                        self.locationofuser.text = addressString
                        
                    }
                }
            })
         }
    
    func UserWorkoutDataResponse(){
        let parameter = [
            "user_id": getSharedPrefrance(key:"id")
        ]
        
        executePOST(view: self.view, path: Constants.APIs.LIVEURL+"/Api/Home/userWorkoutData", parameter: parameter){ response in
            print(response)
            let status = response["ResponseCode"].stringValue
            print(status)
            self.view.makeToast(response["MessageWhatHappen"].stringValue)
            if(status == "true"){
                let user_Data = User()
                self.userWorkoutClass = UserWorkoutClass.init(json:response["UserWorkoutDataResponse"].dictionaryObject!)
                
                let totaltimelabel = self.userWorkoutClass?.attendee_workout_time as String? ?? ""
                user_Data.total_Time = totaltimelabel
                let timeIn_Min = Int(totaltimelabel ?? "")
                if totaltimelabel == "" {
                
                }
                else {
                self.convertTo_Min_Hrs_Sec(value: timeIn_Min!)
//              self.secondsToHoursMinutesSeconds(seconds: timeIn_Min!)
                }
                
                self.workoutlabel.text = self.userWorkoutClass?.total_Workouts as String? ?? ""
                user_Data.workout = self.userWorkoutClass?.total_Workouts as String? ?? ""
                let highesthrlabel = self.userWorkoutClass?.avg_heart_rate as String? ?? ""
                user_Data.heart_Rate = self.userWorkoutClass?.avg_heart_rate as String? ?? ""
                self.pointlabel.text = self.userWorkoutClass?.workout_point as String? ?? ""
                user_Data.points = self.userWorkoutClass?.workout_point as String? ?? ""
                AppInstance.shared.userData = user_Data
                if highesthrlabel == "" {
                    
                } else {
                let first_String: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.black, .font: UIFont.boldSystemFont(ofSize: 18)]
                let second_String: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.red, .font: UIFont.systemFont(ofSize: 14)]
//
//                let partOne = NSMutableAttributedString(string: totaltimelabel!, attributes: first_String)
//                let partTwo = NSMutableAttributedString(string: "  min", attributes: second_String)
//                partOne.append(partTwo)
                self.totaltimelabel.text = self.hours
                
                let firstOne = NSMutableAttributedString(string: highesthrlabel, attributes: first_String)
                let secondOne = NSMutableAttributedString(string: "  bpm", attributes: second_String)
                firstOne.append(secondOne)
                self.highesthrlabel.attributedText = firstOne
                }
            }else{
                self.view.makeToast(response["MessageWhatHappen"].stringValue)
            }
        }
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    func printSecondsToHoursMinutesSeconds () -> () {
        let (h, m, s) = secondsToHoursMinutesSeconds (seconds: 1065)
        print("\(h) hrs: \(m) min: \(s) sec")
        hours = "\(h) hrs: \(m) min: \(s) sec"
    }
    
    func convertTo_Min_Hrs_Sec(value: Int) {
        
        let min = (value/60)
        let hrs = value/3600
        if min > 60 {
            let total_Hours = "\(hrs)" + "+" + " " + "hrs"
            hours = "\(total_Hours)"
            print(hours)
        }else {
            hours = "\(min)" + "+" + " " + "min"
            print(hours)
        }
      }
    }

