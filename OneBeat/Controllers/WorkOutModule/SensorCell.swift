//
//  SensorCell.swift
//  OneBeat
//
//  Created by osvinuser on 17/12/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//

import UIKit

class SensorCell: UITableViewCell {
   
    @IBOutlet weak var connectbutton: UIButton!
    @IBOutlet weak var connectionStatusLabel: UILabel!
    @IBOutlet weak var namelabel: UILabel!
    @IBOutlet weak var heartrateLabel: UILabel!
    @IBOutlet weak var connectionActivityIndicator: UIActivityIndicatorView!
    
    var deviceInformation: WFDeviceInformation?
    var sensorConnections = [WFSensorConnection]()
    var sensorConnectionLookup = [String:WFSensorConnection]()
    var updateTimer: Timer?
    var dataUpdateTimer: Timer?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
     self.updateTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimerFired(timer:)), userInfo: nil, repeats: true)
     dataUpdateTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateCellsheartbeat), userInfo: nil, repeats: true)
      
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func connectionbutton(_ sender:UIButton){
        connect()
    }
    
    @IBAction func heartratebuttonaction(_ sender:UIButton){
        print("heartrate")
        updateCellsheartbeat()
    }
    
    @objc func updateCellsheartbeat(){
        
        let heartrateboolean:String =  getSharedPrefrance(key:"heartrate")
        
        if heartrateboolean == "true"{
            
            let updateArray = self.deviceInformation?.supportedSensorTypes
            
            if updateArray != nil{
                //for result:Int in updateArray{
                for result in updateArray!{
                    
                    //let sensorType = WFSensorType_t(UInt(result))
                    let sensorType = wfSensorTypeFromNSNumber((result as! NSNumber))
                    
                    print(sensorType)
                    
                    switch sensorType{
                        
                    case WF_SENSORTYPE_BIKE_POWER:
                        //---- Bike Speed and/or Cadence
                        //---- Including KICKR, inRide and other power meters
                        break
                    case WF_SENSORTYPE_BIKE_SPEED_CADENCE:
                        //---- Bike Speed and/or Cadence
                        break
                    case WF_SENSORTYPE_FOOTPOD:
                        //---- Foodpods / Running Speed & Cadence sensors
                        break
                    case WF_SENSORTYPE_HEARTRATE:
                        //---- Heartrate
                        
                        let i = result
                        
                        //print(WFSensorType_t(UInt(i)))\
                        //let connection:WFHeartrateConnection = self.sensorConnectionLookup["\(wfSensorTypeFromNSNumber((i as! NSNumber)))"] as! WFHeartrateConnection
                        //let data:WFHeartrateData = connection.getHeartrateData()
                        //self.heartrateLabel.text = data.formattedHeartrate(true) ?? "N/A"
                        
                        if let connection:WFHeartrateConnection = self.sensorConnectionLookup["\(wfSensorTypeFromNSNumber((i as! NSNumber)))"] as? WFHeartrateConnection{
                            
                            if let data:WFHeartrateData = connection.getHeartrateData(){
                                self.heartrateLabel.text = data.formattedHeartrate(true) ?? "N/A"
                            }
                            
                        }
                        
                        break
                    case WF_SENSORTYPE_WEIGHT_SCALE:
                        //---- Weight Scale
                        break
                    case WF_SENSORTYPE_DISPLAY:
                        //---- Display / RFLKT / Echo / Timex
                        break
                    case WF_SENSORTYPE_BIKE_SPEED:
                        //---- ANT+ Speed Only
                        //---- All BTLE Speed and/or Cadence sensors use the Bike Speed and Cadence Profile
                        break
                    case WF_SENSORTYPE_BIKE_CADENCE:
                        //---- ANT+ Cadence Only
                        //---- All BTLE Speed and/or Cadence sensors use the Bike Speed and Cadence Profile
                        break
                    default:
                        break
                        
                    }
                }
            }
            
        }else{}
    }
    
    @objc func updateTimerFired(timer:Timer){        
        self.updateConnectionUI()
    }
    
    func setDeviceInformation(deviceInformation:WFDeviceInformation) {
        if deviceInformation != self.deviceInformation{
            self.deviceInformation = deviceInformation
            self.updateCell()
        }
    }
    
    func setSensorConnections(sensorConnections:[WFSensorConnection]){
        self.sensorConnections = sensorConnections
        self.updateConnectionUI()
    }
    
    func updateCell(){
        self.namelabel.text = self.deviceInformation?.name
    }
    
    func connect(){
        
        print("WFSensorTVC::connect")
        var connectionParams = [WFConnectionParams]()
        print("connection Paramteres----",connectionParams)
        
        connectionParams = self.deviceInformation?.connecitonParamsForAllSupportSensorTypes() as! [WFConnectionParams]
        var connectionLookup = [String:WFSensorConnection]()
        
        for params: WFConnectionParams in connectionParams {
            let error: Error? = nil
            print("parameterss---",params)
            let sensorConnection: WFSensorConnection? = try? WFHardwareConnector.shared().requestSensorConnection(params, withProximity: WF_PROXIMITY_RANGE_DISABLED)
            if error != nil {
                if let anError = error {
                    print("ERROR: Failed to create sensor connection! \n\(anError)")
                }
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                    self.connect()
                })
            }else{
                print("sensorytypes----",params.sensorType)
                connectionLookup["\(params.sensorType)"] = sensorConnection
            }
        }
      
        print("conection values---",connectionLookup.values)
        self.sensorConnections = Array(connectionLookup.values)
        print("coneection cont------",self.sensorConnections.count)
        self.sensorConnectionLookup = connectionLookup
        updateConnectionUI()
        
    }
    
    func disconnect(){
        //[self.sensorConnectionLookup.allValues makeObjectsPerformSelector:@selector(disconnect)];
        //self.sensorConnections.makeObjectsPerform(#selector(self.disconnect))
    }
    
    func updateConnectionUI(){
       
        if self.sensorConnections.isEmpty{
            self.connectionStatusLabel.text = ""
            if self.connectionActivityIndicator.isAnimating{
                self.connectionActivityIndicator.stopAnimating()
            }
        }else{
            let status:WFSensorConnectionStatus_t = SensorConnectionHelper.combindedSensorStatus(fromConnections:self.sensorConnections)
            self.deviceInformation = self.sensorConnections.first?.deviceInformation
            if status == WF_SENSOR_CONNECTION_STATUS_CONNECTED && self.connectionActivityIndicator.isAnimating{
                self.connectionActivityIndicator.stopAnimating()
                self.connectionActivityIndicator.isHidden = true
            }else if status != WF_SENSOR_CONNECTION_STATUS_CONNECTED && self.connectionActivityIndicator.isAnimating{
                self.connectionActivityIndicator.startAnimating()
                self.connectionActivityIndicator.isHidden = false
            }else{
                switch status{
                    case WF_SENSOR_CONNECTION_STATUS_CONNECTED:
                        self.connectionStatusLabel.text = "Connected"
                        savesharedprefrence(key:"heartrate", value:"true")
                        UserDefaults.standard.set("connected", forKey: "checkconnection")
                    
                        break
                    case WF_SENSOR_CONNECTION_STATUS_CONNECTING:
                        self.connectionStatusLabel.text = "Connecting"
                       //savesharedprefrence(key:"heartrate", value:"false")
                        break
                    case WF_SENSOR_CONNECTION_STATUS_DISCONNECTING:
                        self.connectionStatusLabel.text = "Disconnecting"
                         savesharedprefrence(key:"heartrate", value:"false")
                       
                        break
                    case WF_SENSOR_CONNECTION_STATUS_INTERRUPTED:
                        self.connectionStatusLabel.text = "Interrupted"
                       // savesharedprefrence(key:"heartrate", value:"false")
                       // UserDefaults.standard.removeObject(forKey: "checkconnection")
                        break;
                    case WF_SENSOR_CONNECTION_STATUS_IDLE:
                        self.connectionStatusLabel.text = "Waiting"
                       //  savesharedprefrence(key:"heartrate", value:"false")
                        break
                    default:
                        savesharedprefrence(key:"heartrate", value:"false")
                        self.connectionStatusLabel.text = "Waiting"
                }
            }
        }
    }
}
