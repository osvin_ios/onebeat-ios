//
//  FullViewTableVIewCell.swift
//  OneBeat
//
//  Created by osvinuser on 14/06/19.
//  Copyright © 2019 osvinuser. All rights reserved.
//

import UIKit

class FullViewTableVIewCell: UITableViewCell {

    @IBOutlet weak var user_ImageView: ImageViewDesign!
    @IBOutlet weak var user_Name: UILabel!
    @IBOutlet weak var user_Point_Label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
