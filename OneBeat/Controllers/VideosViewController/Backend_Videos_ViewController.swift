//
//  Backend_Videos_ViewController.swift
//  OneBeat
//
//  Created by osvinuser on 17/06/19.
//  Copyright © 2019 osvinuser. All rights reserved.
//

import UIKit
import AVKit

class Backend_Videos_ViewController: UIViewController {

    @IBOutlet weak var video_View: UIView!
    var player = AVPlayer()
    var leaderBoardUsersListingData = [WorkoutHistoryClass]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let path = Bundle.main.path(forResource: "video", ofType: "mp4") else {
            return
        }
        let videoURL = NSURL(fileURLWithPath: path)
        
        // Create an AVPlayer, passing it the local video url path
        player = AVPlayer(url: videoURL as URL)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.video_View.bounds
        video_View.clipsToBounds = true
        video_View.layoutIfNeeded()
        self.video_View.layer.addSublayer(playerLayer)
        player.play()
//        let controller = AVPlayerViewController()
//        controller.player = player
//        present(controller, animated: true) {
//            self.player.play()
//        }

//        let videoURL = URL(string: "https://www.youtube.com/results?search_query=dogs+puppy")
//        player = AVPlayer(url: videoURL!)
//        let playerViewController = AVPlayerViewController()
//        playerViewController.player = player
//        self.present(playerViewController, animated: true) {
//            playerViewController.player!.play()
//        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        player.pause()
        appDelegate.deviceOrientation = .portrait
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    
    func fetch_Videos_Api(user_id : String){
        let parameter = [
            "from_id" : getSharedPrefrance(key:"id"),
            "to_id"   : user_id
        ]
        
        executePOST(view: self.view, path: Constants.APIs.LIVEURL+Constants.APIs.User_Like_Api, parameter: parameter){ response in
            print(response)
            
            for data in response["leaderBoardWorkoutResponse"].arrayValue{
                self.leaderBoardUsersListingData.append(WorkoutHistoryClass(json:data.dictionaryObject!)!)
            }
//            let status = response["ResponseCode"].stringValue
//            if(status == "true"){
//                self.showToast(message: "Liked Successfully")
//
//            } else {
//                self.view.makeToast(response["MessageWhatHappen"].stringValue)
//            }
        }
    }
}
